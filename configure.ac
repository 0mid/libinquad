dnl configure.ac ---

dnl Copyright (C) 2016 Omid Khanmohamadi

dnl Author: Omid Khanmohamadi

dnl This program is free software; you can redistribute it and/or
dnl modify it under the terms of the GNU General Public License
dnl as published by the Free Software Foundation; either version 3
dnl of the License, or (at your option) any later version.

dnl This program is distributed in the hope that it will be useful,
dnl but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
dnl GNU General Public License for more details.

dnl You should have received a copy of the GNU General Public License
dnl along with this program. If not, see <http://www.gnu.org/licenses/>.


dnl Maintainer requirements:
dnl - Automake >= 1.13.
dnl - Autotools versions actually used (lower versions also work):
dnl   - Autoconf 2.69, Automake 1.15, Libtool 2.4.6, m4 1.4.17.

AC_INIT([libinquad], [1.0.0], [])
AC_CONFIG_AUX_DIR([build-aux])
AC_CONFIG_MACRO_DIR([m4])

dnl Starting from Automake 1.13, the parallel testsuite harness has
dnl been made the default one; this harness is quite silent, and even
dnl with VERBOSE=yes, it only displays the logs of the non-passed
dnl tests (i.e., only of the failed or skipped ones, or of the ones
dnl that passed unexpectedly). Until Automake 1.12, the older (and
dnl more verbose) serial harness was the default, so -- among other
dnl differences -- the output used to be more verbose. If you want to
dnl retain the use of the old serial harness in your project, you have
dnl to use the 'serial-tests' option (recognized by automake 1.12,
dnl 1.13, 1.14, and 1.15, but unfortunately *not* by 1.11).
dnl
dnl [https://lists.gnu.org/archive/html/automake/2013-06/msg00049.html]
dnl [https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=710955]
AM_INIT_AUTOMAKE([1.13 serial-tests -Wall -Werror foreign])

dnl NEWS from Automake 1.12:
dnl
dnl - The warnings in the category 'extra-portability' are now enabled
dnl   by '-Wall'. In previous versions, one has to use
dnl   '-Wextra-portability' to enable them.
dnl
dnl which should be read in light of this from Automake 1.11.2:
dnl
dnl - New macro AM_PROG_AR that looks for an archiver and wraps it in the new
dnl   'ar-lib' auxiliary script if the selected archiver is Microsoft lib.
dnl   This new macro is required for LIBRARIES and LTLIBRARIES when automake
dnl   is run with -Wextra-portability and -Werror.
dnl
dnl [https://lists.gnu.org/archive/html/bug-automake/2012-05/msg00009.html]
AM_PROG_AR

LT_INIT
AC_PROG_CC
AC_PROG_CXX
AC_LANG([C++])
AC_CONFIG_HEADERS([config.h])

dnl The libraries found here by 'configure' are automatically added to
dnl the linker LIBS variable and are used (last on linker command
dnl line) for linking ALL targets.

AC_CHECK_LIB([m],[cos])
AC_CHECK_LIB([openblas],[cblas_dgemm])
AC_CHECK_LIB([gsl],[gsl_blas_dgemm])

dnl Unless listed in AC_CONFIG_FILES, a Makefile is not created (by
dnl ./configure or more precisely ./config.status) for a Makefile.am
dnl in a directory. You may have only one Makefile (hence only one
dnl Makefile.am) per directory.
AC_CONFIG_FILES([
 Makefile
 src/Makefile
 tests/Makefile
])
AC_OUTPUT
