// test_inquad_3d.cc ---

// Copyright (C) 2017 Omid Khanmohamadi

// Author: Omid Khanmohamadi

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "src/inquad.h"
#include "tst_functions.h"
#include "tst_cond_compile.h"
#include "tst_invoke_macro.h"	// for INVOKE_INQUAD_TEST

#include "gsl/gsl_math.h"	// for M_constants
#include "gtest/gtest.h"	// for Google Test framework

#include "gsl/gsl_sf_gamma.h"	// for gsl_sf_gamma

#include <math.h>		// for pow

double vol_ball(double r, double d)
{
  return pow(M_PI, d/2) * pow(r, d) / gsl_sf_gamma(d/2 + 1);
}


TEST(inquad_test,
     inquad_area_unit_disk_in_neg0p5_0p7_x_2_4_x_0p1_0p3_pruning_zero)
{
  const int	sgn	= -1;
  const bool	is_surf = false;

  inquad_params inqparams = inquad_default_params;
  inqparams.quad_npts = 32;
  IF_TRACE_QUAD_NPTS(2);

  // The area of the intersection of the unit disk (enclosed by the
  // unit circle) with the hyprect h = [-0.5, 0.7]x[2.0, 4.0]x[0.1, 0.3] is
  //
  // 0.0
  const unsigned dim = 3;
  const double h_xmins[] = { -0.5, 2.0, 0.1};
  const double h_xmaxs[] = {  0.7, 4.0, 0.3};

  const double	integral_value_relerr	= 1e-6;
  const double	integral_value_expected = 0.0;

  integrand_field   integrand_F;
  const_func_params integrand_F_params;
  integrand_F_params.c = 1.0;
  integrand_F.function = const_func;
  integrand_F.params   = &integrand_F_params;

  INVOKE_INQUAD_TEST;
}


TEST(inquad_test,
     inquad_area_unit_ball_in_neg0p1_0p3_x_neg0p2_0p4_x_0p3_0p4_pruning_cubature)
{
  const int	sgn	= -1;
  const bool	is_surf = false;

  IF_WRITE_SET(fnamebase, "unit_ball_in_neg0p1_0p3_x_neg0p2_0p4_x_0p3_0p4.txt");

  inquad_params inqparams = inquad_default_params;
  IF_WRITEQPTS_QUAD_OUTFNAME("quadpts_" + fnamebase);
  IF_WRITEQPTS_QUAD_NPTS(8);
  IF_TRACE_QUAD_NPTS(2);

  // The area of the intersection of the unit ball (enclosed by the
  // unit sphere) with the hyprect h = [-0.1, 0.3]x[-0.2, 0.4]x[0.3,
  // 0.4] is
  //
  // (0.3-(-0.1))*(0.4-(-0.2))*(0.4-0.3)
  // =
  // 0.024
  const unsigned dim = 3;
  const double h_xmins[] = {-0.1, -0.2, 0.3};
  const double h_xmaxs[] = { 0.3,  0.4, 0.4};

  const double	integral_value_relerr	= 1e-6;
  const double	integral_value_expected = 0.024;

  integrand_field   integrand_F;
  const_func_params integrand_F_params;
  integrand_F_params.c = 1.0;
  integrand_F.function = const_func;
  integrand_F.params   = &integrand_F_params;

  INVOKE_INQUAD_TEST;

  IF_WRITEVALS_SAVE_CONVERGENCE_AND_TIMINING_DATA(("values_" + fnamebase).c_str(),
						  4, 256, n);

}


TEST(inquad_test, inquad_vol_unit_ball_in_0_1p1_cubed)
{
  const int	sgn	= -1;
  const bool	is_surf = false;

  IF_WRITE_SET(fnamebase, "unit_ball_in_0_1p1_cubed.txt");

  inquad_params inqparams = inquad_default_params;
  inqparams.quad_npts = 64;
  IF_WRITEQPTS_QUAD_OUTFNAME("quadpts_" + fnamebase);
  IF_WRITEQPTS_QUAD_NPTS(8);
  IF_TRACE_QUAD_NPTS(2);

  // The volume of the intersection of the unit ball (enclosed by the
  // unit sphere) with the hyprect h = [0.0, 1.1]^3 is
  //
  // 4π/3/8 (1/2^3 of unit ball)
  // =
  // 0.52359877559829887307
  const unsigned dim = 3;
  const double h_xmins[] = { 0.0,  0.0,  0.0};
  const double h_xmaxs[] = { 1.1,  1.1,  1.1};

  const double	integral_value_relerr	= 1e-6;
  const double	integral_value_expected = 0.52359877559829887307;

  integrand_field   integrand_F;
  const_func_params integrand_F_params;
  integrand_F_params.c = 1.0;
  integrand_F.function = const_func;
  integrand_F.params   = &integrand_F_params;

  INVOKE_INQUAD_TEST;

  IF_WRITEVALS_SAVE_CONVERGENCE_AND_TIMINING_DATA(("values_" + fnamebase).c_str(),
						  4, 512, n);
}


TEST(inquad_test, inquad_vol_unit_ball_in_neg1p1_1p1_cubed)
{
  const int	sgn	= -1;
  const bool	is_surf = false;

  IF_WRITE_SET(fnamebase, "unit_ball_in_neg1p1_1p1_cubed.txt");

  inquad_params inqparams = inquad_default_params;
  inqparams.quad_npts = 64;
  IF_WRITEQPTS_QUAD_OUTFNAME("quadpts_" + fnamebase);
  IF_WRITEQPTS_QUAD_NPTS(4);
  IF_TRACE_QUAD_NPTS(2);

  // The volume of the intersection of the unit ball (enclosed by the
  // unit sphere) with the hyprect h = [0.0, 1.1]^3 is
  //
  // 4π/3 (whole unit ball)
  // =
  // 4.18879020478639098458
  const unsigned dim = 3;
  const double h_xmins[] = {-1.1, -1.1, -1.1};
  const double h_xmaxs[] = { 1.1,  1.1,  1.1};

  const double	integral_value_relerr	= 1e-6;
  const double	integral_value_expected = 4.18879020478639098458;

  integrand_field   integrand_F;
  const_func_params integrand_F_params;
  integrand_F_params.c = 1.0;
  integrand_F.function = const_func;
  integrand_F.params   = &integrand_F_params;

  INVOKE_INQUAD_TEST;

  IF_WRITEVALS_SAVE_CONVERGENCE_AND_TIMINING_DATA(("values_" + fnamebase).c_str(),
						  4, 512, n);
}


TEST(inquad_test, inquad_vol_unit_ball_in_neg1p1_1p1_d)
{
  const int	sgn	= -1;
  const bool	is_surf = false;

  IF_WRITE_SET(fnamebase, "unit_ball_in_neg1p1_1p1.txt");

  inquad_params inqparams = inquad_default_params;
  inqparams.quad_npts = 64;
  IF_WRITEQPTS_QUAD_OUTFNAME("quadpts_" + fnamebase);
  IF_WRITEQPTS_QUAD_NPTS(4);
  IF_TRACE_QUAD_NPTS(2);

  integrand_field   integrand_F;
  const_func_params integrand_F_params;
  integrand_F_params.c = 1.0;
  integrand_F.function = const_func;
  integrand_F.params   = &integrand_F_params;

  const unsigned dim_max = 7 + 1;
  for (unsigned dim = 3; dim < dim_max; ++dim) {

    double *h_xmins = (double *) malloc(dim * sizeof(double));
    double *h_xmaxs = (double *) malloc(dim * sizeof(double));

    for (unsigned i = 0; i < dim; ++i) {
      h_xmins[i] = -1.1;
      h_xmaxs[i] =  1.1;
    }

    double integral_value_expected = vol_ball(1.0, (double)dim);

    IF_WRITEVALS_SAVE_CONVERGENCE_AND_TIMINING_DATA(("values_"
						     + std::to_string(dim)
						     + "_"
						     + fnamebase).c_str(),
						    4, 8, n);

    free(h_xmins);
    free(h_xmaxs);
  }
}
