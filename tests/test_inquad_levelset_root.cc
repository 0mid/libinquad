// test_inquad_levelset_root.cc ---

// Copyright (C) 2017 Omid Khanmohamadi

// Author: Omid Khanmohamadi

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "src/inquad_levelset_root.h" // for levelset_find_roots_...
#include "src/inquad_levelset_height_function.h" // for levelset_duplicate_pts_...
#include "src/bit_util.h"	// for idx_least_sig_0_bit
#include "src/array_util.h"	// for darray_{alloc,free}, NELEMS
#include "src/sort_util.h"	// for darray_qsort

#include "tst_functions.h"	// for d_unit_sphere

#include "gsl/gsl_math.h"	// for M_constants
#include "gtest/gtest.h"	// for Google Test framework

static const int sgn = -1;

TEST(inquad_levelset_root_test,
     levelset_find_roots_in_interval_along_dir_on_facets_one_per)
{

  inquad_params inqparams;
  inqparams.root_abserr			= 0;
  inqparams.root_relerr			= 1e-6;
  inqparams.root_interval_shrink_factor = 0.96;
  inqparams.root_nsubintervals		= 50;
  inqparams.root_maxniters		= 100;

  const unsigned dim = 2;
  const double h_xmins[] = {0.0, 0.5};
  const double h_xmaxs[] = {1.0, M_SQRT3/2};

  hyprect h;
  ASSERT_TRUE(SUCCESS
	      ==
	      hyprect_make_from_range(dim, h_xmins, h_xmaxs, &h));

  levelset phi;
  scalar_field phi_field; phi.field = &phi_field;
  // Among doing other things, this initialization allocates storage
  // for TWO points (but fills only one and sets npts to 1) when
  // TESTING.
  ASSERT_TRUE(SUCCESS
	      ==
	      levelset_init_all_but_grad_on_orig_hyprect(&phi, &h,
							 d_unit_sphere,
							 NULL,
							 sgn));

  EXPECT_EQ(phi.npts, 1);

  double *eval_pt;
  ASSERT_TRUE(SUCCESS
	      ==
	      darray_alloc(phi.dim, &eval_pt));

  double *roots;
  ASSERT_TRUE(SUCCESS
	      ==
	      darray_alloc(phi.npts, &roots));

  eval_pt[0] = M_SQRT1_2; // root to be found along line x = 1/sqrt(2)

  double	root_expected	= M_SQRT1_2; // sqrt(1/2) = 1/sqrt(2)
  unsigned	nroots_expected = 1;

  // Modifies phi->pts.
  levelset_subs_eval_pt_coords_in_unfrzn_coords_of_pts(&phi,
						       h.treated_dims,
						       eval_pt);

  unsigned dir = 1;		// x1 direction

  double interval_left_end  = h.center[dir] - h.halfwidths[dir];
  double interval_right_end = h.center[dir] + h.halfwidths[dir];

  root_situation rt_situation = AT_MOST_ONE_ROOT_PER_FACET;
  // Output in roots
  int nroots =
    levelset_find_roots_in_interval_along_dir_on_facets(&phi,
							interval_left_end,
							interval_right_end,
							dir,
							rt_situation,
							&inqparams,
							roots);

  double abserr = inqparams.root_relerr * fabs(root_expected);
  EXPECT_NEAR(roots[0], root_expected, abserr);
  EXPECT_EQ(nroots, nroots_expected);

  // ----------------------------------------------------------------------

  eval_pt[0] = 1/3; // root to be found along line x = 1/3 (no root there)
  nroots_expected = 0;

  // Modifies phi->pts.
  levelset_subs_eval_pt_coords_in_unfrzn_coords_of_pts(&phi,
						       h.treated_dims,
						       eval_pt);

  // Output in roots
  nroots =
    levelset_find_roots_in_interval_along_dir_on_facets(&phi,
							interval_left_end,
							interval_right_end,
							dir,
							rt_situation,
							&inqparams,
							roots);

  EXPECT_EQ(nroots, nroots_expected);

  // ----------------------------------------------------------------------

  eval_pt[0] = 1/2; // root to be found along line x = 1/2 (root coincides end pt)
  nroots_expected = 0;

  // Modifies phi->pts.
  levelset_subs_eval_pt_coords_in_unfrzn_coords_of_pts(&phi,
						       h.treated_dims,
						       eval_pt);

  // Output in roots
  nroots =
    levelset_find_roots_in_interval_along_dir_on_facets(&phi,
							interval_left_end,
							interval_right_end,
							dir,
							rt_situation,
							&inqparams,
							roots);

  EXPECT_EQ(nroots, nroots_expected);

  // ----------------------------------------------------------------------

  // Root to be found along line x = sqrt(3)/2. The root and the end
  // point (1/2) coincide in infinite-precision arithmetic, but not in
  // finite-precision (floating-point) arithmetic.
  eval_pt[0] = M_SQRT3/2;

  root_expected	  = 0.5;
  nroots_expected = 1;

  // Modifies phi->pts.
  levelset_subs_eval_pt_coords_in_unfrzn_coords_of_pts(&phi,
						       h.treated_dims,
						       eval_pt);

  // Output in roots
  nroots =
    levelset_find_roots_in_interval_along_dir_on_facets(&phi,
							interval_left_end,
							interval_right_end,
							dir,
							rt_situation,
							&inqparams,
							roots);

  abserr = inqparams.root_relerr * fabs(root_expected);
  EXPECT_NEAR(roots[0], root_expected, abserr);
  EXPECT_EQ(nroots, nroots_expected);

  darray_free(roots); // We need a two-element roots for next section.

  // ----------------------------------------------------------------------

  hyprect_mark_dim_as_treated(&h, dir);

  // The following call doubles the number of points at the end.
  levelset_duplicate_pts_and_fill_in_new_frzn_coord(&phi, dir, &h);

  EXPECT_EQ(phi.npts, 2);

  unsigned dir_final = idx_least_sig_0_bit(h.treated_dims); // x0 direction


  interval_left_end  = h.center[dir_final] - h.halfwidths[dir_final];
  interval_right_end = h.center[dir_final] + h.halfwidths[dir_final];

  ASSERT_TRUE(SUCCESS
	      ==
	      darray_alloc(phi.npts, &roots));

  // Output in roots.
  nroots =
    levelset_find_roots_in_interval_along_dir_on_facets(&phi,
							interval_left_end,
							interval_right_end,
							dir_final,
							rt_situation,
							&inqparams,
							roots);

  double roots_expected[] = {M_SQRT3/2, 0.5};

  for (unsigned j = 0; j < phi.npts; ++j) {
    double root_j_expected = roots_expected[j];
    double abserr = inqparams.root_relerr * fabs(root_j_expected);
    EXPECT_NEAR(roots[j],
		root_j_expected, abserr);
  }

  EXPECT_EQ(nroots, NELEMS(roots_expected));

  hyprect_free(&h);
  levelset_free_pts(&phi);
  darray_free(eval_pt);
  darray_free(roots);
}


TEST(inquad_levelset_root_test,
     levelset_find_roots_in_interval_along_dir_on_facets_several_per)
{

  inquad_params inqparams;
  inqparams.root_abserr			= 0;
  inqparams.root_relerr			= 1e-6;
  inqparams.root_interval_shrink_factor = 0.96;
  inqparams.root_nsubintervals		= 50;
  inqparams.root_maxniters		= 100;

  const unsigned dim = 2;
  const double h_xmins[] = {-1.1, 0.0};
  const double h_xmaxs[] = { 1.1, 1.1};

  hyprect h;
  ASSERT_TRUE(SUCCESS
	      ==
	      hyprect_make_from_range(dim, h_xmins, h_xmaxs, &h));

  levelset phi;
  scalar_field phi_field; phi.field = &phi_field;
  // Among doing other things, this initialization allocates storage
  // for TWO points (but fills only one and sets npts to 1) when
  // TESTING.
  ASSERT_TRUE(SUCCESS
	      ==
	      levelset_init_all_but_grad_on_orig_hyprect(&phi, &h,
							 d_unit_sphere,
							 NULL,
							 sgn));

  double *eval_pt;
  ASSERT_TRUE(SUCCESS
	      ==
	      darray_alloc(phi.dim, &eval_pt));

  unsigned nroots_expected  = 2;
  double   roots_expected[] = {-1.0, 1.0};

  double *roots;
  ASSERT_TRUE(SUCCESS
	      ==
	      darray_alloc(phi.npts * nroots_expected, &roots));

  unsigned dir = 1;		// x1 direction
  hyprect_mark_dim_as_treated(&h, dir);

  levelset_duplicate_pts_and_fill_in_new_frzn_coord(&phi, dir, &h);

  unsigned dir_final = idx_least_sig_0_bit(h.treated_dims); // x0 direction

  double interval_left_end  = h.center[dir_final] - h.halfwidths[dir_final];
  double interval_right_end = h.center[dir_final] + h.halfwidths[dir_final];

  root_situation rt_situation = MAY_HAVE_SEVERAL_ROOTS_PER_FACET;
  // Output in roots
  int nroots =
    levelset_find_roots_in_interval_along_dir_on_facets(&phi,
							interval_left_end,
							interval_right_end,
							dir_final,
							rt_situation,
							&inqparams,
							roots);

  for (unsigned j = 0; j < phi.npts; ++j) {
    double root_j_expected = roots_expected[j];
    double abserr = inqparams.root_relerr * fabs(root_j_expected);
    EXPECT_NEAR(roots[j],
		root_j_expected, abserr);
  }

  EXPECT_EQ(nroots, NELEMS(roots_expected));

  hyprect_free(&h);
  levelset_free_pts(&phi);
  darray_free(eval_pt);
  darray_free(roots);
}
