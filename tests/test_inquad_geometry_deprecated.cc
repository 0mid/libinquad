// test_inquad_geometry_deprecated.cc ---

// Copyright (C) 2016 Omid Khanmohamadi

// Author: Omid Khanmohamadi

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "src/inquad_geometry_deprecated.h"
#include "gtest/gtest.h"   // for Google Test framework

TEST(inquad_geometry_d_test, volume_d)
{
  double h1_center[]     = {0};
  double h1_halfwidths[] = {1};
  hyprect_d h1 = {
    h1_center,	// no & needed; array names "decay" to pointers (a.e.)
    h1_halfwidths,
    1,		// xdim
    0.0		// initialize volume to 0.0 till it's calculated.
  };
  double h1_vol = (2*1);


  double h2_center[]     = {-1, 1};
  double h2_halfwidths[] = {1,  2};
  hyprect_d h2 = {
    h2_center,
    h2_halfwidths,
    2,
    0.0
  };
  double h2_vol = (2*1)*(2*2);


  double h3_center[]     = {-1, 0, 1};
  double h3_halfwidths[] = {1, 2, 3};
  hyprect_d h3 = {
    h3_center,
    h3_halfwidths,
    3,
    0.0
  };
  double h3_vol = (2*1)*(2*2)*(2*3);

  // In general, for floating-point comparison to make sense, the user
  // needs to carefully choose the error bound. If they don't want or
  // care to, comparing in terms of Units in the Last Place (ULPs) is
  // a good default, and GTest provides assertions to do this. Full
  // details about ULPs are quite long; to learn more, see, e.g.,:
  // http://www.cygnus-software.com/papers/comparingfloats/comparingfloats.htm
  EXPECT_DOUBLE_EQ(h1_vol, volume_d(&h1));
  EXPECT_DOUBLE_EQ(h2_vol, volume_d(&h2));
  EXPECT_DOUBLE_EQ(h3_vol, volume_d(&h3));
}


TEST(inquad_geometry_d_test, make_hyprect_d)
{
  unsigned h2_dim	   = 2;
  double   h2_center[]	   = {-1, 1};
  double   h2_halfwidths[] = {1,  2};

  hyprect_d  h2; make_hyprect_d(2, h2_center, h2_halfwidths, &h2);

  unsigned h3_dim	   = 3;
  double   h3_center[]	   = {-1, 0, 1};
  double   h3_halfwidths[] = {1, 2, 3};

  hyprect_d  h3; make_hyprect_d(3, h3_center, h3_halfwidths, &h3);

  unsigned i;
  for (i = 0; i < h2_dim; ++i){
    EXPECT_DOUBLE_EQ(h2_center[i],
		     h2.center[i]);

    EXPECT_DOUBLE_EQ(h2_halfwidths[i],
		     h2.halfwidths[i]);
  }
  free_hyprect_d(&h2);

  for (i = 0; i < h3_dim; ++i){
    EXPECT_DOUBLE_EQ(h3_center[i],
		     h3.center[i]);

    EXPECT_DOUBLE_EQ(h3_center[i],
		     h3.center[i]);
  }
  free_hyprect_d(&h3);
}

TEST(inquad_geometry_d_test, make_hyprect_range_d)
{
  // If xmins[j]==0 for all j, then center[j]==halfwidths[j] for all j.
  unsigned h1_dim	 = 2;
  double   h1_xmins[]	 = {0, 0};
  double   h1_xmaxs[]	 = {1, 2};
  double   h1_center[]	 = {0.5, 1};
  double  *h1_halfwidths = h1_center;

  hyprect_d  h1; make_hyprect_range_d(h1_dim, h1_xmins, h1_xmaxs, &h1);

  unsigned h2_dim	   = 3;
  double   h2_xmins[]	   = {-1, 5,   -20};
  double   h2_xmaxs[]	   = { 1, 8,    -5};
  double   h2_center[]	   = {0,  6.5, -12.5};
  double   h2_halfwidths[] = {1,  1.5,   7.5};

  hyprect_d  h2; make_hyprect_range_d(h2_dim, h2_xmins, h2_xmaxs, &h2);

  unsigned i;
  for (i = 0; i < h1_dim; ++i){
    EXPECT_DOUBLE_EQ(h1_center[i],
		     h1.center[i]);

    EXPECT_DOUBLE_EQ(h1_halfwidths[i],
		     h1.halfwidths[i]);
  }
  free_hyprect_d(&h1);

  for (i = 0; i < h2_dim; ++i){
    EXPECT_DOUBLE_EQ(h2_center[i],
		     h2.center[i]);

    EXPECT_DOUBLE_EQ(h2_halfwidths[i],
		     h2.halfwidths[i]);
  }
  free_hyprect_d(&h2);

}

TEST(inquad_geometry_d_test, make_region_d)
{
  unsigned h1_dim	   = 3;
  double   h1_xmins[]	   = {-1, 5,   -20};
  double   h1_xmaxs[]	   = {1,  8,    -5};
  double   h1_center[]	   = {0,  6.5, -12.5};
  double   h1_halfwidths[] = {1,  1.5,   7.5};

  hyprect_d  h1; make_hyprect_range_d(h1_dim, h1_xmins, h1_xmaxs, &h1);

  unsigned R1_fdim = 4;
  region_d R1; make_region_d(&h1, R1_fdim, &R1);

  for (unsigned i = 0; i < h1_dim; ++i){
    EXPECT_DOUBLE_EQ(   h1_center[i],
		     R1.h->center[i]);

    EXPECT_DOUBLE_EQ(   h1_halfwidths[i],
		     R1.h->halfwidths[i]);
  }

  EXPECT_EQ(R1_fdim,
	    R1.fdim);

  free_region_d(&R1);    // also frees associated hyprect_d (R1.h)
}

TEST(inquad_geometry_d_test, halve_hyprect_d)
{
  unsigned h1_dim	   = 3;
  double   h1_xmins[]	   = {-1, 5, -20};
  double   h1_xmaxs[]	   = {1, 11,  -6};
  // So,   h1_center[]     = {0,  8, -13};
  // and   h1_halfwidths[] = {1,  3,   7};
  unsigned h1_splitdim     = 2;	// == argmax(h1.halfwidths, h1.xdim)

  hyprect_d  h1; make_hyprect_range_d(h1_dim, h1_xmins, h1_xmaxs, &h1);

  double   h1_new_halfwidths[] = {1, 3,       0.5*7};
  double  *h2_new_halfwidths   = h1_new_halfwidths;
  double   h1_new_center[]     = {0, 8, -13 - 0.5*7};
  double   h2_new_center[]     = {0, 8, -13 + 0.5*7};

  hyprect_d h2; make_hyprect_d(h1.xdim, h1.center, h1.halfwidths, &h2);

  halve_hyprect_d(&h1, &h2, h1_splitdim);

  for (unsigned i = 0; i < h1_dim; ++i){
    EXPECT_DOUBLE_EQ(h1_new_center[i],
			 h1.center[i]);

    EXPECT_DOUBLE_EQ(h1_new_halfwidths[i],
			 h1.halfwidths[i]);

    EXPECT_DOUBLE_EQ(h2_new_center[i],
			 h2.center[i]);

    EXPECT_DOUBLE_EQ(h2_new_halfwidths[i],
			 h2.halfwidths[i]);
  }

  free_hyprect_d(&h1);
  free_hyprect_d(&h2);
}

TEST(inquad_geometry_d_test, halve_region_d)
{
  unsigned h1_dim	   = 3;
  double   h1_xmins[]	   = {-1, 5, -20};
  double   h1_xmaxs[]	   = {1, 11,  -6};
  // So,   h1_center[]     = {0,  8, -13};
  // and   h1_halfwidths[] = {1,  3,   7};
  unsigned R1_splitdim     = 2;	// == argmax(h1.halfwidths, h1.xdim)
  hyprect_d  h1; make_hyprect_range_d(h1_dim, h1_xmins, h1_xmaxs, &h1);

  double   h1_new_halfwidths[] = {1, 3,       0.5*7};
  double  *h2_new_halfwidths   = h1_new_halfwidths;
  double   h1_new_center[]     = {0, 8, -13 - 0.5*7};
  double   h2_new_center[]     = {0, 8, -13 + 0.5*7};

  unsigned R1_fdim, R2_fdim;
  R1_fdim = R2_fdim = 4;

  region_d R1; make_region_d(&h1, R1_fdim, &R1);

  hyprect_d h2; make_hyprect_d(h1.xdim, h1.center, h1.halfwidths, &h2);
  region_d  R2; make_region_d(&h2, R1.fdim, &R2);

  halve_region_d(&R1, &R2, R1_splitdim);

  for (unsigned i = 0; i < h1_dim; ++i){
    EXPECT_DOUBLE_EQ(h1_new_center[i],
		      R1.h->center[i]);

    EXPECT_DOUBLE_EQ(h1_new_halfwidths[i],
		      R1.h->halfwidths[i]);

    EXPECT_DOUBLE_EQ(h2_new_center[i],
		      R2.h->center[i]);

    EXPECT_DOUBLE_EQ(h2_new_halfwidths[i],
		      R2.h->halfwidths[i]);
  }

  EXPECT_EQ(R1_fdim,
	    R1.fdim);

  EXPECT_EQ(R2_fdim,
	    R2.fdim);

  // Although the struct R1 'itself' is on the stack (and is popped
  // automatically when this test exits), it has members that are
  // pointers to heap-allocated memory. As a result, we DO need to
  // de-allocate those members 'manually'. This is done by using
  // free_region_d, which de-allocates the hyprect_d and the valerr
  // array (ve) associated with R1.
  free_region_d(&R1);

  // Similarly for R2.
  free_region_d(&R2);
}

TEST(inquad_geometry_d_test, reduce_hyprect_d)
{
  unsigned h1_dim	   = 3;
  double   h1_xmins[]	   = {-1, 5, -20};
  double   h1_xmaxs[]	   = {1, 11,  -6};
  // So,   h1_center[]     = {0,  8, -13};
  // and   h1_halfwidths[] = {1,  3,   7};
  hyprect_d  h1; make_hyprect_range_d(h1_dim, h1_xmins, h1_xmaxs, &h1);

  unsigned h1_new_dim = h1_dim - 1;
  double h1_new_center[]     = {8, -13,
				0, -13,
				0,   8};
  double h1_new_halfwidths[] = {3, 7,
				1, 7,
				1, 3};

  unsigned rmdim = 2;
  reduce_hyprect_d(&h1, rmdim);

  for (unsigned i = 0; i < h1_new_dim; ++i){
    unsigned idx = rmdim*h1_new_dim + i;
    EXPECT_DOUBLE_EQ(h1_new_center[idx],
			 h1.center[i]);

    EXPECT_DOUBLE_EQ(h1_new_halfwidths[idx],
			 h1.halfwidths[i]);
  }

  free_hyprect_d(&h1);
}

TEST(inquad_geometry_d_test, reduce_region_d)
{
  unsigned h1_dim	   = 3;
  double   h1_xmins[]	   = {-1, 5, -20};
  double   h1_xmaxs[]	   = {1, 11,  -6};
  // So,   h1_center[]     = {0,  8, -13};
  // and   h1_halfwidths[] = {1,  3,   7};
  hyprect_d  h1; make_hyprect_range_d(h1_dim, h1_xmins, h1_xmaxs, &h1);

  unsigned R1_fdim  = 4;
  region_d R1; make_region_d(&h1, R1_fdim, &R1);

  unsigned h1_new_dim = h1_dim - 1;
  double h1_new_center[]     = {8, -13,
				0, -13,
				0,   8};
  double h1_new_halfwidths[] = {3, 7,
				1, 7,
				1, 3};

  unsigned rmdim = 2;
  reduce_region_d(&R1, rmdim);

  for (unsigned i = 0; i < h1_new_dim; ++i){
    unsigned idx = rmdim*h1_new_dim + i;
    EXPECT_DOUBLE_EQ(h1_new_center[idx],
		      R1.h->center[i]);

    EXPECT_DOUBLE_EQ(h1_new_halfwidths[idx],
		      R1.h->halfwidths[i]);
  }

  free_region_d(&R1);
}
