// test_sort_util.cc ---

// Copyright (C) 2017 Omid Khanmohamadi

// Author: Omid Khanmohamadi

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "src/array_util.h"	// for NELEMS
#include "src/sort_util.h"	// for darray_qsort

#include "gtest/gtest.h"	// for Google Test framework

TEST(sort_util_test, darray_qsort)
{
  double x[]		  = {4, 5, 2, 3, 1, 0, 9, 8, 6, 7};
  const double x_sorted[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};

  const unsigned dim = NELEMS(x);

  darray_qsort(dim, x);

  for (unsigned i = 0; i < dim; ++i) {
    EXPECT_EQ(x[i], x_sorted[i]);
  }
}
