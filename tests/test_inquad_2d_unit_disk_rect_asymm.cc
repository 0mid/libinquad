// test_inquad_2d_unit_disk_rect_asymm.cc ---

// Copyright (C) 2017 Omid Khanmohamadi

// Author: Omid Khanmohamadi

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "src/inquad.h"
#include "tst_functions.h"
#include "tst_cond_compile.h"
#include "tst_invoke_macro.h"	// for INVOKE_INQUAD_TEST

#include "gsl/gsl_math.h"	// for M_constants
#include "gtest/gtest.h"	// for Google Test framework


TEST(inquad_test, inquad_area_unit_disk_in_0_1_x_0p5_sqrt3over2)
{
  const int	sgn	= -1;
  const bool	is_surf = false;

  IF_WRITE_SET(fnamebase, "unit_disk_in_0_1_x_0p5_sqrt3over2.txt");

  inquad_params inqparams = inquad_default_params;
  IF_WRITEQPTS_QUAD_OUTFNAME("quadpts_" + fnamebase);
  IF_TRACE_QUAD_NPTS(2);

  // The area of the intersection of the unit disk (enclosed by the
  // unit circle) with the hyprect h = [0, 1]x[1/2, √3/2] is
  //
  // π/12 (s, π/6 sector of unit disk)
  // -
  // 0.5(√3/2-1/2√3)(1/2) (triangle part of s outside h)
  // +
  // 0.5(√3/2-1/2)(1/2√3+1/2) (trapezoid between the y axis and s)
  // =
  // 0.26179938779914943653
  const unsigned dim = 2;
  const double h_xmins[] = {0.0, 0.5};
  const double h_xmaxs[] = {1.0, M_SQRT3/2};

  const double	integral_value_relerr	= 1e-10;
  const double	integral_value_expected = 0.26179938779914943653;

  integrand_field   integrand_F;
  const_func_params integrand_F_params;
  integrand_F_params.c = 1.0;
  integrand_F.function = const_func;
  integrand_F.params   = &integrand_F_params;

  INVOKE_INQUAD_TEST;

  IF_WRITEVALS_SAVE_CONVERGENCE_AND_TIMINING_DATA(("values_" + fnamebase).c_str(),
						  2, 256, n);
}


TEST(inquad_test, inquad_area_unit_disk_in_0_1_x_0p5_sqrt3over2_x0_func)
{
  const int	sgn	= -1;
  const bool	is_surf = false;

  IF_WRITE_SET(fnamebase, "unit_disk_in_0_1_x_0p5_sqrt3over2_x0_func.txt");

  inquad_params inqparams = inquad_default_params;
  IF_WRITEQPTS_QUAD_OUTFNAME("quadpts_" + fnamebase);
  IF_TRACE_QUAD_NPTS(2);

  // The weight of the intersection of the unit disk (enclosed by the
  // unit circle) with the hyprect h = [0, 1]x[1/2, √3/2], given the
  // surface density function ρ(x):=x₀, is
  // ∫[x=0 to 1/2]∫[y=1/2 to √3/2]xdxdy
  // +
  // ∫[x=1/2 to √3/2]∫[y=1/2 to √(1-x^2)]xdydx
  // =
  // (√3 - 1)/16
  // +
  // (3√3 - 4)/24
  // =
  // (9√3 - 11)/48
  // ≈
  // 0.09559285975249782586
  const unsigned dim = 2;
  const double h_xmins[] = {0.0, 0.5};
  const double h_xmaxs[] = {1.0, M_SQRT3/2};

  const double	integral_value_relerr	= 1e-9;
  const double	integral_value_expected = 0.09559285975249782586;

  integrand_field   integrand_F;
  const_func_params integrand_F_params;
  integrand_F.function = x0_func;
  integrand_F.params   = &integrand_F_params;

  INVOKE_INQUAD_TEST;

  IF_WRITEVALS_SAVE_CONVERGENCE_AND_TIMINING_DATA(("values_" + fnamebase).c_str(),
						  2, 256, n);
}


TEST(inquad_test, inquad_area_unit_disk_out_0_1_x_0p5_sqrt3over2)
{
  const int	sgn	= 1;
  const bool	is_surf = false;

  IF_WRITE_SET(fnamebase, "unit_disk_out_0_1_x_0p5_sqrt3over2.txt");

  inquad_params inqparams = inquad_default_params;
  IF_WRITEQPTS_QUAD_OUTFNAME("quadpts_" + fnamebase);
  IF_TRACE_QUAD_NPTS(2);

  // The area of the intersection of the outside of the unit disk
  // (enclosed by the unit circle) with the hyprect h = [0, 1]x[1/2,
  // √3/2] is
  //
  // (1-0)(√3/2-1/2) (whole rectangle)
  // -
  // (
  // π/12 (s, π/6 sector of unit disk)
  // -
  // 0.5(√3/2-1/2√3)(1/2) (triangle part of s outside h)
  // +
  // 0.5(√3/2-1/2)(1/2√3+1/2) (trapezoid between the y axis and s)
  // )
  // =
  // 0.36602540378443864676 - 0.26179938779914943653
  // =
  // 0.10422601598528921023
  const unsigned dim = 2;
  const double h_xmins[] = {0.0, 0.5};
  const double h_xmaxs[] = {1.0, M_SQRT3/2};

  const double	integral_value_relerr	= 1e-10;
  const double	integral_value_expected = 0.10422601598528921023;

  integrand_field   integrand_F;
  const_func_params integrand_F_params;
  integrand_F_params.c = 1.0;
  integrand_F.function = const_func;
  integrand_F.params   = &integrand_F_params;

  INVOKE_INQUAD_TEST;

  IF_WRITEVALS_SAVE_CONVERGENCE_AND_TIMINING_DATA(("values_" + fnamebase).c_str(),
						  2, 512, n);
}


TEST(inquad_test, inquad_area_unit_disk_in_neg0p5_2_x_neg1p6_1p3)
{
  const int		sgn		    = -1;
  const bool		is_surf		    = false;

  IF_WRITE_SET(fnamebase, "unit_disk_in_neg0p5_2_x_neg1p6_1p3.txt");

  inquad_params inqparams = inquad_default_params;
  inqparams.quad_npts = 16;
  IF_WRITEQPTS_QUAD_OUTFNAME("quadpts_" + fnamebase);
  IF_WRITEQPTS_QUAD_NPTS(8);
  IF_TRACE_QUAD_NPTS(2);

  // The area of the intersection of the unit disk (enclosed by the
  // unit circle) with the hyprect h = [-0.5, 2]x[-1.6, 1.3] is
  //
  // π/2 (half of the unit disk)
  // +
  // 2((1/2)(π/6)(1)^2 + (1/2)(1/2)(√3/2))
  // =
  // 2.52740780428541481567
  const unsigned dim = 2;
  const double h_xmins[] = {-0.5, -1.6};
  const double h_xmaxs[] = { 2.0,  1.3};

  const double	integral_value_relerr	= 1e-6;
  const double	integral_value_expected = 2.52740780428541481567;

  integrand_field   integrand_F;
  const_func_params integrand_F_params;
  integrand_F_params.c = 1.0;
  integrand_F.function = const_func;
  integrand_F.params   = &integrand_F_params;

  INVOKE_INQUAD_TEST;

  IF_WRITEVALS_SAVE_CONVERGENCE_AND_TIMINING_DATA(("values_" + fnamebase).c_str(),
						  2, 256 , n);
}
