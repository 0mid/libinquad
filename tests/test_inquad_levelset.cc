// test_inquad_levelset.cc ---

// Copyright (C) 2017 Omid Khanmohamadi

// Author: Omid Khanmohamadi

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "src/inquad_levelset.h" // for levelset_{copy, init, etc.}
#include "src/inquad_geometry.h" // for hyprect
#include "src/bit_util.h"	 // for bit_pair_to_sgn

#include "tst_functions.h"	// for d_unit_sphere

#include "gtest/gtest.h"	// for Google Test framework

#include <stdlib.h>		// for NULL

TEST(inquad_levelset_test, levelset_alloc_free_pts)
{
  unsigned	dim  = 2;
  unsigned	npts = 6;

  levelset phi;
  ASSERT_TRUE(SUCCESS
	      ==
	      levelset_alloc_pts(&phi, dim, npts));

  levelset_free_pts(&phi);
}


TEST(inquad_levelset_test, levelset_init_pts_on_orig_hyprect)
{
  const unsigned dim = 2;
  const double h_xmins[] = {0.0, 0.5};
  const double h_xmaxs[] = {1.0, 1.5};

  hyprect h;
  ASSERT_TRUE(SUCCESS
	      ==
	      hyprect_make_from_range(dim, h_xmins, h_xmaxs, &h));

  levelset phi;

  // Among doing other things, this initialization allocates storage
  // for TWO points (but fills only one and sets npts to 1) when
  // TESTING.
  ASSERT_TRUE(SUCCESS
	      ==
	      levelset_init_pts_on_orig_hyprect(&phi,
						&h));

  EXPECT_EQ(phi.npts, 1);
  EXPECT_EQ(phi.dim,  h.dim);

  hyprect_free(&h);
  levelset_free_pts(&phi);
}

static const int sgn = -1;

TEST(inquad_levelset_test, levelset_init_all_but_grad_on_orig_hyprect)
{
  const unsigned dim = 2;
  const double h_xmins[] = {0.0, 0.5};
  const double h_xmaxs[] = {1.0, 1.5};

  hyprect h;
  ASSERT_TRUE(SUCCESS
	      ==
	      hyprect_make_from_range(dim, h_xmins, h_xmaxs, &h));

  levelset phi;
  scalar_field	phi_field;
  vector_field	phi_grad_field;
  phi.field      = &phi_field;
  phi.grad_field = &phi_grad_field;

  // Among doing other things, this initialization allocates storage
  // for TWO points (but fills only one and sets npts to 1) when
  // TESTING.
  ASSERT_TRUE(SUCCESS
	      ==
	      levelset_init_all_but_grad_on_orig_hyprect(&phi,
							 &h,
							 d_unit_sphere,
							 NULL,
							 sgn));

  EXPECT_EQ(phi.npts, 1);
  EXPECT_EQ(phi.dim,  h.dim);
  EXPECT_EQ(phi.field->function, d_unit_sphere);

  // Cannot use NULL here, as 'ISO C++ forbids comparison between
  // pointer (LHS) and integer (NULL).
  EXPECT_EQ(phi.field->params, nullptr);

  EXPECT_EQ(-1,
	    bit_pair_to_sgn(phi.sgns_neutral, phi.sgns_posneg, 0));

  hyprect_free(&h);
  levelset_free_pts(&phi);
}


TEST(inquad_levelset_test, levelset_init_on_orig_hyprect)
{
  const unsigned dim = 2;
  const double h_xmins[] = {0.0, 0.5};
  const double h_xmaxs[] = {1.0, 1.5};

  hyprect h;
  ASSERT_TRUE(SUCCESS
	      ==
	      hyprect_make_from_range(dim, h_xmins, h_xmaxs, &h));

  levelset	phi;
  scalar_field	phi_field;
  vector_field	phi_grad_field;
  phi.field	 = &phi_field;
  phi.grad_field = &phi_grad_field;

  // Among doing other things, this initialization allocates storage
  // for TWO points (but fills only one and sets npts to 1) when
  // TESTING.
  ASSERT_TRUE(SUCCESS
	      ==
	      levelset_init_on_orig_hyprect(&phi,
					    &h,
					    d_unit_sphere,
					    NULL,
					    grad_d_unit_sphere,
					    NULL,
					    sgn));

  EXPECT_EQ(phi.npts, 1);
  EXPECT_EQ(phi.dim,  h.dim);
  EXPECT_EQ(phi.field->function,           d_unit_sphere);
  EXPECT_EQ(phi.grad_field->function, grad_d_unit_sphere);

  // Cannot use NULL here, as 'ISO C++ forbids comparison between
  // pointer (LHS) and integer (NULL).
  EXPECT_EQ(phi.field->params,	    nullptr);
  EXPECT_EQ(phi.grad_field->params, nullptr);

  EXPECT_EQ(-1,
	    bit_pair_to_sgn(phi.sgns_neutral, phi.sgns_posneg, 0));

  hyprect_free(&h);
  levelset_free_pts(&phi);
}


TEST(inquad_levelset_test, levelset_copy_no_sgns_w_twice_storage_for_pts)
{
  const unsigned dim = 2;
  const double h_xmins[] = {0.0, 0.5};
  const double h_xmaxs[] = {1.0, 1.5};

  hyprect h;
  ASSERT_TRUE(SUCCESS
	      ==
	      hyprect_make_from_range(dim, h_xmins, h_xmaxs, &h));

  levelset	phi;
  scalar_field	phi_field;
  vector_field	phi_grad_field;
  phi.field	 = &phi_field;
  phi.grad_field = &phi_grad_field;

  // Among doing other things, this initialization allocates storage
  // for TWO points (but fills only one and sets npts to 1) when
  // TESTING.
  ASSERT_TRUE(SUCCESS
	      ==
	      levelset_init_on_orig_hyprect(&phi,
					    &h,
					    d_unit_sphere,
					    NULL,
					    grad_d_unit_sphere,
					    NULL,
					    sgn));


  levelset	phi_new;
  scalar_field	phi_new_field;      phi_new.field      = &phi_new_field;
  vector_field	phi_new_grad_field; phi_new.grad_field = &phi_new_grad_field;

  // Although we allocate double the amount of storage for points in
  // phi_new as that of phi, we set the number of points, npts, in the
  // former to be equal to that of the latter.
  ASSERT_TRUE(SUCCESS
	      ==
	      levelset_copy_no_sgns(&phi_new,
				    &phi));
  EXPECT_EQ(phi_new.npts, phi.npts);


  levelset	phi_new_2;
  scalar_field	phi_new_2_field;
  phi_new_2.field = &phi_new_2_field;

  vector_field  phi_new_2_grad_field;
  phi_new_2.grad_field = &phi_new_2_grad_field;

  ASSERT_TRUE(SUCCESS
	      ==
	      levelset_copy_no_sgns_w_twice_storage_for_pts(&phi_new_2,
							    &phi));

  EXPECT_EQ(phi_new_2.npts, phi.npts);

  hyprect_free(&h);
  levelset_free_pts(&phi);
  levelset_free_pts(&phi_new);
  levelset_free_pts(&phi_new_2);
}
