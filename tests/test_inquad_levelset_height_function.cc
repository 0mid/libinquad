// test_inquad_levelset_height_function.cc ---

// Copyright (C) 2016 Omid Khanmohamadi

// Author: Omid Khanmohamadi

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "src/inquad_levelset.h"
#include "src/inquad_levelset_height_function.h"
#include "src/return_code.h"
#include "src/norm2.h"
#include "tst_functions.h"
#include "src/array_util.h"	// for NELEMS

#include "gtest/gtest.h"	// for Google Test framework

#include <math.h>		// for sqrt


static const int sgn_negative = -1;

TEST(inquad_levelset_height_function_test, levelset_eval_grad_on_facet)
{

  const unsigned dim = 2;
  double xmins[] = {-2, -2};
  double xmaxs[] = { 2,  2};

  hyprect h;
  ASSERT_TRUE(SUCCESS
	      ==
	      hyprect_make_from_range(dim, xmins, xmaxs, &h));

  unsigned dim_treated = 1;	// x_1, corresponding to 'y direction'

  hyprect_mark_dim_as_treated(&h, dim_treated);

  double x[] = {1, -2}; // (identifying) facet lying on y = -2

  levelset phi;
  vector_field	phi_grad_field; phi.grad_field = &phi_grad_field;
  phi.dim = h.dim;

  phi.grad_field->function = grad_d_unit_sphere;

  ASSERT_TRUE(SUCCESS
	      ==
	      levelset_alloc_grad_workspace(&phi));

  levelset_eval_grad_on_facet(&phi, x, h.treated_dims);

  double r_expected = norm2(x, dim);
  double grad_d_unit_sphere_expected[] = {x[0]/r_expected,
					0};
  for (unsigned i = 0; i < dim; ++i) {
    EXPECT_DOUBLE_EQ(phi.grad_value[i], grad_d_unit_sphere_expected[i]);
  }

  levelset_free_grad_workspace(&phi);
  hyprect_free(&h);
}


TEST(inquad_levelset_height_function_test,
     levelset_partial_on_facet_from_grad)
{

  const unsigned dim	     = 3;
  double   xmins[]	     = {-2, -3, -4};
  double   xmaxs[]	     = { 1,  3,  3};

  hyprect h;
  ASSERT_TRUE(SUCCESS
	      ==
	      hyprect_make_from_range(dim, xmins, xmaxs, &h));

  unsigned dim_treated = 2;	// x_2, corresponding to 'z direction'
  hyprect_mark_dim_as_treated(&h, dim_treated);

  double x[] = {-0.5, 0.0, -4}; // (identifying) facet lying on z = -4

  levelset phi;
  vector_field	phi_grad_field; phi.grad_field = &phi_grad_field;
  phi.dim = h.dim;

  phi.grad_field->function = grad_d_unit_sphere;

  ASSERT_TRUE(SUCCESS
	      ==
	      levelset_alloc_grad_workspace(&phi));

  unsigned k = 0;
  double D_0_d_unit_sphere =
    levelset_partial_on_facet_from_grad(&phi, x, k, h.treated_dims);

  double r_expected = norm2(x, dim);
  double D_0_d_unit_sphere_expected = x[0]/r_expected;

  EXPECT_DOUBLE_EQ(D_0_d_unit_sphere, D_0_d_unit_sphere_expected);

  levelset_free_grad_workspace(&phi);
  hyprect_free(&h);
}


static const bool is_surf = false;

TEST(inquad_levelset_height_function_test,
     levelset_direction_gives_height_function_2d)
{

  const unsigned dim = 2;
  const double h1_xmins[] = {-0.5,  0.5};
  const double h1_xmaxs[] = { 0.5, 10.0};

  hyprect h1;
  ASSERT_TRUE(SUCCESS
	      ==
	      hyprect_make_from_range(dim, h1_xmins, h1_xmaxs, &h1));

  levelset	phi;
  scalar_field	phi_field; phi.field	       = &phi_field;
  vector_field	phi_grad_field; phi.grad_field = &phi_grad_field;
  ASSERT_TRUE(SUCCESS
	      ==
	      levelset_init_all_but_grad_on_orig_hyprect(&phi, &h1,
							 d_unit_sphere,
							 NULL,
							 sgn_negative));

  phi.grad_field->function = grad_d_unit_sphere;

  ASSERT_TRUE(SUCCESS
	      ==
	      levelset_alloc_grad_workspace(&phi));

  // The equation sqrt(x0^2 + x1^2) = 1 DOES implicitly express x1 as
  // a function of x0 in a neighborhood of the center of the square
  // [-0.5,0.5]x[0.5,10], and our algorithm can decide that.
  unsigned dir = 1;
  ullong partial_sgns_neutral, partial_sgns_posneg;
  EXPECT_TRUE(levelset_direction_gives_height_function(&phi, dir, &h1,
						       is_surf,
						       &partial_sgns_neutral,
						       &partial_sgns_posneg));

  // The equation sqrt(x0^2 + x1^2) = 1 DOES NOT implicitly express x0
  // as a function of x1 in a neighborhood of the center of the square
  // [-0.5,0.5]x[0.5,10], and our algorithm can decide that.
  dir = 0;
  EXPECT_FALSE(levelset_direction_gives_height_function(&phi, dir, &h1,
							is_surf,
							&partial_sgns_neutral,
							&partial_sgns_posneg));

  levelset_free_pts(&phi);
  hyprect_free(&h1);
  levelset_free_grad_workspace(&phi);
}


TEST(inquad_levelset_height_function_test,
     levelset_direction_gives_height_function_3d)
{

  const unsigned dim = 3;
  const double h1_xmins[] = {-0.5,  0.5, -1.0};
  const double h1_xmaxs[] = { 0.5, 10.0,  1.0};

  hyprect h1;
  ASSERT_TRUE(SUCCESS
	      ==
	      hyprect_make_from_range(dim, h1_xmins, h1_xmaxs, &h1));

  levelset phi;
  scalar_field	phi_field; phi.field	       = &phi_field;
  vector_field	phi_grad_field; phi.grad_field = &phi_grad_field;
  ASSERT_TRUE(SUCCESS
	      ==
	      levelset_init_all_but_grad_on_orig_hyprect(&phi, &h1,
							 d_unit_sphere,
							 NULL,
							 sgn_negative));

  phi.grad_field->function = grad_d_unit_sphere;

  ASSERT_TRUE(SUCCESS
	      ==
	      levelset_alloc_grad_workspace(&phi));

  // The equation sqrt(x0^2 + x1^2 + x2^2) = 1 DOES implicitly express
  // x1 as a function of x0 and x2 in a neighborhood of the center of
  // the cube [-0.5,0.5]x[0.5,10]x[-1,1], and our algorithm can decide
  // that, as D_1 φ has the same sign at the center and vertices of
  // the said cube.
  unsigned dir = 1;
  ullong partial_sgns_neutral, partial_sgns_posneg;
  EXPECT_TRUE(levelset_direction_gives_height_function(&phi, dir, &h1,
						       is_surf,
						       &partial_sgns_neutral,
						       &partial_sgns_posneg));

  // The equation sqrt(x0^2 + x1^2 + x2^2) = 1 DOES NOT implicitly
  // express x0 (or x2) in as a function of x1 and x2 (or x0 and x1)
  // in a neighborhood of the center of the cube above, and our
  // algorithm can decide that, as D_1 φ changes sign as we move
  // between the center and vertices of said cube.
  dir = 0;
  EXPECT_FALSE(levelset_direction_gives_height_function(&phi, dir, &h1,
							is_surf,
							&partial_sgns_neutral,
							&partial_sgns_posneg));

  dir = 2;
  EXPECT_FALSE(levelset_direction_gives_height_function(&phi, dir, &h1,
							is_surf,
							&partial_sgns_neutral,
							&partial_sgns_posneg));

  levelset_free_pts(&phi);

  // ----------------------------------------------------------------------

  const unsigned dim_treated = 1; // x1, corresponding to 'y direction'
  hyprect_mark_dim_as_treated(&h1, dim_treated);
  double cen_pts_of_facets_w_x1_treated[] = {0.0,  0.5, 0.0,  // x1 =  0.5
					     0.0, 10.0, 0.0}; // x1 = 10.0

  phi.pts  = cen_pts_of_facets_w_x1_treated;
  phi.npts = 2;

  dir = 0;
  EXPECT_FALSE(levelset_direction_gives_height_function(&phi, dir, &h1,
							is_surf,
							&partial_sgns_neutral,
							&partial_sgns_posneg));

  dir = 2;
  EXPECT_FALSE(levelset_direction_gives_height_function(&phi, dir, &h1,
							is_surf,
							&partial_sgns_neutral,
							&partial_sgns_posneg));

  hyprect_free(&h1);
  levelset_free_grad_workspace(&phi);
}


#define VERIFY_PT_DUPLICATION(phi)			\
  do {							\
							\
    unsigned	npts	    = (phi).npts;		\
    unsigned	pt_n_offset = dim*npts;			\
							\
    const double *	pt_j	 = (phi).pts;		\
    const double *	pt_j_dup = pt_j + pt_n_offset;	\
							\
    for (unsigned j = 0; j < npts; ++j) {		\
      for (unsigned i = 0; i < dim; ++i) {		\
	EXPECT_EQ(pt_j[i], pt_j_dup[i]);		\
      }							\
							\
      pt_j     += dim;					\
      pt_j_dup += dim;					\
    }							\
							\
  } while (0)

TEST(inquad_levelset_height_function_test, levelset_duplicate_pts)
{
  unsigned dim = 2;
  const double h1_xmins[] = {-0.5, -0.5};
  const double h1_xmaxs[] = { 0.5,  0.5};

  hyprect h1;
  ASSERT_TRUE(SUCCESS
	      ==
	      hyprect_make_from_range(dim, h1_xmins, h1_xmaxs, &h1));

  levelset phi;
  scalar_field	phi_field; phi.field	       = &phi_field;
  vector_field	phi_grad_field; phi.grad_field = &phi_grad_field;
  ASSERT_TRUE(SUCCESS
	      ==
	      levelset_init_all_but_grad_on_orig_hyprect(&phi, &h1,
					    d_unit_sphere,
					    NULL,
					    sgn_negative));

  levelset_duplicate_pts(&phi);
  VERIFY_PT_DUPLICATION(phi);

  hyprect_free(&h1);
  levelset_free_pts(&phi);

  // ----------------------------------------------------------------------

  dim = 3;
  const double h2_xmins[] = {-0.5,  0.5, -1.0};
  const double h2_xmaxs[] = { 0.5, 10.0,  1.0};

  hyprect h2;
  ASSERT_TRUE(SUCCESS
	      ==
	      hyprect_make_from_range(dim, h2_xmins, h2_xmaxs, &h2));

  ASSERT_TRUE(SUCCESS
	      ==
	      levelset_init_all_but_grad_on_orig_hyprect(&phi, &h2,
					    d_unit_sphere,
					    NULL,
					    sgn_negative));

  levelset_duplicate_pts(&phi);
  VERIFY_PT_DUPLICATION(phi);

  hyprect_free(&h2);
  levelset_free_pts(&phi);
}


TEST(inquad_levelset_height_function_test,
     levelset_fill_new_frzn_coord_in_pts)
{
  unsigned dim = 3;
  const double h_xmins[] = {-0.5,  0.5, -1.0};
  const double h_xmaxs[] = { 0.5, 10.0,  1.0};

  hyprect h;
  ASSERT_TRUE(SUCCESS
	      ==
	      hyprect_make_from_range(dim, h_xmins, h_xmaxs, &h));

  levelset phi;
  scalar_field	phi_field; phi.field	       = &phi_field;
  vector_field	phi_grad_field; phi.grad_field = &phi_grad_field;
  ASSERT_TRUE(SUCCESS
	      ==
	      levelset_init_all_but_grad_on_orig_hyprect(&phi, &h,
							 d_unit_sphere,
							 NULL,
							 sgn_negative));

  levelset_duplicate_pts(&phi);

  unsigned frzn_coord = 0;
  levelset_fill_new_frzn_coord_in_pts(&phi, frzn_coord, &h);
  const double * pt_j = phi.pts;

  const double pts_expected[] = {-0.5, 10.5/2, 0.0,
				  0.5, 10.5/2, 0.0};
  const double * pt_j_expected = pts_expected;

  unsigned npts = phi.npts;
  for (unsigned j = 0; j < npts; ++j) {
    for (unsigned i = 0; i < dim; ++i) {
      EXPECT_DOUBLE_EQ(pt_j_expected[i], pt_j[i]);
    }

    pt_j_expected += dim;
    pt_j	  += dim;
  }

  hyprect_free(&h);
  levelset_free_pts(&phi);
}


TEST(inquad_levelset_height_function_test,
     levelset_set_new_facets_sgn_conds)
{
  ullong partial_sgns_neutral, partial_sgns_posneg;

  const int sgn_conds_facets[]	      = {-1, 0, 1, -1,  1, 0, 1};
  const int partials_sgns[]	      = { 1, 0, 1, -1, -1, 1, 0};
  const int sgn_conds_new_lo_facets[] = {-1, 0, 0,  0,  1, 0, 0};
  const int sgn_conds_new_hi_facets[] = { 0, 0, 1, -1,  0, 0, 0};

  unsigned	npts		= NELEMS(sgn_conds_facets);
  unsigned	hi_facet_offset = npts;

  const int *sgn_conds_new_lo_facets_no_surf_surf[] =
    {sgn_conds_new_lo_facets, sgn_conds_facets};

  const int *sgn_conds_new_hi_facets_no_surf_surf[] =
    {sgn_conds_new_hi_facets, sgn_conds_facets};

  levelset phi;
  phi.npts = npts;

  const bool is_surf_flags[] = {false, true};
  for (unsigned s = 0; s < NELEMS(is_surf_flags); ++s) {

    const int *sgn_conds_new_lo_facets_s =
      sgn_conds_new_lo_facets_no_surf_surf[s];

    const int *sgn_conds_new_hi_facets_s =
      sgn_conds_new_hi_facets_no_surf_surf[s];

    // Initialize.
    for (unsigned j = 0; j < npts; ++j) {
      store_sgn_in_bit_pair(partials_sgns[j],
			    j,
			    &partial_sgns_neutral,
			    &partial_sgns_posneg);

      store_sgn_in_bit_pair(sgn_conds_facets[j],
			    j,
			    &phi.sgns_neutral,
			    &phi.sgns_posneg);
    }

    bool is_surf_s = is_surf_flags[s];
    levelset_set_new_facets_sgn_conds(&phi, is_surf_s,
				      partial_sgns_neutral,
				      partial_sgns_posneg);

    for (unsigned j = 0; j < npts; ++j) {
      EXPECT_EQ(sgn_conds_new_lo_facets_s[j]
		,
		bit_pair_to_sgn(phi.sgns_neutral,
				phi.sgns_posneg,
				j));

      EXPECT_EQ(sgn_conds_new_hi_facets_s[j]
		,
		bit_pair_to_sgn(phi.sgns_neutral,
				phi.sgns_posneg,
				j + hi_facet_offset));

    }
  }
}
