// test_inquad_2d_unit_circle_rect_asymm.cc ---

// Copyright (C) 2017 Omid Khanmohamadi

// Author: Omid Khanmohamadi

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "src/inquad.h"
#include "tst_functions.h"
#include "tst_cond_compile.h"
#include "tst_invoke_macro.h"	// for INVOKE_INQUAD_TEST

#include "gsl/gsl_math.h"	// for M_constants
#include "gtest/gtest.h"	// for Google Test framework


TEST(inquad_test, inquad_area_unit_circle_in_0_1_x_0p5_sqrt3over2)
{
  const int	sgn	= 0;
  const bool	is_surf = true;

  IF_WRITE_SET(fnamebase, "unit_circle_in_0_1_x_0p5_sqrt3over2.txt");

  inquad_params inqparams = inquad_default_params;
  inqparams.quad_npts = 16;
  IF_WRITEQPTS_QUAD_OUTFNAME("quadpts_" + fnamebase);
  IF_WRITEQPTS_QUAD_NPTS(8);
  IF_TRACE_QUAD_NPTS(2);

  // The arc length of the intersection of the unit circle with the
  // hyprect h = [0, 1]x[1/2, √3/2] is
  //
  // π/6
  const unsigned dim = 2;
  const double h_xmins[] = {0.0, 0.5};
  const double h_xmaxs[] = {1.0, M_SQRT3/2};

  const double	integral_value_relerr	= 1e-10;
  const double	integral_value_expected = M_PI/6;

  integrand_field   integrand_F;
  const_func_params integrand_F_params;
  integrand_F_params.c = 1.0;
  integrand_F.function = const_func;
  integrand_F.params   = &integrand_F_params;

  INVOKE_INQUAD_TEST;

  IF_WRITEVALS_SAVE_CONVERGENCE_AND_TIMINING_DATA(("values_" + fnamebase).c_str(),
						  2, 256, n);
}


TEST(inquad_test, inquad_area_unit_circle_in_neg0p5_2_x_neg1p6_1p3)
{
  const int		sgn		    = 0;
  const bool		is_surf		    = true;

  IF_WRITE_SET(fnamebase, "unit_circle_in_neg0p5_2_x_neg1p6_1p3.txt");

  inquad_params inqparams = inquad_default_params;
  inqparams.quad_npts = 32;
  IF_WRITEQPTS_QUAD_OUTFNAME("quadpts_" + fnamebase);
  IF_WRITEQPTS_QUAD_NPTS(8);
  IF_TRACE_QUAD_NPTS(2);

  // The arc length of the intersection of the unit circle with the
  // hyprect h = [-0.5, 2]x[-1.6, 1.3] is
  //
  // 2π/2 (half unit circle)
  // +
  // 2π/6 (two arcs of unit circle each subtending an angle of π/6)
  // =
  // 4π/3
  const unsigned dim = 2;
  const double h_xmins[] = {-0.5, -1.6};
  const double h_xmaxs[] = { 2.0,  1.3};

  const double	integral_value_relerr	= 1e-6;
  const double	integral_value_expected = 4*M_PI/3;

  integrand_field   integrand_F;
  const_func_params integrand_F_params;
  integrand_F_params.c = 1.0;
  integrand_F.function = const_func;
  integrand_F.params   = &integrand_F_params;

  INVOKE_INQUAD_TEST;

  IF_WRITEVALS_SAVE_CONVERGENCE_AND_TIMINING_DATA(("values_" + fnamebase).c_str(),
						  2, 256 , n);
}
