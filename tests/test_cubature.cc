// test_cubature.cc ---

// Copyright (C) 2016 Omid Khanmohamadi

// Author: Omid Khanmohamadi

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "gtest/gtest.h"	// for Google Test framework
#include "cubature.h"		// for hcubature

#include <math.h> // for exp, pow (for independence, as cubature.h #include's)

#define K_2_SQRTPI 1.12837916709551257390

// Gaussian centered at 0.5.
double gaussian(unsigned xdim, const double *x, void *params)
{
     double a = *(double *)params;
     double sum = 0.;
     for (unsigned i = 0; i < xdim; i++) {
	  double dx = x[i] - 0.5;
	  sum += dx * dx;
     }
     return (pow(K_2_SQRTPI / (2. * a), (double) xdim) *
	     exp(-sum / (a * a)));
}

TEST(cubature_test, hcubature_gaussian)
{
  const unsigned xdim = 3;
  double xmin[] = {-2, -2, -2}, xmax[] = {2, 2, 2};

  const double intg_expected = 1;
  double a = 0.1;
  double *fdata = &a;

  const unsigned fdim = 1;
  size_t maxeval = 0;
  double abserr = 0, relerr = 1e-6, intg, err;

  auto fgaussian = [](unsigned xdim, const double *x,
		      void *fdata, unsigned fdim, double *fval)
    {
      for (unsigned i = 0; i < fdim; ++i){
	fval[i] = gaussian(xdim, x, fdata);
      }

      return 0;
    };
  hcubature(fdim, fgaussian, fdata,
	    xdim, xmin, xmax,
	    maxeval, abserr, relerr, ERROR_INDIVIDUAL,
	    &intg, &err);

  double intg_abserr = intg_expected * relerr;
  EXPECT_NEAR(intg_expected, intg, intg_abserr);
}
