// tst_cond_compile.h ---

// Copyright (C) 2017 Omid Khanmohamadi

// Author: Omid Khanmohamadi

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef TST_COND_COMPILE_H_INCLUDED
#define TST_COND_COMPILE_H_INCLUDED

// Reduce number of quadrature points (quad_npts in a inquad_params
// struct) if TRACE is on, so that we are not buried under an
// avalanche of output!
#if TRACE
#define IF_TRACE_QUAD_NPTS(N) inqparams.quad_npts = N
#else
#define IF_TRACE_QUAD_NPTS(N) /* */
#endif

#if WRITEQPTS || WRITEVALS
#include <string>
#define IF_WRITE_SET(FNAMEBASE,NAME) std::string FNAMEBASE {NAME}
#else
#define IF_WRITE_SET(FNAMEBASE,NAME) /* */
#endif

#if WRITEQPTS
#define IF_WRITEQPTS_QUAD_NPTS(N) inqparams.quad_npts = N
#define IF_WRITEQPTS_QUAD_OUTFNAME(NAME)		\
  std::string quad_outfname = NAME;			\
  do {							\
    inqparams.quad_outfname = quad_outfname.c_str();	\
							\
  } while(0)
#else
#define IF_WRITEQPTS_QUAD_NPTS(N) /* */
#define IF_WRITEQPTS_QUAD_OUTFNAME(NAME) /* */
#endif

#if WRITEVALS
#include <chrono>		// for high_resolution_clock

#define IF_WRITEVALS_SAVE_CONVERGENCE_AND_TIMINING_DATA(NAME,nbegin,nend,nstep) \
  do {									\
    const char *valfilename;						\
    valfilename = NAME;							\
    FILE *fptr = fopen(valfilename, "w");				\
    if (fptr) {								\
      inqparams.root_relerr = 1e-15;					\
									\
      for (unsigned n = nbegin; n < nend + 1; n += nstep) {		\
									\
	using hiresclock = std::chrono::high_resolution_clock;		\
	using ms = std::chrono::microseconds;				\
									\
	hyprect h;							\
	ASSERT_TRUE(SUCCESS						\
		    ==							\
		    hyprect_make_from_range(dim, h_xmins, h_xmaxs,	\
					    &h));			\
									\
	levelset	phi;						\
	scalar_field	phi_field;					\
	vector_field	phi_grad_field;					\
	phi.field	= &phi_field;					\
	phi.grad_field	= &phi_grad_field;				\
									\
	ASSERT_TRUE(SUCCESS						\
		    ==							\
		    levelset_init_on_orig_hyprect(&phi,			\
						  &h,			\
						  d_unit_sphere,	\
						  NULL,			\
						  grad_d_unit_sphere,	\
						  NULL,			\
						  sgn));		\
	inqparams.quad_npts = n;					\
									\
	auto t_before = hiresclock::now();				\
									\
	double integral_value;						\
	ASSERT_TRUE(SUCCESS						\
		    ==							\
		    inquad(&integrand_F, &phi, &h, is_surf, &inqparams,	\
			   &integral_value));				\
									\
	double integral_value_relerr =					\
	  fabs(integral_value/integral_value_expected - 1);		\
									\
	auto t_after = hiresclock::now();				\
									\
	fprintf(fptr, "%d %.16f %d\n",					\
		n,							\
		integral_value_relerr,					\
		std::chrono::duration_cast<ms>				\
		(t_after - t_before).count());				\
									\
	hyprect_free(&h);						\
	levelset_free_pts(&phi);					\
      }									\
    }									\
									\
    fclose(fptr);							\
  } while (0)
#else
#define IF_WRITEVALS_SAVE_CONVERGENCE_AND_TIMINING_DATA(NAME,nbegin,nend,nstep)
#endif

#endif // TST_COND_COMPILE_H_INCLUDED
