// test_gsl_roots.cc ---

// Copyright (C) 2016 Omid Khanmohamadi

// Author: Omid Khanmohamadi

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "gtest/gtest.h"   // declarations for Google Test framework.

#include "gsl/gsl_math.h"	// gsl_function, M_constants
#include "gsl/gsl_errno.h"	// GSL_CONTINUE
#include "gsl/gsl_roots.h"

#include <stdbool.h> // for bool in C (>= C99); fund. type in C++; need no header

TEST(gsl_roots_test, brent_qform)
{
  typedef struct params_s {
    unsigned xdim;
    unsigned idx;
    double  *xx;
  } params;

  auto qform = [](double x, void *prms)->double
    {
      params *p = (params *) prms;
      unsigned xdim = p->xdim, idx = p->idx;
      p->xx[idx] = 0;

      double sum_xx_i_sqrd = 0;
      for (unsigned i = 0; i < xdim; ++i){
	double xx_i = p->xx[i];
	sum_xx_i_sqrd += xx_i*xx_i;
      }
      return x*x - sum_xx_i_sqrd;
    };

  unsigned xdim = 5;
  unsigned idx = 2;
  double   xx[] = {-1, 0, 123, 0, 2}; // 123 is a filler for xx[idx]

  params qform_params = {xdim, idx, xx};

  gsl_function F;
  F.function = qform;  // lambda won't convert to ptr if preceded by &
  F.params   = &qform_params;

  const unsigned max_iter = 100;
  const double abserr = 0, relerr = 1e-6;

  int status;
  unsigned iter = 0;
  double rt = 0, rt_expected = sqrt(5.0);
  double x_lo = 0.0, x_hi = 5.0;

  const gsl_root_fsolver_type *brent_solver_t = gsl_root_fsolver_brent;
  gsl_root_fsolver *brent_solver;
  brent_solver = gsl_root_fsolver_alloc(brent_solver_t);
  gsl_root_fsolver_set(brent_solver, &F, x_lo, x_hi);

  do
    {
      ++iter;
      gsl_root_fsolver_iterate(brent_solver);
      rt = gsl_root_fsolver_root(brent_solver);

      x_lo = gsl_root_fsolver_x_lower(brent_solver);
      x_hi = gsl_root_fsolver_x_upper(brent_solver);
      status = gsl_root_test_interval(x_lo, x_hi, abserr, relerr);

    }
  while (status == GSL_CONTINUE && iter < max_iter);

  bool rts_eq = (gsl_fcmp(rt_expected, rt, relerr) == 0)?
    true: false;
  EXPECT_TRUE(rts_eq);

  gsl_root_fsolver_free(brent_solver);
}


// If this function (factored_poly3d) is defined in the body of the
// TEST below (using a lambda expression), the lambda expression
// factored_poly1d there will not see it unless it's either captured
// (making factored_poly1d unconvertable to a function pointer and
// unusable as a GSL function) or sent in via the prms argument
// (requiring the addition of an item to the params struct).
double factored_poly3d(double *x)
{
  return (x[0]-2.0) * (x[1]+1.0) * (x[2]-7.0);
}

TEST(gsl_roots_test, brent_factored_poly)
{
  typedef struct params_s {
    unsigned idx;
    double  *xx;
  } params;

  auto factored_poly1d = [](double x, void *prms)->double
    {
      params *p = (params *) prms;
      unsigned idx = p->idx;
      p->xx[idx] = x;

      return factored_poly3d(p->xx);
    };

  const unsigned xdim = 3;
  double xx[xdim];
  double xx_frzn[xdim - 1] = {100, 200};

  double rt;
  double rts_expected[] = {2.0, -1.0, 7.0};
  double xs_lo[] = { 0.0, -5.0,  0.0};
  double xs_hi[] = {10.0, 10.0, 10.0};

  params factored_poly1d_params;
  factored_poly1d_params.xx = xx;

  gsl_function F;

  // A stateless lambda (i.e., with no capture [] as in
  // factored_poly1d) is converted to a function pointer upon
  // assignment to one.
  F.function = factored_poly1d;
  F.params   = &factored_poly1d_params;

  const unsigned max_iter = 100;
  const double abserr = 1e-5, relerr = 1e-8;

  const gsl_root_fsolver_type *solver_t = gsl_root_fsolver_brent;
  gsl_root_fsolver *brent_solver;
  brent_solver = gsl_root_fsolver_alloc(solver_t);

  for (unsigned idx = 0; idx < xdim; ++idx){

    double rt_expected = rts_expected[idx];
    double x_lo = xs_lo[idx], x_hi = xs_hi[idx];
    factored_poly1d_params.idx = idx;

    for (unsigned i = 0; i < xdim; ++i){
      if (idx != i)
	xx[i] = xx_frzn[i];
    }

    gsl_root_fsolver_set(brent_solver, &F, x_lo, x_hi);
    int status;
    unsigned iter = 0;
    do
      {
	++iter;
	gsl_root_fsolver_iterate(brent_solver);
	rt = gsl_root_fsolver_root(brent_solver);

	x_lo = gsl_root_fsolver_x_lower(brent_solver);
	x_hi = gsl_root_fsolver_x_upper(brent_solver);
	status = gsl_root_test_interval(x_lo, x_hi, abserr, relerr);

      }
    while (status == GSL_CONTINUE && iter < max_iter);

    bool rts_eq = (gsl_fcmp(rt_expected, rt, relerr) == 0)?
      true: false;
    EXPECT_TRUE(rts_eq);
  }

  gsl_root_fsolver_free(brent_solver);
}
