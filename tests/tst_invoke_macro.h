// tst_invoke_macro.h ---

// Copyright (C) 2017 Omid Khanmohamadi

// Author: Omid Khanmohamadi

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef TST_INVOKE_MACRO_H_INCLUDED
#define TST_INVOKE_MACRO_H_INCLUDED

#define INVOKE_INQUAD_TEST						\
  do {									\
									\
    hyprect h;								\
    ASSERT_TRUE(SUCCESS							\
		==							\
		hyprect_make_from_range(dim, h_xmins, h_xmaxs, &h));	\
									\
    levelset	phi;							\
    scalar_field	phi_field;					\
    vector_field	phi_grad_field;					\
    phi.field	 = &phi_field;						\
    phi.grad_field = &phi_grad_field;					\
									\
    ASSERT_TRUE(SUCCESS							\
		==							\
		levelset_init_on_orig_hyprect(&phi,			\
					      &h,			\
					      d_unit_sphere,		\
					      /* params of function */	\
					      NULL,			\
					      grad_d_unit_sphere,	\
					      /* params of grad_function */ \
					      NULL,			\
					      sgn));			\
									\
    double integral_value;						\
    ASSERT_TRUE(SUCCESS							\
		==							\
		inquad(&integrand_F, &phi, &h, is_surf, &inqparams,	\
		       &integral_value));				\
									\
    double integral_value_abserr =					\
      integral_value_relerr * integral_value_expected;			\
									\
    EXPECT_NEAR(integral_value,						\
		integral_value_expected,				\
		integral_value_abserr);					\
									\
    hyprect_free(&h);							\
    levelset_free_pts(&phi);						\
									\
  } while (0)

#endif // TST_INVOKE_MACRO_H_INCLUDED
