// test_inquad_geometry.cc ---

// Copyright (C) 2016 Omid Khanmohamadi

// Author: Omid Khanmohamadi

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "src/inquad_geometry.h"
#include "src/return_code.h"

#include "gtest/gtest.h"   // for Google Test framework

// If, rather than a macro as below, we used a boolean function (and
// returned true or false from it) it would hide from us where an
// error had happened, as GTest Framework would only report that the
// line calling the function is erroneous.
//
// 'do {} while (0)' is the only construct in C that you can use to
// #define a multi-statement operation, put a semicolon after its use,
// and still use it within an if statement which has no '{}' and has
// an 'else' [https://stackoverflow.com/a/257425]. (from Dave Miller)
// Empty statements give a warning from the compiler so this is why
// you see #define FOO do { } while(0). (from Dave Miller) It gives
// you a basic block in which to declare local variables. (from Ben
// Collins) It allows you to use more complex macros in conditional
// code [https://kernelnewbies.org/FAQ/DoWhile0].
#define COMPARE_HYPRECTS(h1, h2)			\
  do {							\
							\
    EXPECT_EQ((h1).dim,           (h2).dim);		\
    EXPECT_EQ((h1).reduced_dim,   (h2).reduced_dim);	\
    EXPECT_EQ((h1).treated_dims,  (h2).treated_dims);	\
							\
    for (unsigned i = 0; i < (h1).dim; ++i) {		\
      EXPECT_DOUBLE_EQ((h1).center[i],			\
		       (h2).center[i]);			\
      EXPECT_DOUBLE_EQ((h1).halfwidths[i],		\
		       (h2).halfwidths[i]);		\
    }							\
							\
  } while(0)


// The '##' preprocessing operator performs token pasting. When a
// macro is expanded, the two tokens on either side of each '##'
// operator are combined into a single token, which then replaces the
// '##' and the two original tokens in the macro expansion. If either
// of the tokens next to an '##' is a parameter name, it is replaced
// by its actual argument before '##' executes. As with
// stringification, the actual argument is not macro-expanded first.
// It is an error if '##' appears at either end of a macro body
// [https://gcc.gnu.org/onlinedocs/cpp/Concatenation.html].
//
// This macro defines (read conjures up) a local variable and its body
// CANNOT be wrapped in a do {} while (0) guard. If wrapped, the
// conjured up hyprect1 would go out of scope as soon as we exited the
// 'loop' scope.
#define CONJURE_UP_HYPRECT(hrect1)		\
  hyprect hrect1 = {				\
    hrect1 ## _center,				\
    hrect1 ## _halfwidths,			\
    hrect1 ## _dim,				\
    hrect1 ## _reduced_dim,			\
    hrect1 ## _treated_dims			\
  }


TEST(inquad_geometry_test, hyprect_make)
{
  unsigned h1_dim	     = 2;
  unsigned h1_reduced_dim    = h1_dim;
  double   h1_center[]	     = {-1, 1};
  double   h1_halfwidths[]   = {1,  2};
  ulong    h1_treated_dims   = zero_UL;

  CONJURE_UP_HYPRECT(h1);

  hyprect h1_inquad;
  ASSERT_TRUE(hyprect_make(h1_dim, h1_center, h1_halfwidths, &h1_inquad)
	      ==
	      SUCCESS);

  COMPARE_HYPRECTS(h1, h1_inquad);

  unsigned h2_dim	     = 3;
  unsigned h2_reduced_dim    = h2_dim;
  double   h2_center[]	     = {-1, 0, 1};
  double   h2_halfwidths[]   = {1, 2, 3};
  ulong    h2_treated_dims   = zero_UL;

  CONJURE_UP_HYPRECT(h2);

  hyprect  h2_inquad;
  ASSERT_TRUE(hyprect_make(h2_dim, h2_center, h2_halfwidths, &h2_inquad)
	      ==
	      SUCCESS);

  COMPARE_HYPRECTS(h2, h2_inquad);

  hyprect_free(&h1_inquad);
  hyprect_free(&h2_inquad);
}

TEST(inquad_geometry_test, hyprect_make_from_range)
{
  // If xmins[j]==0 for all j, then center[j]==halfwidths[j] for all j.
  unsigned h1_dim	     = 2;
  unsigned h1_reduced_dim    = h1_dim;
  double   h1_xmins[]	     = {0, 0};
  double   h1_xmaxs[]	     = {1, 2};
  double   h1_center[]	     = {0.5, 1};
  double  *h1_halfwidths     = h1_center;
  ulong    h1_treated_dims   = zero_UL;

  CONJURE_UP_HYPRECT(h1);

  hyprect  h1_inquad;
  ASSERT_TRUE(hyprect_make_from_range(h1_dim, h1_xmins, h1_xmaxs, &h1_inquad)
	      ==
	      SUCCESS);

  COMPARE_HYPRECTS(h1, h1_inquad);

  unsigned h2_dim	     = 3;
  unsigned h2_reduced_dim    = h2_dim;
  double   h2_xmins[]	     = {-1, 5,   -20};
  double   h2_xmaxs[]	     = { 1, 8,    -5};
  double   h2_center[]	     = {0,  6.5, -12.5};
  double   h2_halfwidths[]   = {1,  1.5,   7.5};
  ulong    h2_treated_dims   = zero_UL;

  CONJURE_UP_HYPRECT(h2);

  hyprect  h2_inquad;
  ASSERT_TRUE(hyprect_make_from_range(h2_dim, h2_xmins, h2_xmaxs, &h2_inquad)
	      ==
	      SUCCESS);

  COMPARE_HYPRECTS(h2, h2_inquad);

  hyprect_free(&h1_inquad);
  hyprect_free(&h2_inquad);

}

TEST(inquad_geometry_test, hyprect_copy)
{

  unsigned h1_dim	     = 3;
  double   h1_center[]	     = {-1, 0, 1};
  double   h1_halfwidths[]   = {1, 2, 3};

  hyprect h1_inquad;
  ASSERT_TRUE(hyprect_make(h1_dim, h1_center, h1_halfwidths, &h1_inquad)
	      ==
	      SUCCESS);

  hyprect h2_inquad;
  ASSERT_TRUE(hyprect_copy(&h1_inquad, &h2_inquad)
	      ==
	      SUCCESS);

  COMPARE_HYPRECTS(h1_inquad, h2_inquad);

  hyprect_free(&h1_inquad);
  hyprect_free(&h2_inquad);

}

TEST(inquad_geometry_test, hyprect_halve)
{
  unsigned h1_dim	     = 3;
  double   h1_xmins[]	     = {-1, 5, -20};
  double   h1_xmaxs[]	     = {1, 11,  -6};
  // So,   h1_center[]       = {0,  8, -13};
  // and   h1_halfwidths[]   = {1,  3,   7};
  unsigned h1_dim2halve      = 2;	// == argmax(h1.halfwidths, h1.dim)
  unsigned h1_reduced_dim    = h1_dim;
  ulong    h1_treated_dims   = zero_UL;

  hyprect  h1_new_inquad;
  ASSERT_TRUE(hyprect_make_from_range(h1_dim, h1_xmins, h1_xmaxs, &h1_new_inquad)
	      ==
	      SUCCESS);

  double         h1_new_halfwidths[]  = {1, 3,       0.5*7};
  double         h1_new_center[]      = {0, 8, -13 - 0.5*7};
  unsigned	 h1_new_dim	      = h1_dim;
  unsigned	 h1_new_reduced_dim   = h1_reduced_dim;
  ulong          h1_new_treated_dims  = h1_treated_dims;

  double	*h2_new_halfwidths    = h1_new_halfwidths;
  double         h2_new_center[]      = {0, 8, -13 + 0.5*7};
  unsigned	 h2_new_dim	      = h1_dim;
  unsigned	 h2_new_reduced_dim   = h1_reduced_dim;
  ulong          h2_new_treated_dims  = h1_treated_dims;

  CONJURE_UP_HYPRECT(h1_new);
  CONJURE_UP_HYPRECT(h2_new);

  hyprect h2_new_inquad;
  ASSERT_TRUE(hyprect_halve(&h1_new_inquad, &h2_new_inquad, h1_dim2halve)
	      ==
	      SUCCESS);

  COMPARE_HYPRECTS(h1_new, h1_new_inquad);
  COMPARE_HYPRECTS(h2_new, h2_new_inquad);

  hyprect_free(&h1_new_inquad);
  hyprect_free(&h2_new_inquad);

}

TEST(inquad_geometry_test, hyprect_facet_set_next_vertex_dim_2)
{
  const unsigned h_dim	     = 2;
  double   h_xmins[]	     = {0, 0};
  double   h_xmaxs[]	     = {2, 1};

  hyprect h;
  hyprect_make_from_range(h_dim, h_xmins, h_xmaxs, &h);

  ulong treated_dims   = zero_UL;
  unsigned dim_treated = 1;	// x_1, corresponding to 'y direction'

  SET_BIT(treated_dims, dim_treated);
  h.treated_dims = treated_dims;
  h.reduced_dim  = 1;

  double xc1[] = {1, 0}; // (identifying) facet lying on y = 0
  double xvs1_expected[] = {2, 0,  // vertex 0 on facet y = 0
			    0, 0}; // vertex 1 on facet y = 0

  double xv1_i[h_dim];
  unsigned offset_xv1_i = 0;
  while (ITER_STOP != hyprect_facet_set_next_vertex(&h, xc1, xv1_i)) {
    double *xv1_i_expected = xvs1_expected + offset_xv1_i;
    for (unsigned j = 0; j < h_dim; ++j) {
      EXPECT_DOUBLE_EQ(xv1_i[j], xv1_i_expected[j]);
    }

    offset_xv1_i += h_dim;
  }

  double xc2[] = {1, 1}; // (identifying) facet lying on y = 1
  double xvs2_expected[] = {2, 1,  // vertex 0 on facet y = 1
			    0, 1}; // vertex 1 on facet y = 1

  double xv2_i[h_dim];
  unsigned offset_xv2_i = 0;
  while (ITER_STOP != hyprect_facet_set_next_vertex(&h, xc2, xv2_i)) {
    double *xv2_i_expected = xvs2_expected + offset_xv2_i;
    for (unsigned j = 0; j < h_dim; ++j) {
      EXPECT_DOUBLE_EQ(xv2_i[j], xv2_i_expected[j]);
    }

    offset_xv2_i += h_dim;
  }

  hyprect_free(&h);
}

TEST(inquad_geometry_test, hyprect_facet_set_next_vertex_dim_3)
{
  const unsigned h_dim	     = 3;
  double   h_xmins[]	     = {0, 0, 0};
  double   h_xmaxs[]	     = {2, 1, 3};

  hyprect h;
  hyprect_make_from_range(h_dim, h_xmins, h_xmaxs, &h);

  ulong treated_dims   = zero_UL;
  unsigned dim_treated = 2;	// x_2, corresponding to 'z direction'

  SET_BIT(treated_dims, dim_treated);
  h.treated_dims = treated_dims;
  h.reduced_dim  = 2;

  double xc1[] = {1, 0.5, 0}; // (identifying) facet lying on z = 0

  double xvs1_expected[] = {2, 1, 0,  // vertex 0 on facet z = 0
			    0, 1, 0,  // vertex 1 on facet z = 0
			    0, 0, 0,  // vertex 2 on facet z = 0
			    2, 0, 0}; // vertex 3 on facet z = 0

  double xv1_i[h_dim];
  unsigned offset_xv1_i = 0;
  while (ITER_STOP != hyprect_facet_set_next_vertex(&h, xc1, xv1_i)) {

    double *xv1_i_expected = xvs1_expected + offset_xv1_i;
    for (unsigned j = 0; j < h_dim; ++j) {
      EXPECT_DOUBLE_EQ(xv1_i[j], xv1_i_expected[j]);
    }

    offset_xv1_i += h_dim;
  }

  SET_BIT(treated_dims, dim_treated);
  h.treated_dims = treated_dims;
  h.reduced_dim  = 2;

  double xc2[] = {1, 0.5, 3}; // (identifying) facet lying on z = 0

  double xvs2_expected[] = {2, 1, 3,  // vertex 0 on facet z = 3
			    0, 1, 3,  // vertex 1 on facet z = 3
			    0, 0, 3,  // vertex 2 on facet z = 3
			    2, 0, 3}; // vertex 3 on facet z = 3

  double xv2_i[h_dim];
  unsigned offset_xv2_i = 0;
  while (ITER_STOP != hyprect_facet_set_next_vertex(&h, xc2, xv2_i)) {

    double *xv2_i_expected = xvs2_expected + offset_xv2_i;
    for (unsigned j = 0; j < h_dim; ++j) {
      EXPECT_DOUBLE_EQ(xv2_i[j], xv2_i_expected[j]);
    }

    offset_xv2_i += h_dim;
  }

  hyprect_free(&h);
}


TEST(inquad_geometry_test, hyprect_reduced_diam)
{
  const unsigned dim = 3;
  const double h1_xmins[] = { 0.0, 0.0, 0.0};
  const double h1_xmaxs[] = { 3.0, 4.0, 1.0};

  hyprect h1;
  ASSERT_TRUE(SUCCESS
	      ==
	      hyprect_make_from_range(dim, h1_xmins, h1_xmaxs, &h1));

  double h1_reduced_radius_expected = sqrt(9.0 + 16.0 + 1.0)/2;
  EXPECT_DOUBLE_EQ(h1_reduced_radius_expected,
		   hyprect_reduced_radius(&h1));

  // ----------------------------------------------------------------------

  unsigned dim_treated = 2;
  hyprect_mark_dim_as_treated(&h1, dim_treated);

  h1_reduced_radius_expected = 5.0/2;
  EXPECT_DOUBLE_EQ(h1_reduced_radius_expected,
		   hyprect_reduced_radius(&h1));

  // ----------------------------------------------------------------------

  hyprect_mark_dim_as_untreated(&h1, dim_treated);

  dim_treated = 0;
  hyprect_mark_dim_as_treated(&h1, dim_treated);

  h1_reduced_radius_expected = sqrt(16.0 + 1.0)/2;
  EXPECT_DOUBLE_EQ(h1_reduced_radius_expected,
		   hyprect_reduced_radius(&h1));

  hyprect_free(&h1);
}
