// tst_functions.cc ---

// Copyright (C) 2017 Omid Khanmohamadi

// Author: Omid Khanmohamadi

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "tst_functions.h"	// for const_func_params

#include "src/norm2.h"		// for norm2
#include "src/unused_param.h"	// for UNUSED_PARAM

#include <math.h>		// for sin

double qform1d(double x, double a)
{
  return a*x*x;
}

void qform1d_vec_fn(unsigned xdim, const double *x, void *params,
		    unsigned fdim, double *value)
{
  double a = *((double *) params);

  // In 1D fdim == xdim == 1; 'fdim-1' and 'xdim-1' are used below
  // instead of '0' only to avoid the 'unused-variable', compiler
  // warning about these arguments.
  value[fdim-1] = qform1d(x[xdim-1], a);
}

double xysinz(unsigned xdim, const double *x)
{
  return x[0]*x[1]*sin(x[2]);
}

void xysinz_vec_fn(unsigned xdim, const double * const x,
		   void *params,
		   unsigned fdim, double * const value)
{
  value[fdim-1] = xysinz(xdim, x);
}

void fR2_R3_vec_fn(unsigned xdim, const double * const x, void *params,
		   unsigned fdim, double * const value)
{
  value[0] = x[0]*x[1];
  value[1] = sin(x[0] + x[1]);
  value[2] = x[0]*x[0] - x[1]*x[1];
}


// Distance function for the unit circle.
double d_unit_sphere(unsigned			 dim,
		     const double * const	 x,
		     void			*params)

{
  UNUSED_PARAM(params);

  return norm2(x, dim) - 1.0;
}

void grad_d_unit_sphere(unsigned		 dim,
			const double * const	 x,
			void			*params,
			double * const		 value)

{
  UNUSED_PARAM(params);

  double r = norm2(x, dim);

  // This is to avoid division by zero in setting 'value' below. The
  // gradient is actually undefined at r=0, since we have different
  // limits on different paths (e.g., consider in 1D, sqrt(x^2)=|x|,
  // which is not differentiable at x=0.)
  if (r == 0) {
    r = 1;
  }

  for (unsigned i = 0; i < dim; ++i) {
    value[i] = x[i]/r;
  }
}


double const_func(unsigned dim, double * const x, void *params)
{
  const_func_params *prms = (const_func_params *) params;
  double c = prms->c;

  return c;
}

double x0_func(unsigned dim, double * const x, void *params)
{
  UNUSED_PARAM(params);

  return x[0];
}
