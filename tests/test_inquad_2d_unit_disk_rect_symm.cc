// test_inquad_2d.cc ---

// Copyright (C) 2017 Omid Khanmohamadi

// Author: Omid Khanmohamadi

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "src/inquad.h"
#include "tst_functions.h"
#include "tst_cond_compile.h"
#include "tst_invoke_macro.h"	// for INVOKE_INQUAD_TEST

#include "gsl/gsl_math.h"	// for M_constants
#include "gtest/gtest.h"	// for Google Test framework


TEST(inquad_test, inquad_area_unit_disk_in_0_1p1_x_neg1p1_1p1)
{
  const int		sgn		    = -1;
  const bool		is_surf		    = false;

  IF_WRITE_SET(fnamebase, "unit_disk_in_0_1p1_x_neg1p1_1p1.txt");

  inquad_params inqparams = inquad_default_params;
  inqparams.quad_npts = 90;
  IF_WRITEQPTS_QUAD_OUTFNAME("quadpts_" + fnamebase);
  IF_WRITEQPTS_QUAD_NPTS(8);
  IF_TRACE_QUAD_NPTS(2);

  // The area of the intersection of the unit disk (enclosed by the
  // unit circle) with the hyprect h = [0, 1.1]x[-1.1, 1.1] is
  //
  // π/2 (half of the unit disk)
  // =
  // 1.57079632679489661922
  const unsigned dim = 2;
  const double h_xmins[] = {0.0, -1.1};
  const double h_xmaxs[] = {1.1,  1.1};

  const double	integral_value_relerr	= 1e-6;
  const double	integral_value_expected = 1.57079632679489661922;

  integrand_field   integrand_F;
  const_func_params integrand_F_params;
  integrand_F_params.c = 1.0;
  integrand_F.function = const_func;
  integrand_F.params   = &integrand_F_params;


  INVOKE_INQUAD_TEST;

  IF_WRITEVALS_SAVE_CONVERGENCE_AND_TIMINING_DATA(("values_" + fnamebase).c_str(),
						  8, 1024, n);
}


TEST(inquad_test, inquad_area_unit_disk_out_0_1p1_x_neg1p1_1p1)
{
  const int		sgn		    = 1;
  const bool		is_surf		    = false;

  IF_WRITE_SET(fnamebase, "unit_disk_out_0_1p1_x_neg1p1_1p1.txt");

  inquad_params inqparams = inquad_default_params;
  inqparams.quad_npts = 100;
  IF_WRITEQPTS_QUAD_OUTFNAME("quadpts_" + fnamebase);
  IF_WRITEQPTS_QUAD_NPTS(8);
  IF_TRACE_QUAD_NPTS(2);

  // The area of the intersection of the outside of unit disk
  // (enclosed by the unit circle) with the hyprect h = [0,
  // 1.1]x[-1.1, 1.1] is
  //
  // (1.1-0)(1.1-(-1.1))
  // -
  // π/2 (half of the unit disk)
  // =
  // 0.22 - 1.57079632679489661922
  // =
  // 0.84920367320510338078
  const unsigned dim = 2;
  const double h_xmins[] = {0.0, -1.1};
  const double h_xmaxs[] = {1.1,  1.1};

  const double	integral_value_relerr	= 1e-6;
  const double	integral_value_expected = 0.84920367320510338078;

  integrand_field   integrand_F;
  const_func_params integrand_F_params;
  integrand_F_params.c = 1.0;
  integrand_F.function = const_func;
  integrand_F.params   = &integrand_F_params;

  INVOKE_INQUAD_TEST;

  IF_WRITEVALS_SAVE_CONVERGENCE_AND_TIMINING_DATA(("values_" + fnamebase).c_str(),
						  8, 1024, n);
}


TEST(inquad_test, inquad_area_unit_disk_in_0_1p1_squared)
{
  const int	sgn	= -1;
  const bool	is_surf = false;

  inquad_params inqparams = inquad_default_params;
  inqparams.quad_npts = 64;
  IF_TRACE_QUAD_NPTS(2);

  // The area of the intersection of the unit disk (enclosed by the
  // unit circle) with the hyprect h = [0, 1.1]x[0, 1.1] is
  //
  // π/4 (quarter of the unit disk)
  // =
  // 0.78539816339744830961
  const unsigned dim = 2;
  const double h_xmins[] = {0.0, 0.0};
  const double h_xmaxs[] = {1.1, 1.1};

  const double	integral_value_relerr	= 1e-6;
  const double	integral_value_expected = 0.78539816339744830961;

  integrand_field   integrand_F;
  const_func_params integrand_F_params;
  integrand_F_params.c = 1.0;
  integrand_F.function = const_func;
  integrand_F.params   = &integrand_F_params;

  INVOKE_INQUAD_TEST;
}


TEST(inquad_test, inquad_area_unit_disk_in_neg1p1_1p1_squared)
{
  const int	sgn	= -1;
  const bool	is_surf = false;

  IF_WRITE_SET(fnamebase, "unit_disk_in_neg1p1_1p1_squared.txt");

  inquad_params inqparams = inquad_default_params;
  inqparams.quad_npts = 128;
  IF_WRITEQPTS_QUAD_OUTFNAME("quadpts_" + fnamebase);
  IF_WRITEQPTS_QUAD_NPTS(8);
  IF_TRACE_QUAD_NPTS(2);

  // The area of the intersection of the unit disk (enclosed by the
  // unit circle) with the hyprect h = [-1.1, 1.1]x[-1.1, 1.1] is
  //
  // π (whole unit disk)
  // =
  // 3.14159265358979323844
  const unsigned dim = 2;
  const double h_xmins[] = {-1.1, -1.1};
  const double h_xmaxs[] = { 1.1,  1.1};

  const double	integral_value_relerr	= 1e-6;
  const double	integral_value_expected = 3.14159265358979323844;

  integrand_field   integrand_F;
  const_func_params integrand_F_params;
  integrand_F_params.c = 1.0;
  integrand_F.function = const_func;
  integrand_F.params   = &integrand_F_params;

  INVOKE_INQUAD_TEST;

  IF_WRITEVALS_SAVE_CONVERGENCE_AND_TIMINING_DATA(("values_" + fnamebase).c_str(),
						  8, 1024, n);
}


TEST(inquad_test, inquad_area_unit_disk_out_neg1p1_1p1_squared)
{
  const int	sgn	= 1;
  const bool	is_surf = false;

  IF_WRITE_SET(fnamebase, "unit_disk_out_neg1p1_1p1_squared.txt");

  inquad_params inqparams = inquad_default_params;
  inqparams.quad_npts = 128;
  IF_WRITEQPTS_QUAD_OUTFNAME("quadpts_" + fnamebase);
  IF_WRITEQPTS_QUAD_NPTS(8);
  IF_TRACE_QUAD_NPTS(2);

  // The area of the intersection of the outside of the unit disk
  // (enclosed by the unit circle) with the hyprect h = [-1.1,
  // 1.1]x[-1.1, 1.1] is
  //
  // (1.1-(-1.1))(1.1-(-1.1))
  // -
  // π (whole unit disk)
  // =
  // 4.84 - 3.14159265358979323844
  // =
  // 1.69840734641020676156
  const unsigned dim = 2;
  const double h_xmins[] = {-1.1, -1.1};
  const double h_xmaxs[] = { 1.1,  1.1};

  const double	integral_value_relerr	= 1e-6;
  const double	integral_value_expected = 1.69840734641020676156;

  integrand_field   integrand_F;
  const_func_params integrand_F_params;
  integrand_F_params.c = 1.0;
  integrand_F.function = const_func;
  integrand_F.params   = &integrand_F_params;

  INVOKE_INQUAD_TEST;

  IF_WRITEVALS_SAVE_CONVERGENCE_AND_TIMINING_DATA(("values_" + fnamebase).c_str(),
						  8, 1024, n);
}


TEST(inquad_test,
     inquad_area_unit_disk_in_neg0p1_0p3_x_neg0p2_0p4_pruning_cubature)
{
  const int	sgn	= -1;
  const bool	is_surf = false;

  inquad_params inqparams = inquad_default_params;
  IF_TRACE_QUAD_NPTS(2);

  // The area of the intersection of the unit disk (enclosed by the
  // unit circle) with the hyprect h = [-1.1, 1.1]x[-1.1, 1.1] is
  //
  // (0.3-(-0.1))*(0.4-(-0.2))
  // =
  // 0.24
  const unsigned dim = 2;
  const double h_xmins[] = {-0.1, -0.2};
  const double h_xmaxs[] = { 0.3,  0.4};

  const double	integral_value_relerr	= 1e-6;
  const double	integral_value_expected = 0.24;

  integrand_field   integrand_F;
  const_func_params integrand_F_params;
  integrand_F_params.c = 1.0;
  integrand_F.function = const_func;
  integrand_F.params   = &integrand_F_params;

  INVOKE_INQUAD_TEST;
}


TEST(inquad_test, inquad_area_unit_disk_in_neg0p5_0p7_x_2_4_pruning_zero)
{
  const int	sgn	= -1;
  const bool	is_surf = false;

  inquad_params inqparams = inquad_default_params;
  inqparams.quad_npts = 32;
  IF_TRACE_QUAD_NPTS(2);

  // The area of the intersection of the unit disk (enclosed by the
  // unit circle) with the hyprect h = [-0.5, 0.7]x[2.0, 4.0] is
  //
  // 0.0
  const unsigned dim = 2;
  const double h_xmins[] = { -0.5, 2.0};
  const double h_xmaxs[] = {  0.7, 4.0};

  const double	integral_value_relerr	= 1e-6;
  const double	integral_value_expected = 0.0;

  integrand_field   integrand_F;
  const_func_params integrand_F_params;
  integrand_F_params.c = 1.0;
  integrand_F.function = const_func;
  integrand_F.params   = &integrand_F_params;

  INVOKE_INQUAD_TEST;
}
