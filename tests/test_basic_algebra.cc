// test_basic_algebra.cc ---

// Copyright (C) 2016 Omid Khanmohamadi

// Author: Omid Khanmohamadi

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "src/basic_algebra.h"

#include "gtest/gtest.h"	// for Google Test framework

#include <math.h>		// for sqrt

TEST(basic_algebra_test, square_d)
{
  EXPECT_DOUBLE_EQ(square_d(-1.0), 1.0);
  EXPECT_DOUBLE_EQ(square_d( 1.0), 1.0);
  EXPECT_DOUBLE_EQ(square_d( 2.0), 4.0);
  EXPECT_DOUBLE_EQ(square_d( 0.0), 0.0);

  EXPECT_DOUBLE_EQ(square_d(sqrt(2)), 2.0);

}


TEST(basic_algebra_test, have_same_sign_zero_pos)
{
  EXPECT_TRUE(have_same_sign_zero_pos( 1.5,  0.5));
  EXPECT_TRUE(have_same_sign_zero_pos(-1.5, -0.5));
  EXPECT_TRUE(have_same_sign_zero_pos( 0.0,  0.0));

  EXPECT_FALSE(have_same_sign_zero_pos(-1.5,  1.0));
  EXPECT_FALSE(have_same_sign_zero_pos( 1.0, -1.5));

  // Borderline cases
  EXPECT_TRUE(have_same_sign_zero_pos(-0.0, 0.0));

  EXPECT_TRUE(have_same_sign_zero_pos(1.0,  0.0));
  EXPECT_TRUE(have_same_sign_zero_pos(1.0, -0.0));

  EXPECT_FALSE(have_same_sign_zero_pos(-1.0, 0.0));
}


TEST(basic_algebra_test, have_same_sign_zero_signless)
{
  EXPECT_TRUE(have_same_sign_zero_signless( 1.5,  0.5));
  EXPECT_TRUE(have_same_sign_zero_signless(-1.5, -0.5));
  EXPECT_TRUE(have_same_sign_zero_signless( 0.0,  0.0));

  EXPECT_FALSE(have_same_sign_zero_signless(-1.5,  1.0));
  EXPECT_FALSE(have_same_sign_zero_signless( 1.0, -1.5));

  // Borderline cases
  EXPECT_TRUE(have_same_sign_zero_signless(-0.0, 0.0));

  EXPECT_TRUE(have_same_sign_zero_signless(1.0,  0.0));
  EXPECT_TRUE(have_same_sign_zero_signless(1.0, -0.0));

  EXPECT_TRUE(have_same_sign_zero_signless(-1.0, 0.0));
}


TEST(basic_algebra_test, signum)
{
  EXPECT_EQ(1,  signum( 2.5));
  EXPECT_EQ(1,  signum( 0.5));
  EXPECT_EQ(0,  signum( 0.0));
  EXPECT_EQ(0,  signum(-0.0));
  EXPECT_EQ(0,  signum(-0.0));
  EXPECT_EQ(-1, signum(-0.5));
  EXPECT_EQ(-1, signum(-2.5));
}
