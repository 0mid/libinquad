// test_inquad_integrand.cc ---

// Copyright (C) 2017 Omid Khanmohamadi

// Author: Omid Khanmohamadi

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "src/inquad_integrand.h" // for integrand_eval_pt_{alloc,free}
#include "src/inquad_levelset_height_function.h" // for levelset_duplicate_pts_...
#include "src/bit_util.h"	// for idx_least_sig_0_bit, zero_ULL, etc.
#include "src/return_code.h"	// for errcode (incl. SUCCESS)

#include "tst_functions.h"
#include "tst_cond_compile.h"

#include "gsl/gsl_math.h"	// for M_constants
#include "gtest/gtest.h"	// for Google Test framework

TEST(inquad_integrand_test, integrand_eval_pt_alloc_free)
{
  const unsigned dim = 4;
  double *eval_pt;
  ASSERT_TRUE(SUCCESS
	      ==
	      integrand_eval_pt_alloc(dim, &eval_pt));

  eval_pt[0] = -1.0;
  eval_pt[1] =  0.0;
  eval_pt[2] =  0.5;
  eval_pt[3] =  1.5;

  double eval_pt_expected[] = {-1.0, 0.0, 0.5, 1.5};

  for (unsigned i = 0; i < dim; ++i) {
    EXPECT_EQ(eval_pt[i], eval_pt_expected[i]);
  }

  integrand_eval_pt_free(eval_pt);
}


TEST(inquad_integrand_test, integrand_params_alloc_partition_pts)
{
  const int		sgn  = -1;
  const unsigned	npts = 3;

  integrand_params f_params;
  ASSERT_TRUE(SUCCESS
	      ==
	      integrand_params_alloc_partition_pts(&f_params, npts));

  f_params.partition_pts[0] = -0.5;
  f_params.partition_pts[1] =  0.0;
  f_params.partition_pts[2] =  0.5;

  const double partition_pts_expected[] = {-0.5, 0.0, 0.5};

  for (unsigned j = 0; j < npts; ++j) {
    EXPECT_EQ(f_params.partition_pts[j], partition_pts_expected[j]);
  }

  integrand_params_free_partition_pts(&f_params);
}


TEST(inquad_integrand_test, integrand_value)
{
  const int	sgn	= -1;
  const bool	is_surf = false;

  inquad_params inqparams = inquad_default_params;
  IF_TRACE_QUAD_NPTS(2);

  // The area of the intersection of the unit disk (enclosed by the
  // unit circle) with the hyprect h = [0, 1]x[1/2, √3/2] is
  //
  // π/12 (s, π/6 sector of unit disk)
  // -
  // 0.5(√3/2-1/2√3)(1/2) (triangle part of s outside h)
  // +
  // 0.5(√3/2-1/2)(1/2√3+1/2) (trapezoid between the y axis and s)
  // =
  // 0.26179938779914943653
  const unsigned dim = 2;
  const double h_xmins[] = {0.0, 0.5};
  const double h_xmaxs[] = {1.0, M_SQRT3/2};

  double	integral_value_relerr	= 1e-10;
  const double	integral_value_expected = 0.26179938779914943653;

  hyprect h;
  ASSERT_TRUE(SUCCESS
	      ==
	      hyprect_make_from_range(dim, h_xmins, h_xmaxs, &h));

  levelset	phi;
  scalar_field	phi_field;
  vector_field	phi_grad_field;
  phi.field	 = &phi_field;
  phi.grad_field = &phi_grad_field;

  // Among doing other things, this initialization allocates storage
  // for TWO points (but fills only one and sets npts to 1) when
  // TESTING.
  ASSERT_TRUE(SUCCESS
	      ==
	      levelset_init_on_orig_hyprect(&phi,
					    &h,
					    d_unit_sphere,
					    NULL,
					    grad_d_unit_sphere,
					    NULL,
					    sgn));


  levelset	phi_pruned;
  scalar_field	phi_pruned_field;
  vector_field	phi_pruned_grad_field;
  phi_pruned.field	= &phi_pruned_field;
  phi_pruned.grad_field = &phi_pruned_grad_field;

  ASSERT_TRUE(SUCCESS
	      ==
	      levelset_copy(&phi_pruned, &phi));

  unsigned dir0 = 1;		// inner most ∫ (indexed 0) is in x1

  // Necessary to set manually for utesting only. Otherwise set by
  // levelset_direction_gives_height_function. Note that we have only
  // one facet (facet 0, identified by xc_0) at the start.
  //
  // signum(D_dir0_phi_xc_0) == signum(x1/r) == 1.

  int sgn_D_dir0_phi_xc_0 = 1;
  ullong partial_sgns_neutral = zero_ULL;
  ullong partial_sgns_posneg  = zero_ULL;
  store_sgn_in_bit_pair(sgn_D_dir0_phi_xc_0,
			0,	// facet 0
			&partial_sgns_neutral,
			&partial_sgns_posneg);

  levelset phi_new;
  phi_new.npts = phi_pruned.npts;

  // We first assign the sgn conds of phi_pruned (same as those of
  // phi) to phi_new. Then, each bit j pair is used to determined the
  // sgn conds for new lo and hi facets arising from facet j. These
  // two new sgn conds are stored in bit j pair and bit
  // j+hi_facet_offset=j+npts for the new lo and hi facets,
  // respectively.
  levelset_copy_sgns(&phi_new, &phi_pruned);

  levelset_set_new_facets_sgn_conds(&phi_new,
				    is_surf,
				    partial_sgns_neutral,
				    partial_sgns_posneg);

  scalar_field	phi_new_field;      phi_new.field      = &phi_new_field;
  vector_field	phi_new_grad_field; phi_new.grad_field = &phi_new_grad_field;

  ASSERT_TRUE(SUCCESS
	      ==
	      levelset_copy_no_sgns_w_twice_storage_for_pts(&phi_new,
							    &phi_pruned));

  integrand_field	integrand_F;
  const_func_params	integrand_F_params;
  integrand_F_params.c = 1.0;

  integrand_F.function  = const_func;
  integrand_F.params    = &integrand_F_params;

  levelset_duplicate_pts_and_fill_in_new_frzn_coord(&phi_new, dir0, &h);

  integrand_field	f0;
  integrand_params	f0_params;
  f0.function = integrand_value;
  f0.params   = &f0_params;

  ASSERT_TRUE(SUCCESS
	      ==
	      integrand_params_alloc_partition_pts(&f0_params,
						   phi_pruned.npts + 2));

  // This call sets f0(x_dir1) to be ∫integrand_F(x_0, x_1) dx_dir0 over
  // subintervals in the range dir0 of h which satisfy our sign
  // condition. This gives
  //
  // f0(x_0)
  // := ∫1_[0.5, d_unit_sphere(x_0, x_1)] dx_1
  // = sqrt(1-x_0^2) - 0.5,
  //
  // after root finding and integration for each given x_0 coming from
  // the outer ∫.
  integrand_params_set_all_but_partition_pts(&f0_params,
					     &integrand_F,
					     &phi_pruned,
					     &h,
					     dir0,
					     &inqparams);

  hyprect_mark_dim_as_treated(&h, dir0);

  // ----------------------------------------------------------------------
  // Manually 'go to recursion level' of next (outermost) integral.
  // Necessary for unit testing only.

  integrand_params f_final_params;
  ASSERT_TRUE(SUCCESS
	      ==
	      integrand_params_alloc_partition_pts(&f_final_params,
						   phi_new.npts + 2));

  double *eval_pt;
  ASSERT_TRUE(SUCCESS
	      ==
	      integrand_eval_pt_alloc(dim, &eval_pt));

  unsigned dir_final = idx_least_sig_0_bit(h.treated_dims);

  integrand_params_set_all_but_partition_pts(&f_final_params,
					     &f0,
					     &phi_new,
					     &h,
					     dir_final,
					     &inqparams);

  double integral_value = integrand_value(dim, eval_pt, &f_final_params);

  double integral_value_abserr = integral_value_relerr * integral_value_expected;

  EXPECT_NEAR(integral_value,
	      integral_value_expected,
	      integral_value_abserr);

  hyprect_free(&h);

  integrand_params_free_partition_pts(&f0_params);
  integrand_params_free_partition_pts(&f_final_params);
  integrand_eval_pt_free(eval_pt);

  levelset_free_pts(&phi_pruned);
  levelset_free_pts(&phi_new);

  levelset_free_pts(&phi);
}
