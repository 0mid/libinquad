// test_bit_util.cc ---

// Copyright (C) 2016 Omid Khanmohamadi

// Author: Omid Khanmohamadi

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "src/bit_util.h"

#include "gtest/gtest.h"   // for Google Test framework
#include <iostream>

TEST(bit_util_test, SET_BIT)
{
  // Literal binary representations are not in the C or C++standard,
  // but GCC and Clang support them.
  unsigned x1 = 0b101010;	// 42 (bits 1, 3, 5 set)

  ullong x1_set_bit = 0UL;
  SET_BIT(x1_set_bit, 1);
  SET_BIT(x1_set_bit, 3);
  SET_BIT(x1_set_bit, 5);

  EXPECT_EQ(x1, x1_set_bit);

  // This is a 64 bit unsigned long long int, with its
  // most-significant bit (bit 63) set.
  ullong x2 = 0b1000000000000000000000000000000000000000000000000000000000000000;

  ullong x2_set_bit = 0UL;
  SET_BIT(x2_set_bit, 63);

  EXPECT_EQ(x2, x2_set_bit);
}


TEST(bit_util_test, IS_BIT_SET)
{
  unsigned x1 = 0b101010;

  EXPECT_TRUE(IS_BIT_SET(x1, 1));
  EXPECT_FALSE(IS_BIT_SET(x1, 2));
}


TEST(bit_util_test, CLEAR_BIT)
{
  unsigned	x1	    = 0b101010;
  unsigned	x1_1cleared = 0b101000;

  EXPECT_EQ(CLEAR_BIT(x1, 1), x1_1cleared);

  unsigned	x2	    = 0b101010;
  unsigned	x2_2cleared = 0b101010;	// bit 2 is 0; clear should leave it 0

  EXPECT_EQ(CLEAR_BIT(x2, 2), x2_2cleared);
}


TEST(bit_util_test, TOGGLE_BIT)
{
  ullong x2 = 0b1000000000000000000000000000000000000000000000000000000000000000;

  ullong x2_tgl = x2;

  EXPECT_FALSE(TOGGLE_BIT(x2_tgl, 63));
  EXPECT_TRUE(TOGGLE_BIT(x2_tgl, 63));
  EXPECT_EQ(x2, x2_tgl);
}


TEST(bit_util_test, nset_bits_ull)
{
  unsigned	x1	     = 0b101010;
  unsigned	x1_nset_bits = 3;
  EXPECT_EQ(nset_bits_ull(x1), x1_nset_bits);

  ullong x2 = 0b1000000000000000000000000000000000000000000000000000000000000000;
  unsigned x2_nset_bits = 1;
  EXPECT_EQ(nset_bits_ull(x2), x2_nset_bits);

  ullong x3 = 0b0000000000000000000000000000000000000000000000000000000000000001;
  unsigned x3_nset_bits = 1;
  EXPECT_EQ(nset_bits_ull(x3), x3_nset_bits);

  ullong x4 = 0b0000000000000000000000000000000000000000000000000000000000000000;
  unsigned x4_nset_bits = 0;
  EXPECT_EQ(nset_bits_ull(x4), x4_nset_bits);

  ullong x5 = 0b1000000000000000000000000001000000000000000000000000000000000001;
  unsigned x5_nset_bits = 3;
  EXPECT_EQ(nset_bits_ull(x5), x5_nset_bits);
}

TEST(bit_util_test, nset_bits_ul)
{
  unsigned	x1	     = 0b101010;
  unsigned	x1_nset_bits = 3;
  EXPECT_EQ(nset_bits_ul(x1), x1_nset_bits);

  ulong x2 = 0b10000000000000000000000000000000;
  unsigned x2_nset_bits = 1;
  EXPECT_EQ(nset_bits_ul(x2), x2_nset_bits);

  ulong x3 = 0b00000000000000000000000000000001;
  unsigned x3_nset_bits = 1;
  EXPECT_EQ(nset_bits_ul(x3), x3_nset_bits);

  ulong x4 = 0b00000000000000000000000000000000;
  unsigned x4_nset_bits = 0;
  EXPECT_EQ(nset_bits_ul(x4), x4_nset_bits);

  ulong x5 = 0b10000000000000100000000000000001;
  unsigned x5_nset_bits = 3;
  EXPECT_EQ(nset_bits_ul(x5), x5_nset_bits);
}


TEST(bit_util_test, idx_1_bit)
{
  ullong x1 = 0b10000000000000000000000001010001; // 32 sig. bits in ullong number
  EXPECT_EQ(idx_1_bit(x1, 1), 0);
  EXPECT_EQ(idx_1_bit(x1, 2), 4);
  EXPECT_EQ(idx_1_bit(x1, 3), 6);
  EXPECT_EQ(idx_1_bit(x1, 4), 31);
  EXPECT_EQ(idx_1_bit(x1, 5), -1);

  ullong x2 = 0b00000000000000000000000000000000; // 0 sig. bit in ullong number
  EXPECT_EQ(idx_1_bit(x2, 1), -1);
  EXPECT_EQ(idx_1_bit(x2, 2), -1);
  EXPECT_EQ(idx_1_bit(x2, 3), -1);
}


TEST(bit_util_test, find_uset_bit_pos)
{
  ullong x1 = 0b011010001;
  EXPECT_EQ(idx_0_bit(x1, 1), 1);
  EXPECT_EQ(idx_0_bit(x1, 2), 2);
  EXPECT_EQ(idx_0_bit(x1, 3), 3);
  EXPECT_EQ(idx_0_bit(x1, 4), 5);
  EXPECT_EQ(idx_0_bit(x1, 4+1), 8);
  EXPECT_EQ(idx_0_bit(x1, NBITS_TYPE(ullong)-4-1), 62);
  EXPECT_EQ(idx_0_bit(x1, NBITS_TYPE(ullong)-4),   63);
  EXPECT_EQ(idx_0_bit(x1, NBITS_TYPE(ullong)-4+1), -1);
  EXPECT_EQ(idx_0_bit(x1, NBITS_TYPE(ullong)-4+2), -1);
}


TEST(bit_util_test, idx_least_sig_0_bit)
{
  ullong x1 = 0b00000000000000000000000000000000;
  EXPECT_EQ(idx_least_sig_0_bit(x1), 0);

  ullong x2 = 0b01111111111111111111111111111111;
  EXPECT_EQ(idx_least_sig_0_bit(x2), 31);

  ullong x3 = 0b0000000000000000000000000000000000000000000000000000000000000001;
  EXPECT_EQ(idx_least_sig_0_bit(x3), 1);

  ullong x4 = 0b0111111111111111111111111111111111111111111111111111111111111111;
  EXPECT_EQ(idx_least_sig_0_bit(x4), 63);

  ullong x5 = 0b0111111111111111111111111111111111111111111111111111101111111111;
  EXPECT_EQ(idx_least_sig_0_bit(x5), 10);
}


TEST(bit_util_test, store_sgn_in_bit_pair)
{
  ullong sgns_neutral, sgns_posneg;

  int sgn = 0;
  unsigned idx = 2;
  store_sgn_in_bit_pair(sgn,
			idx,
			&sgns_neutral,
			&sgns_posneg);

  EXPECT_TRUE(IS_BIT_SET(sgns_neutral, idx));


  sgn = 1;
  idx = 1;
  store_sgn_in_bit_pair(sgn,
			idx,
			&sgns_neutral,
			&sgns_posneg);

  EXPECT_TRUE(!IS_BIT_SET(sgns_neutral, idx)); // non-zero
  EXPECT_TRUE(!IS_BIT_SET(sgns_posneg,  idx)); // and non-negative => positive


  sgn = -1;
  idx =  0;
  store_sgn_in_bit_pair(sgn,
			idx,
			&sgns_neutral,
			&sgns_posneg);

  EXPECT_TRUE(!IS_BIT_SET(sgns_neutral, idx)); // non-zero
  EXPECT_TRUE( IS_BIT_SET(sgns_posneg,  idx)); // negative
}


TEST(bit_util_test, bit_pair_to_sgn)
{
  ullong sgns_neutral, sgns_posneg;

  int sgn = 0;
  unsigned idx = 2;
  store_sgn_in_bit_pair(sgn,
			idx,
			&sgns_neutral,
			&sgns_posneg);

  EXPECT_EQ(sgn, bit_pair_to_sgn(sgns_neutral,
				 sgns_posneg,
				 idx));

  sgn = -1;
  idx = 2;
  store_sgn_in_bit_pair(sgn,
			idx,
			&sgns_neutral,
			&sgns_posneg);

  EXPECT_EQ(sgn, bit_pair_to_sgn(sgns_neutral,
				 sgns_posneg,
				 idx));


  sgn = 1;
  idx = 2;
  store_sgn_in_bit_pair(sgn,
			idx,
			&sgns_neutral,
			&sgns_posneg);

  EXPECT_EQ(sgn, bit_pair_to_sgn(sgns_neutral,
				 sgns_posneg,
				 idx));


  sgn = -1;
  idx = 0;
  store_sgn_in_bit_pair(sgn,
			idx,
			&sgns_neutral,
			&sgns_posneg);

  EXPECT_EQ(sgn, bit_pair_to_sgn(sgns_neutral,
				 sgns_posneg,
				 idx));

}
