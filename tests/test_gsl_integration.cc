// test_gsl_integration.cc ---

// Copyright (C) 2016 Omid Khanmohamadi

// Author: Omid Khanmohamadi

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "gsl/gsl_integration.h"
#include "gtest/gtest.h"   // for Google Test framework

double ellipse(double *x)
{
  return 4*x[0]*x[0] + x[1]*x[1] - 4;
}

TEST(gsl_integration_test, glfixed_ellipse)
{
  typedef struct params_s {
    unsigned idx;
    double  *xx;
  } params;

  auto quadratic = [](double x, void *prms)->double
    {
      params *p = (params *) prms;
      unsigned idx = p->idx;
      p->xx[idx] = x;

      return ellipse(p->xx);
    };

  // Gauss-Legendre n-points quadrature is exact for polynomials of
  // degree <=2n-1.
  const unsigned gl_npts = 2;
  const unsigned xdim = 2;
  double xx[xdim];
  double xx_frzn[xdim - 1] = {1};

  double lwr_bnd = 0, upr_bnd = 1;
  double intgs_expected[] = {4.0/3.0 - 3.0, // ∫₀¹(4x²+(1)²-4)
			     1.0/3.0};	    // ∫₀¹(4(1)²+x²-4)

  params quadratic_params;
  quadratic_params.xx = xx;

  gsl_function F;
  F.function = quadratic;
  F.params   = &quadratic_params;

  gsl_integration_glfixed_table * gl_table =
    gsl_integration_glfixed_table_alloc(gl_npts);

  for (unsigned idx = 0; idx < xdim; ++idx){
    double intg_expected = intgs_expected[idx];
    quadratic_params.idx = idx;	// treat xx[idx] as the variable in lambda expr.

    for (unsigned i = 0; i < xdim; ++i){
      if (idx != i)
	xx[i] = xx_frzn[i]; // freeze xx[i != idx] at val xx_frzn[i]
    }

    double intg = gsl_integration_glfixed(&F, lwr_bnd, upr_bnd, gl_table);
    EXPECT_DOUBLE_EQ(intg_expected, intg);
  }

  gsl_integration_glfixed_table_free(gl_table);
}
