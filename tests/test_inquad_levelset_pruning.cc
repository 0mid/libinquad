// test_inquad_levelset_pruning.cc ---

// Copyright (C) 2016 Omid Khanmohamadi

// Author: Omid Khanmohamadi

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "src/inquad_levelset_pruning.h"
#include "tst_functions.h"

#include "gtest/gtest.h"	// for Google Test framework

TEST(inquad_levelset_pruning_test, levelset_subs_unfrzn_center_coords_in_pts)
{
  const unsigned dim = 2;
  double xmins[] = {-2, -2};
  double xmaxs[] = { 2,  2};

  hyprect h;
  ASSERT_TRUE(SUCCESS
	      ==
	      hyprect_make_from_range(dim, xmins, xmaxs, &h));

  levelset phi;
  ASSERT_TRUE(SUCCESS
	      ==
	      levelset_init_pts_on_orig_hyprect(&phi, &h));

  // If no dimensions have been treated (i.e., dim == reduced_dim),
  // then the following is functionally equivalent to a copy, e.g.,
  // memcpy(phi.pts, h.center, dim*sizeof(double));
  levelset_subs_unfrzn_center_coords_in_pts(&phi, &h);

  EXPECT_DOUBLE_EQ(  phi.pts[0],
		    h.center[0]);

  EXPECT_DOUBLE_EQ(  phi.pts[1],
		    h.center[1]);

  hyprect_free(&h);
  levelset_free_pts(&phi);
}

// Unit test to catch the nasty, insidious, out-of-bound write bug
// that only surfaces with 2 points (facets) or more in phi.
TEST(inquad_levelset_pruning_test,
     levelset_subs_unfrzn_center_coords_in_pts_w_2pts)
{
  const unsigned dim = 3;
  double xmins[] = {-2, -1,    1.5};
  double xmaxs[] = { 2,  0.5, -2.5};

  hyprect h;
  ASSERT_TRUE(SUCCESS
	      ==
	      hyprect_make_from_range(dim, xmins, xmaxs, &h));

  levelset phi;
  unsigned npts = 2;
  ASSERT_TRUE(SUCCESS
	      ==
	      levelset_alloc_pts(&phi, dim, npts));
  phi.npts = 2;

  unsigned dim_treated = 1;
  hyprect_mark_dim_as_treated(&h, dim_treated);

  levelset_subs_unfrzn_center_coords_in_pts(&phi, &h);

  EXPECT_DOUBLE_EQ(  phi.pts[0],
		    h.center[0]);

  EXPECT_DOUBLE_EQ(  phi.pts[2],
		    h.center[2]);

  hyprect_free(&h);
  levelset_free_pts(&phi);
}


TEST(inquad_levelset_pruning_test,
     levelset_sgnd_distfun_interface_situation_on_facet)
{

  const unsigned dim = 2;
  const double h1_xmins[] = {-0.5, -0.5};
  const double h1_xmaxs[] = { 0.5,  0.5};

  hyprect h1;
  ASSERT_TRUE(SUCCESS
	      ==
	      hyprect_make_from_range(dim, h1_xmins, h1_xmaxs, &h1));

  levelset phi;
  scalar_field phi_field; phi.field = &phi_field;
  const int sgn = -1;
  ASSERT_TRUE(SUCCESS
	      ==
	      levelset_init_all_but_grad_on_orig_hyprect(&phi, &h1,
							 d_unit_sphere,
							 NULL,
							 sgn));

  // There is only one (center) point/facet; so its indexed is 0.
  const unsigned j = 0;	    // only necessary to set manually in utest

  // Only necessary to calculated manually in utest.
  double facet_radius = hyprect_reduced_radius(&h1);

  // The unit circle has no interface in the square [-0.5,0.5]^2,
  // and our algorithm can decide that.
  EXPECT_TRUE(NO_INTERFACE_BUT_SGN_COND_PASSED
	      ==
	      levelset_sgnd_distfun_interface_situation_on_facet(&phi,
								 j,
								 &h1,
								 facet_radius));

  levelset_free_pts(&phi);

  // ----------------------------------------------------------------------

  // Mark x_1, corresponding to the 'y direction', as treated.
  unsigned dim_treated = 1;
  hyprect_mark_dim_as_treated(&h1, dim_treated);

  // The unit circle has no interface in {(x0,-0.5) | x0 ∈ [-0.5,0.5]},
  // but our algorithm cannot decide that.
  double xc_x1_lo[] = {0.0, -0.5};
  phi.pts = xc_x1_lo;
  facet_radius = hyprect_reduced_radius(&h1);
  EXPECT_TRUE(MAY_HAVE_INTERFACE
	      ==
	      levelset_sgnd_distfun_interface_situation_on_facet(&phi,
								 j,
								 &h1,
								 facet_radius));
  hyprect_free(&h1);

  // ----------------------------------------------------------------------

  const double h2_xmins[] = {-0.5, -1.0};
  const double h2_xmaxs[] = { 0.5,  1.0};

  hyprect h2;
  ASSERT_TRUE(SUCCESS
	      ==
	      hyprect_make_from_range(dim, h2_xmins, h2_xmaxs, &h2));

  ASSERT_TRUE(SUCCESS
	      ==
	      levelset_init_all_but_grad_on_orig_hyprect(&phi, &h2,
							 d_unit_sphere,
							 NULL,
							 sgn));

  // The unit circle has interface in the square [-0.5,0.5]x[-1,1] and
  // our algorithm can decide that.
  facet_radius = hyprect_reduced_radius(&h2);
  EXPECT_TRUE(MAY_HAVE_INTERFACE
	      ==
	      levelset_sgnd_distfun_interface_situation_on_facet(&phi,
								 j,
								 &h2,
								 facet_radius));

  levelset_free_pts(&phi);

  // ----------------------------------------------------------------------

  hyprect_mark_dim_as_treated(&h2, 0);
  double h2_xc_x0_hi[] = {0.5, 0.0};
  phi.pts = h2_xc_x0_hi;

  facet_radius = hyprect_reduced_radius(&h2);
  // The unit circle has interface in {(-0.5,x1) | x1 ∈ [-1,1]} and our
  // algorithm can decide that.
  EXPECT_TRUE(MAY_HAVE_INTERFACE
	      ==
	      levelset_sgnd_distfun_interface_situation_on_facet(&phi,
								 j,
								 &h2,
								 facet_radius));
  hyprect_free(&h2);

  // ----------------------------------------------------------------------

  const double h3_xmins[] = {-0.5,  0.5};
  const double h3_xmaxs[] = { 0.5, 10.0};

  hyprect h3;
  ASSERT_TRUE(SUCCESS
	      ==
	      hyprect_make_from_range(dim, h3_xmins, h3_xmaxs, &h3));

  ASSERT_TRUE(SUCCESS
	      ==
	      levelset_init_all_but_grad_on_orig_hyprect(&phi, &h3,
							 d_unit_sphere,
							 NULL,
							 sgn));

  facet_radius = hyprect_reduced_radius(&h3);
  // The unit circle has interface in the square [-0.5,0.5]x[0.5,10],
  // and our algorithm can decide that.
  EXPECT_TRUE(MAY_HAVE_INTERFACE
	      ==
	      levelset_sgnd_distfun_interface_situation_on_facet(&phi,
								 j,
								 &h3,
								 facet_radius));


  levelset_free_pts(&phi);

  // ----------------------------------------------------------------------

  hyprect_mark_dim_as_treated(&h3, 1);
  double h3_xc_x1_lo_hi[] = {0.0, 0.5,
			     0.5, 10.5/2};
  phi.pts = h3_xc_x1_lo_hi;

  facet_radius = hyprect_reduced_radius(&h3);
  // The unit circle has no interface in {(x0, 0.5) | x0 ∈ [-0.5,0.5]}
  // (i.e., x0^2+x1^2=1 does not intersect the line x1=0.5 when
  // -0.5≤x0≤0.5), but our algorithm cannot decide that.
  EXPECT_TRUE(MAY_HAVE_INTERFACE
	      ==
	      levelset_sgnd_distfun_interface_situation_on_facet(&phi,
								 j,
								 &h3,
								 facet_radius));

  hyprect_mark_dim_as_untreated(&h3, 1); // used/available only during utesting
  hyprect_mark_dim_as_treated(&h3, 0);

  facet_radius = hyprect_reduced_radius(&h3);
  // The unit circle has interface in {(0.5, x1) | x1 ∈ [0.5,10]}
  // (i.e., x0^2+x1^2=1 intersects the line x0=0.5 when 0.5≤x1≤10.0),
  // and our algorithm can decide that.
  EXPECT_TRUE(MAY_HAVE_INTERFACE
	      ==
	      levelset_sgnd_distfun_interface_situation_on_facet(&phi,
								 j+1,
								 &h3,
								 facet_radius));
  hyprect_free(&h3);

  // ----------------------------------------------------------------------

  const double h4_xmins[] = {-0.5,   2.0};
  const double h4_xmaxs[] = { 0.5,   4.0};

  hyprect h4;
  ASSERT_TRUE(SUCCESS
	      ==
	      hyprect_make_from_range(dim, h4_xmins, h4_xmaxs, &h4));

  ASSERT_TRUE(SUCCESS
	      ==
	      levelset_init_all_but_grad_on_orig_hyprect(&phi, &h4,
							 d_unit_sphere,
							 NULL,
							 sgn));

  facet_radius = hyprect_reduced_radius(&h4);
  // The unit circle no interface in (and is outside of) the square
  // [-0.5,0.5]x[2.0,4.0], and our algorithm can decide that.
  EXPECT_TRUE(NO_INTERFACE_AND_SGN_COND_FAILED
	      ==
	      levelset_sgnd_distfun_interface_situation_on_facet(&phi,
								 j,
								 &h4,
								 facet_radius));
  hyprect_free(&h4);
  levelset_free_pts(&phi);
}


TEST(inquad_levelset_pruning_test,
     levelset_mark_pts_on_facets_w_possible_interface)
{
  const unsigned dim = 2;
  const double h1_xmins[] = {-0.5, -0.5};
  const double h1_xmaxs[] = { 0.5,  0.5};

  hyprect h1;
  ASSERT_TRUE(SUCCESS
	      ==
	      hyprect_make_from_range(dim, h1_xmins, h1_xmaxs, &h1));

  levelset phi;
  scalar_field phi_field; phi.field = &phi_field;
  const int sgn = -1;
  ASSERT_TRUE(SUCCESS
	      ==
	      levelset_init_all_but_grad_on_orig_hyprect(&phi, &h1,
							 d_unit_sphere,
							 NULL,
							 sgn));

  // The unit circle has no interface in (but is inside) the square
  // [-0.5,0.5]^2, and our algorithm can decide that. Hence, no facets
  // (equivalently center points) are marked (to be kept).
  levelset_pruning_data phi_prn_data;
  EXPECT_TRUE(MARK_SUCCESS
	      ==
	      levelset_mark_pts_on_facets_w_possible_interface(&phi,
							       &h1,
							       &phi_prn_data));

  ullong	pts2keep_expected  = zero_ULL;
  unsigned	npts2keep_expected = 0;
  EXPECT_EQ(phi_prn_data.pts2keep,
			 pts2keep_expected);

  EXPECT_EQ(phi_prn_data.npts2keep,
			 npts2keep_expected);

  hyprect_free(&h1);
  levelset_free_pts(&phi);

  // ----------------------------------------------------------------------

  const double h2_xmins[] = {-0.5, -1.0};
  const double h2_xmaxs[] = { 0.5,  1.0};

  hyprect h2;
  ASSERT_TRUE(SUCCESS
	      ==
	      hyprect_make_from_range(dim, h2_xmins, h2_xmaxs, &h2));

  ASSERT_TRUE(SUCCESS
	      ==
	      levelset_init_all_but_grad_on_orig_hyprect(&phi, &h2,
							 d_unit_sphere,
							 NULL,
							 sgn));

  // The unit circle has interface in the square [-0.5,0.5]x[-1,1] and
  // our algorithm can decide that. Hence, this facet (equivalently
  // its center point) is marked (to be kept).
  EXPECT_TRUE(MARK_SUCCESS
	      ==
	      levelset_mark_pts_on_facets_w_possible_interface(&phi,
							       &h2,
							       &phi_prn_data));

  pts2keep_expected = zero_ULL;	// reset to 00...0 (i.e., keep no points)
  SET_BIT(pts2keep_expected, 0);
  npts2keep_expected = 1;

  EXPECT_EQ(phi_prn_data.pts2keep,
			 pts2keep_expected);

  EXPECT_EQ(phi_prn_data.npts2keep,
			 npts2keep_expected);

  levelset_free_pts(&phi);

  // ----------------------------------------------------------------------

  hyprect_mark_dim_as_treated(&h2, 0);
  // Center points of facets corresponding to dim 0 having been marked
  // as treated (frozen) above. Only necessary to supply explicitly
  // for utesting. Since no dimensions have been previously treated in
  // our utest, treating one dimension a pair of (low and high)
  // facets. If we later treat another dimension, we get another pair
  // of facets for each facet we currently have, etc.
  double cen_pts_of_facets_w_x0_treated[] = {-0.5, 0.0,  // x0 = -0.5
					      0.5, 0.0}; // x0 =  0.5

  levelset phi_new;
  scalar_field phi_new_field; phi_new.field = &phi_new_field;
  phi_new.field->function = d_unit_sphere;
  phi_new.pts  = cen_pts_of_facets_w_x0_treated;
  phi_new.dim  = 2;
  phi_new.npts = 2;


  levelset_pruning_data phi_new_prn_data;

  // The unit circle has interface in {(x0,x1) | x0 ∈ {-0.5, 0.5}, x1
  // ∈ [-1,1]} and our algorithm can decide that.
  EXPECT_TRUE(MARK_SUCCESS
	      ==
	      levelset_mark_pts_on_facets_w_possible_interface(&phi_new,
							       &h2,
							       &phi_new_prn_data));

  pts2keep_expected = zero_ULL;	// reset to 00...0 (i.e., keep no points)
  SET_BIT(pts2keep_expected, 0);
  SET_BIT(pts2keep_expected, 1);
  npts2keep_expected = 2;

  EXPECT_EQ(phi_new_prn_data.pts2keep,
			     pts2keep_expected);

  EXPECT_EQ(phi_new_prn_data.npts2keep,
			     npts2keep_expected);

  hyprect_free(&h2);

  // ----------------------------------------------------------------------

  const double h4_xmins[] = {-0.5, 2.0};
  const double h4_xmaxs[] = { 0.5, 4.0};

  hyprect h4;
  ASSERT_TRUE(SUCCESS
	      ==
	      hyprect_make_from_range(dim, h4_xmins, h4_xmaxs, &h4));

  ASSERT_TRUE(SUCCESS
	      ==
	      levelset_init_all_but_grad_on_orig_hyprect(&phi, &h4,
							 d_unit_sphere,
							 NULL,
							 sgn));

  // The unit circle has no interface in (and is outside of) the
  // square [-0.5,0.5]x[2.0,4.0], and our algorithm can decide that.
  EXPECT_TRUE(MARK_NA_DOMAIN_EMPTY
	      ==
	      levelset_mark_pts_on_facets_w_possible_interface(&phi,
							       &h4,
							       &phi_prn_data));
  hyprect_free(&h4);
  levelset_free_pts(&phi);
}


TEST(inquad_levelset_pruning_test,
     levelset_pack_marked_pts_forget_rest)
{
  const unsigned dim = 2;
  const double h1_xmins[] = {-0.5, -0.5};
  const double h1_xmaxs[] = { 0.5,  0.5};

  hyprect h1;
  ASSERT_TRUE(SUCCESS
	      ==
	      hyprect_make_from_range(dim, h1_xmins, h1_xmaxs, &h1));

  levelset phi;
  scalar_field phi_field; phi.field = &phi_field;
  const int sgn = -1;
  ASSERT_TRUE(SUCCESS
	      ==
	      levelset_init_all_but_grad_on_orig_hyprect(&phi, &h1,
							 d_unit_sphere,
							 NULL,
							 sgn));

  levelset_pruning_data phi_prn_data;
  EXPECT_TRUE(MARK_SUCCESS
	      ==
	      levelset_mark_pts_on_facets_w_possible_interface(&phi,
							       &h1,
							       &phi_prn_data));

  // The unit circle has no interface in (but is inside) the square
  // [-0.5,0.5]^2, and our algorithm can decide that. Hence, no facets
  // (equivalently center points) are marked (to be kept). As a
  // result, phi.npts will be set to zero by the following call.
  levelset_pack_marked_pts_forget_rest(&phi,
				       &phi_prn_data);

  unsigned phi_npts_expected = 0;

  EXPECT_EQ(phi.npts,
	    phi_npts_expected);

  hyprect_free(&h1);
  levelset_free_pts(&phi);

  // ----------------------------------------------------------------------

  const double h2_xmins[] = {-0.5, -1.0};
  const double h2_xmaxs[] = { 0.5,  1.0};

  hyprect h2;
  ASSERT_TRUE(SUCCESS
	      ==
	      hyprect_make_from_range(dim, h2_xmins, h2_xmaxs, &h2));

  hyprect_mark_dim_as_treated(&h2, 0);
  // Center points of low and high facets corresponding to dim 0
  // having been marked as treated (frozen) above. Only necessary to
  // supply explicitly for utesting.
  double cen_pts_of_facets_w_x0_treated[] = {-0.5, 0.0,
					      0.5, 0.0};

  phi.field->function = d_unit_sphere;
  phi.pts  = cen_pts_of_facets_w_x0_treated;
  phi.npts = 2;
  phi.dim  = 2;

  // The unit circle has interface in {(x0,x1) | x0 ∈ {-0.5, 0.5}, x1
  // ∈ [-1,1]} and our algorithm can decide that.
  EXPECT_TRUE(MARK_SUCCESS
	      ==
	      levelset_mark_pts_on_facets_w_possible_interface(&phi,
							       &h2,
							       &phi_prn_data));
  levelset_pack_marked_pts_forget_rest(&phi,
				       &phi_prn_data);

  phi_npts_expected = 2;

  EXPECT_EQ(phi.npts,
	    phi_npts_expected);

  const double *phi_pt_j	  = phi.pts;
  const double *phi_pt_j_expected = cen_pts_of_facets_w_x0_treated;
  for (unsigned j = 0; j < phi.npts; ++j) {
    for (unsigned i = 0; i < phi.dim; ++i) {
      EXPECT_EQ(phi_pt_j[i],
		phi_pt_j_expected[i]);
    }

    phi_pt_j	      += dim;
    phi_pt_j_expected += dim;
  }

  hyprect_free(&h2);
}
