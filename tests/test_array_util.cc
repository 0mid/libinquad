// test_inquad_utils.cc ---

// Copyright (C) 2016 Omid Khanmohamadi

// Author: Omid Khanmohamadi

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "src/array_util.h"

#include "gtest/gtest.h"   // for Google Test framework

TEST(array_utils_test, argmax)
{
  double As[] = {
    6, 7, 9, 2, 1,
    5, 2, 5, 5,
    -1, -0, 0, -1, 0, -1.2
  };

  unsigned As_dims[] = {
    5,
    4,
    6
  };

  double As_argmaxs[] = {
    2,
    0,
    1
  };

  double *Ai = As;
  for (unsigned i = 0; i < NELEMS(As_argmaxs); ++i){
    Ai += i ? As_dims[i-1] : 0;
    EXPECT_EQ(As_argmaxs[i], argmax(Ai, As_dims[i]));
  }
}

TEST(array_utils_test, argmax_abs)
{
  double As[] = {
    6, 7, -9, 2, 1,
    -5, 2, 5, 5,
    -1, -0, 0, -1, 0, -1.2, 1.2
  };

  unsigned As_dims[] = {
    5,
    4,
    7
  };

  double As_argmax_abs[] = {
    2,
    0,
    5
  };

  double *Ai = As;
  for (unsigned i = 0; i < NELEMS(As_argmax_abs); ++i){
    Ai += i ? As_dims[i-1] : 0;
    EXPECT_EQ(As_argmax_abs[i], argmax_abs(Ai, As_dims[i]));
  }
}

TEST(array_utils_test, norm2)
{
  double vs[] = {
    0,  0, 0,
    -1, 0, 0,
    3,  4,
    -3, 4,
    1,  1
  };

  unsigned vs_dims[] = {
    3,
    3,
    2,
    2,
    2
  };

  double vs_l2[] = {
    0,
    1,
    5,
    5,
    sqrt(2)
  };

  double *vi = vs;		// vs "decays" to addr. of 1st elem. of vs
  for (unsigned i = 0; i < NELEMS(vs_l2); ++i){
    vi += i ? vs_dims[i-1] : 0;	// offset from the prev. elem. (0 at start)
    EXPECT_DOUBLE_EQ(vs_l2[i], norm2(vi, vs_dims[i]));
  }
}
