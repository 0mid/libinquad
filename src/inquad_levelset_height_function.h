// inquad_levelset_height_function.h ---

// Copyright (C) 2016 Omid Khanmohamadi

// Author: Omid Khanmohamadi

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef INQUAD_LEVELSET_HEIGHT_FUNCTION_INCLUDED
#define INQUAD_LEVELSET_HEIGHT_FUNCTION_INCLUDED

#include "inquad_levelset.h"	// for levelset
#include "inquad_geometry.h"	// for hyprect
#include "return_code.h"
#include "cond_compile.h"	// for TESTING

errcode levelset_alloc_grad_workspace(levelset * phi);

// Output in phi->grad_value.
//
// Before this function is called, storage must be allocated for
// phi->grad_value, perhaps by calling levelset_alloc_grad_workspace.
void
levelset_eval_grad_on_facet(levelset * const      phi,
			    const double * const  x,  // pt on facet
			    ulong treated_dims); // for treated_dims


// Before calling levelset_direction_gives_height_fun, we need to have
// called levelset_alloc_grad_workspace(), either once per new hyprect
// (allowing for a serial or parallel run) or only once in the initial
// hyprect (allowing for a serial run only).
bool
levelset_direction_gives_height_function(levelset * const	phi,
					 unsigned		k, // dir
					 const hyprect * const  h,
					 bool is_surf,
					 ullong * const partial_sgns_neutral,
					 ullong * const partial_sgns_posneg);


void levelset_duplicate_pts_and_fill_in_new_frzn_coord(levelset * const phi,
						       unsigned frzn_coord,
						       const hyprect * const h);

void levelset_free_grad_workspace(levelset * const phi);

void
levelset_set_new_facets_sgn_conds(levelset * const phi,
				  bool   is_surf,
				  ullong partial_sgns_neutral,
				  ullong partial_sgns_posneg);

#if TESTING

// Output in phi->pts.
void
levelset_duplicate_pts(levelset * const phi);

// Output in phi->pts.
// frzn_coord is the new height function direction (k).
void
levelset_fill_new_frzn_coord_in_pts(levelset * const		phi,
				    unsigned			frzn_coord,
				    const hyprect * const	h);

double
levelset_partial_on_facet_from_grad(levelset * const		phi,
				    const double * const	x,
				    unsigned			k,
				    ulong			treated_dims);

int
levelset_sgn_cond_new_lo_facet(const levelset * const	phi,
			       unsigned			j,
			       bool			is_surf,
			       ullong			partial_sgns_neutral,
			       ullong			partial_sgns_posneg);

int
levelset_sgn_cond_new_hi_facet(const levelset * const	phi,
			       unsigned			j,
			       bool			is_surf,
			       ullong			partial_sgns_neutral,
			       ullong			partial_sgns_posneg);

#endif

#endif // INQUAD_LEVELSET_HEIGHT_FUNCTION_INCLUDED
