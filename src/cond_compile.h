// cond_compile.h ---

// Copyright (C) 2017 Omid Khanmohamadi

// Author: Omid Khanmohamadi

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef COND_COMPILE_H_INCLUDED
#define COND_COMPILE_H_INCLUDED

#include <stdio.h>		// for fprintf, stderr

#if TESTING
#define STATIC_UNLESS_TESTING
#else
#define STATIC_UNLESS_TESTING static
#endif

#if TRACE
#define WRT_STDERR 1
#else
#define WRT_STDERR 0
#endif

#if WRITEQPTS
#define WRT_QPT_FILE 1
#else
#define WRT_QPT_FILE 0
#endif

// Variable-argument macros were introduced in 1999 in the ISO/IEC
// 9899:1999 (C99) revision of the C language standard, and in 2011 in
// ISO/IEC 14882:2011 (C++11) revision of the C++ language standard
// [http://en.wikipedia.org/wiki/Variadic_macro].
//
// See https://stackoverflow.com/a/1644898 for details.
#define dbgprintf(...) \
  do { if (WRT_STDERR) fprintf(stderr, __VA_ARGS__); } while (0)

#define dbgprintf_array(dim, idx, ...)			\
  do {							\
    if (WRT_STDERR) {					\
      for (unsigned idx = 0; idx < dim; ++idx) {	\
	fprintf(stderr, __VA_ARGS__);			\
      }							\
    }							\
  } while (0)

#define MACRO_SEMICOLON ;
#define MACRO_COMMA ,

#define wrtinvoke_statements(STATEMENTS)	\
  do { if (WRT_QPT_FILE) { STATEMENTS }		\
  } while (0)

#define wrtprintf_array_if(dim, idx, cond, filename, ...)	\
  do {								\
    if (WRT_QPT_FILE) {						\
      if (cond) {						\
	FILE *fptr = fopen(filename, "a");			\
	if (fptr) {						\
	  for (unsigned idx = 0; idx < dim; ++idx) {		\
	    fprintf(fptr, __VA_ARGS__);				\
	  }							\
	  fprintf(fptr, "\n");					\
	  fclose(fptr);						\
	}							\
      }								\
    }								\
  } while (0)

#endif // COND_COMPILE_H_INCLUDED
