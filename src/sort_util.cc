// sort_util.cc ---

// Copyright (C) 2016 Omid Khanmohamadi

// Author: Omid Khanmohamadi

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "cond_compile.h"	// for STATIC_UNLESS_TESTING

#include <stdlib.h>		// for qsort

STATIC_UNLESS_TESTING int
compare_d(const void *x_p, const void *y_p)
{
  double x = *(double *) x_p;
  double y = *(double *) y_p;
  if (x < y) return -1;
  if (x > y) return  1;
  return 0;
}


void darray_qsort(unsigned dim, double *darray)
{
  qsort(darray, dim, sizeof(double), compare_d);
}
