// inquad_levelset.cc ---

// Copyright (C) 2016 Omid Khanmohamadi

// Author: Omid Khanmohamadi

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "inquad_levelset.h"

#include <stdlib.h>  // for malloc, free, NULL
#include <string.h>  // for memcpy

#if TESTING
#define INIT_NPTS_MULT 2
#else
#define INIT_NPTS_MULT 1
#endif

// Having one (or a few) function(s) in a translation unit call the
// rest provides the compiler with more information about the
// interrelation of these functions, which results in more
// opportunities for optimization (including inlining). For more
// information on inline and other linkage directives (extern and
// static), and in particular why inline is not used in this
// translation unit, see https://stackoverflow.com/a/1759575.

// Output in phi->pts.
STATIC_UNLESS_TESTING errcode
levelset_alloc_pts(levelset * const	phi,
		   unsigned		dim,
		   unsigned		npts)

{
  phi->pts = (double *) malloc(dim*npts * sizeof(double));
  if (NULL == phi->pts) {
    return ERR_MALLOC;
  }

  // We are not updating phi->npts here, as this function is used for
  // allocating space for the old pts (which will be copied over) as
  // well as the new pts, which may be created during height function
  // detection); before that, some old points might be pruned. As a
  // result, npts will be updated at the end, when we get to
  // levelset_pack_marked_pts_forget_rest.
  phi->dim = dim;

  return SUCCESS;
}


void levelset_free_pts(levelset * const phi)
{
  free(phi->pts);
}


STATIC_UNLESS_TESTING errcode
levelset_init_pts_on_orig_hyprect(levelset * const phi,
				  const hyprect * const h)

{
  unsigned dim = h->dim;

  // In the beginning there is only one facet (the whole hyprect).
  // Since there is a one-to-one correspondence between a facet and
  // its center point, we have only one point too.
  const unsigned npts = 1;

  // If not testing, allocate only npts (as opposed to 2*npts), since
  // upon the first call to inquad(), during the pruning phase, we
  // will allocate npts and copy the initial npts over to a
  // phi_pruned, which will in turn be copied (if we decide we have a
  // height function in the direction of the largest partial) into the
  // pts of a phi_new for which we have allocated 2*npts storage.
  //
  // If testing, allocate 2*npts, so that we can use this same level
  // set to test inquad height functions that work with 2*npts storage
  // (e.g. levelset_duplicate_pts).
  errcode ecode;
  if (SUCCESS != (ecode = levelset_alloc_pts(phi,
					     dim,
					     INIT_NPTS_MULT * npts))) {
    return ecode;
  }

  memcpy(phi->pts, h->center, dim * sizeof(double));

  phi->npts = npts;

  return SUCCESS;
}

// Output in phi->{pts, dim, npts, function}.
STATIC_UNLESS_TESTING errcode
levelset_init_all_but_grad_on_orig_hyprect(levelset * const		 phi,
					   const hyprect * const	 h,
					   scalar_field_func		 function,
					   void				*params,
					   int				 sgn)

{
  errcode ecode;
  if (SUCCESS != (ecode = levelset_init_pts_on_orig_hyprect(phi, h))){
    return ecode;
  }

  phi->field->function	= function;
  phi->field->params	= params;

  phi->sgns_neutral = zero_ULL;
  phi->sgns_posneg  = zero_ULL;
  const unsigned sgn_bit_idx = 0;
  store_sgn_in_bit_pair(sgn, sgn_bit_idx,
			&(phi->sgns_neutral),
			&(phi->sgns_posneg));

  return SUCCESS;
}

// Output in phi->{pts, dim, npts, function}.
//
// This function is called exactly once (by the user) to initialize
// the level set on the original hyprect.
//
// After this function is called and after the desired task involving
// phi is finished and before phi goes out of scope,
// levelset_free_pts(phi) must be called.
errcode
levelset_init_on_orig_hyprect(levelset * const		 phi,
			      const hyprect * const	 h,
			      scalar_field_func		 function,
			      void			*function_params,
			      vector_field_func		 grad_function,
			      void			*grad_function_params,
			      int			 sgn)

{
  errcode ecode;
  if (SUCCESS
      !=
      (ecode = levelset_init_all_but_grad_on_orig_hyprect(phi,
							  h,
							  function,
							  function_params,
							  sgn))) {
    return ecode;
  }

  phi->grad_field->function	= grad_function;
  phi->grad_field->params	= grad_function_params;

  return SUCCESS;
}


STATIC_UNLESS_TESTING void
levelset_copy_pts(levelset * const		phi_new,
		  const levelset * const	phi)

{
  unsigned npts = phi->npts;
  unsigned dim  = phi->dim;

  memcpy(phi_new->pts, phi->pts, dim*npts * sizeof(double));
}


STATIC_UNLESS_TESTING void
levelset_copy_all_but_pts_and_sgns(levelset * const		phi_new,
				   const levelset * const	phi)

{
  // field->params is set separately for each recursion level (to
  // 'integrand_value') and MUST NOT be copied.
  phi_new->field->function = phi->field->function;

  phi_new->grad_field->function = phi->grad_field->function;
  phi_new->grad_field->params   = phi->grad_field->params;

  // If we evaluate the integrals on sub-hyprects in parallel, these
  // need be DEEP COPIED. Currently we don't, so they are SHALLOW
  // COPIED.
  phi_new->grad_pt    = phi->grad_pt;
  phi_new->grad_value = phi->grad_value;

  phi_new->dim  = phi->dim;
  phi_new->npts = phi->npts;
}


void
levelset_copy_sgns(levelset * const		phi_new,
		   const levelset * const	phi)

{
  phi_new->sgns_neutral		= phi->sgns_neutral;
  phi_new->sgns_posneg		= phi->sgns_posneg;
}


STATIC_UNLESS_TESTING errcode
levelset_copy_no_sgns_w_multiplier_times_storage_for_pts(levelset * const
							 phi_new,

							 const levelset * const
							 phi,

							 unsigned
							 multiplier)

{
  errcode ecode;
  if (SUCCESS != (ecode = levelset_alloc_pts(phi_new,
					     phi->dim,
					     multiplier*(phi->npts)))) {
    return ecode;
  }

  levelset_copy_pts(phi_new, phi);
  levelset_copy_all_but_pts_and_sgns(phi_new, phi);

  return SUCCESS;
}


errcode levelset_copy_no_sgns(levelset * const		phi_new,
			      const levelset * const	phi)

{
  const unsigned storage_multiplier = 1;

  return
    levelset_copy_no_sgns_w_multiplier_times_storage_for_pts(phi_new,
							     phi,
							     storage_multiplier);
}



errcode
levelset_copy_no_sgns_w_twice_storage_for_pts(levelset * const		phi_new,
					      const levelset * const	phi)

{
  const unsigned storage_multiplier = 2;

  return
    levelset_copy_no_sgns_w_multiplier_times_storage_for_pts(phi_new,
							     phi,
							     storage_multiplier);
}

errcode
levelset_copy(levelset * const		phi_new,
	      const levelset * const	phi)

{
  levelset_copy_sgns(phi_new, phi);
  return levelset_copy_no_sgns(phi_new, phi);
}


// This function is called (through the auxiliary function
// levelset_subs_unfrzn_center_coords_in_pts) during the pruning stage
// (with treated_dims from h) as the recursion stack is being set up
// from bottom (dimension n) to top (dimension 1). During this stage,
// this function is run once for every dimension of a hyprect, and
// double that every time the hyprect halves. This function is also
// called (with treated_dims from prms and NOT h) by integrand_value()
// when the recursion stack is being evaluated from top (dimension 1)
// to bottom (dimension dim), once per dimension, and double that if
// any hyprect's have halved.
void
levelset_subs_eval_pt_coords_in_unfrzn_coords_of_pts(levelset * const
						     phi,

						     ulong
						     treated_dims,

						     const double * const
						     eval_pt)

{
  unsigned	 dim  = phi->dim;
  unsigned	 npts = phi->npts;

  for (unsigned i = 0; i < dim; ++i) { // for every coord i
    double *pt_j = phi->pts;

    if (!IS_BIT_SET(treated_dims, i)) { // if dim i of h has not been treated
      for (unsigned j = 0; j < npts; ++j) {

	pt_j[i] = eval_pt[i]; //set val of that coord to center[i], ∀ pt_j's

	pt_j += dim;
      }
    }
  }
}


void levelset_subs_unfrzn_center_coords_in_pts(levelset * const		phi,
					       const hyprect * const	h)

{
  levelset_subs_eval_pt_coords_in_unfrzn_coords_of_pts(phi,
						       h->treated_dims,
						       h->center);
}


void levelset_subs_value_in_coord_of_pts(levelset * const	phi,
					 unsigned		dir,
					 double			value)

{
  double	*pt_j = phi->pts;
  unsigned	 dim  = phi->dim;
  unsigned	 npts = phi->npts;

  for (unsigned j = 0; j < npts; ++j) {

    pt_j[dir] = value;

    pt_j += dim;
  }
}


bool
levelset_sgn_cond_satisfied_on_facet(const levelset * const	phi,
				     unsigned			j,
				     double			phi_xc)

{
  int sgn_cond_facet_j = bit_pair_to_sgn(phi->sgns_neutral,
					 phi->sgns_posneg,
					 j);
  if (sgn_cond_facet_j == 0) {	// either sign on this facet is ok
    return true;
  }

  if (sgn_cond_facet_j == 1) {	// sign must be non-negative on this facet
    return phi_xc >= 0;
  }
  else {			// sign must be non-positive on this facet
    return phi_xc <= 0;
  }
}


bool
levelset_sgn_cond_satisfied_on_all_facets(const levelset * const phi)

{
  unsigned npts = phi->npts;
  unsigned dim  = phi->dim;
  const double * pt_j = phi->pts;

  for (unsigned j = 0; j < npts; ++j) {
    double sgnd_dist_pt_j_to_bndry = LEVELSET_FN_VAL(phi, pt_j);
    if (!levelset_sgn_cond_satisfied_on_facet(phi, j,
					      sgnd_dist_pt_j_to_bndry)) {
      return false;
    }

    pt_j += dim;
  }

  return true;
}
