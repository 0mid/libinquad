// bit_util.cc ---

// Copyright (C) 2016 Omid Khanmohamadi

// Author: Omid Khanmohamadi

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "bit_util.h"

// With GCC, we can just use '__builtin_popcountll' on unsigned long
// long arguments, where in the worst case the compiler will generate
// a call to a function. In the best case the compiler will emit a CPU
// instruction to do the same job faster.
//
// Published in 1988, the C Programming Language 2nd Ed. (by Brian W.
// Kernighan and Dennis M. Ritchie) mentions this in exercise 2-9. On
// April 19, 2006 Don Knuth pointed out to me [Seander] that this
// method "was first published by Peter Wegner in CACM 3 (1960), 322.
// Also discovered independently by Derrick Lehmer and published in
// 1964 in a book edited by Beckenbach
// [https://graphics.stanford.edu/~seander/bithacks.html].
unsigned nset_bits_ull(ullong num)
{
#if defined(__GNUC__)
  // https://gcc.gnu.org/onlinedocs/gcc/Other-Builtins.html
  return __builtin_popcountll(num);
#else
  unsigned c; // c accumulates the number of bits set in num
  for (c = 0; num; ++c)
    {
      num &= num - 1; // clear the least significant bit set
    }

  return c;
#endif
}


unsigned nset_bits_ul(ulong num)
{
#if defined(__GNUC__)
  // https://gcc.gnu.org/onlinedocs/gcc/Other-Builtins.html
  return __builtin_popcountl(num);
#else
  unsigned c; // c accumulates the number of bits set in num
  for (c = 0; num; ++c)
    {
      num &= num - 1; // clear the least significant bit set
    }

  return c;
#endif
}


// Find position of dth (starting from 1st) set bit of num, counting
// from the right (LSB). For the number 000000000001010001, e.g., the
// position of d=1(st) set bit is 0; the position of d=2(nd) set bit
// is 4; the position of d=3(rd) set bit is 6; the position of d=4(th)
// set bit is undefined. We return -1 to signify the last case.
int idx_1_bit(ullong num, unsigned d)
{

  unsigned nbits_num  = NBITS_TYPE(ullong);

  unsigned running_1_count = 0;
  for (unsigned i = 0; i < nbits_num; ++i) {
    if (IS_BIT_SET(num, i)) {
      ++running_1_count;
    }

    if (running_1_count == d) {
      return i;
    }
  }

  // If we reach this point, running_1_count must have always been
  // strictly less than d. Since we have counted all the set bits in
  // num at this point, there is no dth set bit in num and we report
  // this case by returning -1.
  return -1;
}


// Find position of dth (starting from 1st) unset bit of num, counting
// from the right (LSB). For the number 0b011010001, e.g., the
// position of d=1(st) unset bit is 1; the position of d=2(nd) unset
// bit is 2; the position of d=3(rd) unset bit is 3; the position of
// d=4(th) unset bit is 6. The position of d=5(th) unset bit is 8. The
// position of d>4(th) unset bit is defined only for 5≤d≤64-4=60,
// where 4 is the number of non-trailing zeros in the number given. It
// is undefined for d>60, in which case we return -1.
int idx_0_bit(ullong num, unsigned d)
{

  unsigned nbits_num  = NBITS_TYPE(ullong);

  unsigned running_0_count = 0;
  for (unsigned i = 0; i < nbits_num; ++i) {
    if (!IS_BIT_SET(num, i)) {
      ++running_0_count;
    }

    if (running_0_count == d) {
      return i;
    }
  }

  return -1;
}


// Return the position of the least-significant 0 bit of num (e.g., it
// returns 0 if the LSB is 0, it returns 1 if the 2 LSBs are 01,
// etc.).
unsigned idx_least_sig_0_bit(ullong num)
{
  // Idea of using __builtin_ctz from Steven G. Johnson's cubature.
#if defined(__GNUC__) &&					\
  ((__GNUC__ == 3 && __GNUC_MINOR__ >= 4) || __GNUC__ > 3)
  return __builtin_ctzll(~num);	// gcc builtin for version >= 3.4
#else
  idx_0_bit(num, 1);
#endif
}


// Output in sgns_{neutral, posneg}.
void
store_sgn_in_bit_pair(int	 sgn,
		      unsigned	 idx, // idx of bit to store sgn in
		      ullong	*sgns_neutral,
		      ullong	*sgns_posneg)
{
  switch (sgn) {
  case 0:
    SET_BIT(  *sgns_neutral, idx); // zero
    CLEAR_BIT(*sgns_posneg,  idx); // clear for good form (in case not 0 init'ed)
    break;

  case 1:
    CLEAR_BIT(*sgns_neutral, idx); // non-zero (clear in case not 0 init'ed)
    CLEAR_BIT(*sgns_posneg,  idx); // and non-negative => positive
    break;

  case -1:
    SET_BIT(  *sgns_posneg,  idx); // negative
    CLEAR_BIT(*sgns_neutral, idx); // non-zero (clear in case not 0 init'ed)
    break;
  }
}


int bit_pair_to_sgn(ullong	sgns_neutral,
		    ullong	sgns_posneg,
		    unsigned	idx)

{
  if (IS_BIT_SET(sgns_neutral, idx)) {
    return 0;
  }

  else if (IS_BIT_SET(sgns_posneg, idx)) {
    return -1;
  }

  else {
    return 1;
  }
}
