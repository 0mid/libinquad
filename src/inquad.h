// inquad.h ---

// Copyright (C) 2016 Omid Khanmohamadi

// Author: Omid Khanmohamadi

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef INQUAD_H_INCLUDED
#define INQUAD_H_INCLUDED

#include "inquad_geometry.h"	// for hyprect
#include "inquad_levelset.h"	// for levelset
#include "inquad_field.h"	// for integrand_field
#include "inquad_params.h"	// for quad_params

errcode
inquad(integrand_field			*f,
       const levelset * const		 phi, // from inner ∫; a copy is modified
       hyprect * const			 h,
       bool				 is_surf,
       const inquad_params * const	 inqparams,
       double				*integral_value); // final ∫ value

#endif // INQUAD_H_INCLUDED
