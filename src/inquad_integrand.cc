// inquad_integrand.cc ---

// Copyright (C) 2017 Omid Khanmohamadi

// Author: Omid Khanmohamadi

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "inquad_integrand.h"
#include "inquad_levelset_root.h" // for levelset_find_roots_on_..., partition
#include "sort_util.h"		  // for darray_qsort
#include "inquad_levelset_height_function.h" // for levelset_grad_on_facet
#include "norm2.h"			     // for norm2

#include "gsl/gsl_integration.h" // for gsl_integration_glfixed, gsl_function

#include <stdlib.h>		// for malloc, free, NULL
#include <stdio.h>		// for printf (debugging)

// If we ARE TESTING, the following ARE defined in the header file and
// MUST NOT be redefined here.
#if !TESTING
#include "inquad_integrand_gsl_function.h"
#endif


// We need '**eval_pt' (a pointer to pointer) here, as we want to
// write a pointer value (returned by malloc) to a variable (outside
// this function) whose address is passed in as a pointer.
errcode integrand_eval_pt_alloc(unsigned	  dim,
				double		**eval_pt)

{
  *eval_pt = (double *) malloc(dim * sizeof(double));
  if (NULL == *eval_pt) {
    return ERR_MALLOC;
  }

  return SUCCESS;
}


void integrand_eval_pt_free(double * const eval_pt)

{
  free(eval_pt);
}


// The name integrand_set_params would have been confusing for the
// following function, as its first argument is a pointer to
// integrand_params to be modified, not to a integrand_field (which would
// be the integrand).
void
integrand_params_set_all_but_partition_pts(integrand_params * const f_new_params,
					   integrand_field *f,
					   levelset * const phi,
					   hyprect * const h,
					   unsigned dir,
					   const inquad_params * const inqparams)

{
  f_new_params->field	     = f;
  f_new_params->lvlst	     = phi;
  f_new_params->hrect	     = h;

  f_new_params->inqparams    = inqparams;

  // It is crucial to keep a copy of h->treated_dims at this recursion
  // level, as h->treated_dims will be modified by the upcoming
  // recursion levels and we need to use the value for this level when
  // we come back during the recursive integrand evaluation stage.
  f_new_params->treated_dims = h->treated_dims;
  f_new_params->dir	     = dir;
  f_new_params->dim	     = phi->dim;
}


// Only run at the recursion level of outermost integral.
errcode
integrand_params_alloc_partition_pts(integrand_params * const f_new_params,
				     unsigned npts)

{
  // The number of points, npts, (equivalently the number of facets)
  // at the recursion level of outermost integral is >= than that of
  // all other recursion levels. At any recursion level except that of
  // the outermost integral we may have AT MOST one root per facet (as
  // required by the implicit function theorem), resulting in npts
  // roots AT MOST, where npts is that of the level we are at. At the
  // level of the outermost integral we may have any number of roots.
  // We allocate 1*npts initially and then grow by doubling the size
  // if need be.
  f_new_params->partition_pts = (double *) malloc(npts * sizeof(double));
  if (NULL == f_new_params->partition_pts) {
    return ERR_MALLOC;
  }

  return SUCCESS;
}


// Only run at the recursion level of outermost integral.
void
integrand_params_free_partition_pts(integrand_params * const f_new_params)

{
  free(f_new_params->partition_pts);
}

// This defines the function y ↦ f({x[0], ..., y, x[dim]}) =: F_1d(y),
// where y is the value to which the position dir of x is set.
STATIC_UNLESS_TESTING double
function_1d(double y, void *params)
{
  gsl_function_params *prms = (gsl_function_params *)params;

  prms->eval_pt[prms->dir] = y;

  dbgprintf("dir = %d, eval_pt[dir] = %f\n",
	    prms->dir,
	    prms->dir,
	    prms->eval_pt[prms->dir]);


  wrtprintf_array_if(prms->dim, i,
		     (prms->reduced_dim == prms->dim)
		     &&
		     (prms->quad_outfname[0] != '\0'),
		     prms->quad_outfname,
		     "%f ", prms->eval_pt[i]);

  // Notice that, in the following evaluation 'call', we pass (by
  // reference) as the last argument the updated eval_pt from this
  // integrand to the inner integrand.
  return INTEGRAND_FLD_VAL(prms->field, prms->dim, prms->eval_pt);
}


STATIC_UNLESS_TESTING void
integrand_set_gsl_function_along_eval_pt_dir(gsl_function * const
					     F_1d,

					     gsl_function_params * const
					     F_1d_params,

					     double * const
					     eval_pt,

					     unsigned
					     dir,

					     unsigned
					     dim,

					     unsigned
					     reduced_dim,

					     const char * const
					     quad_outfname,

					     integrand_field * const
					     f,

					     const gsl_function_func
					     F_1d_function)

{
  F_1d_params->field	     = f;
  F_1d_params->eval_pt	     = eval_pt;
  F_1d_params->dir	     = dir;
  F_1d_params->dim	     = dim;
  F_1d_params->reduced_dim   = reduced_dim;
  F_1d_params->quad_outfname = quad_outfname;

  F_1d->function = F_1d_function;
  F_1d->params   = F_1d_params;
}


STATIC_UNLESS_TESTING double
integrand_1d_value_in_subinterval(gsl_function * const
				  F_1d,

				  gsl_integration_glfixed_table
				  *gauss_legendre_tbl,

				  double
				  subint_lendpt,

				  double
				  subint_rendpt)

{
  return gsl_integration_glfixed(F_1d,
				 subint_lendpt,
				 subint_rendpt,
				 gauss_legendre_tbl);
}


STATIC_UNLESS_TESTING ulong
integrand_params_treated_dims(const integrand_params * const params)

{
  return params->treated_dims;
}


STATIC_UNLESS_TESTING unsigned
integrand_params_reduced_dim(const integrand_params * const params)

{
  return
    params->dim - nset_bits_ul(integrand_params_treated_dims(params));
}


// This defines the function x ↦ ∫f(x[0], ..., y, x[dim])dy as the
// integrand for the next outer integral.
double integrand_value(unsigned		 dim,
		       double * const	 eval_pt,
		       void		*params)

{
  // Current integrand params.
  integrand_params * const prms = (integrand_params *) params;

  levelset * const		phi		    = prms->lvlst;
  hyprect * const		h		    = prms->hrect;
  double * const		partition_pts	    = prms->partition_pts;
  const inquad_params * const	inqparams	    = prms->inqparams;
  const unsigned		ngauss_legendre_pts = inqparams->quad_npts;
  const char * const		quad_outfname	    = inqparams->quad_outfname;

  // It is crucial to use the prms->treated_dims for this recursion
  // level, which we saved during the recursion integrand set-up, and
  // NOT the h->treated_dims, which has been modified by outer
  // integral recursion levels since we left this level. For this
  // reason, we are using helper functions here to make this
  // distinction clear.
  ulong			 treated_dims  = integrand_params_treated_dims(prms);
  unsigned		 dir	       = prms->dir;
  integrand_field	*f	       = prms->field;
  unsigned		 reduced_dim   = integrand_params_reduced_dim(prms);

  // Output in phi->pts (modifies unfrzn coords of phi->pts).
  levelset_subs_eval_pt_coords_in_unfrzn_coords_of_pts(phi,
						       treated_dims,
						       eval_pt);

  // ----------------------------------------------------------------------

  double interval_left_end  = h->center[dir] - h->halfwidths[dir];
  double interval_right_end = h->center[dir] + h->halfwidths[dir];

  root_situation rt_situation = AT_MOST_ONE_ROOT_PER_FACET;

  // At the level of the outermost ∫, we may have several roots per
  // facet.
  if (reduced_dim == 1) {
    // Use a different root finding method (e.g., find roots of a
    // Chebyshev interpolatory polynomial), as at the recursion level
    // of the outermost integral there is NO guarantee anymore (as
    // there was by the implicit function theorem for all inner
    // integral levels) that we have at most root per facet. We may
    // have more. Brent or Bisection (or 'untweaked' Newton for that
    // matter) can at best deal with one root.

    rt_situation = MAY_HAVE_SEVERAL_ROOTS_PER_FACET;
  }

  // Output in partition_pts. (Extra) Side-effect: Modifies unfrzn
  // coord 'dir' of phi->pts during root finding process. No need to
  // make a deep copy of phi->pts, as coord 'dir' is the only one
  // modified and other coord's (to be used by upcoming recursion
  // levels) are not touched.
  //
  // We write roots in partition_pts starting from position
  // 'partition_pts + 1' to leave position 0 for interval_left_end.
  int nroots =
    levelset_find_roots_in_interval_along_dir_on_facets(phi,
							interval_left_end,
							interval_right_end,
							dir,
							rt_situation,
							inqparams,
							partition_pts + 1);

  // Put in end points of the interval.
  partition_pts[0]	    = interval_left_end;
  partition_pts[nroots + 1] = interval_right_end;

  // Sorting is necessary only if we have more than 1 root.
  if (nroots > 1) {
    // Only sort the roots part of partition_pts (hence the ' + 1'),
    // as the roots in this interval are bounded below and above by
    // interval_left_end and interval_right_end, respectively.
    darray_qsort(nroots, partition_pts + 1);
  }

  unsigned npartition_pts = nroots + 2;

  dbgprintf("\nENTERING dir %d, where reduced_dim = %d\n", dir, reduced_dim);
  dbgprintf("partition_pts in dx_%d = ", dir);
  dbgprintf_array(npartition_pts, j, "%f ", partition_pts[j]);
  dbgprintf("\n");

  // ----------------------------------------------------------------------

  gsl_function F_1d;
  gsl_function_params F_1d_params;
  integrand_set_gsl_function_along_eval_pt_dir(&F_1d,
					       &F_1d_params,
					       eval_pt,
					       dir,
					       dim,
					       reduced_dim,
					       quad_outfname,
					       f,
					       function_1d);

  gsl_integration_glfixed_table *gauss_legendre_tbl =
    gsl_integration_glfixed_table_alloc(ngauss_legendre_pts);

  double intg_value = 0.0;
  for (unsigned j = 1; j < npartition_pts; ++j) {

    double subint_lendpt = partition_pts[j-1];
    double subint_rendpt = partition_pts[j];

    double subint_midpt = 0.5*(subint_lendpt + subint_rendpt);
    levelset_subs_value_in_coord_of_pts(phi, dir, subint_midpt);

    if (levelset_sgn_cond_satisfied_on_all_facets(phi)) {
      dbgprintf("In [%f, %f] of dx_%d \n", subint_lendpt, subint_rendpt, dir);

      intg_value += integrand_1d_value_in_subinterval(&F_1d,
						      gauss_legendre_tbl,
						      subint_lendpt,
						      subint_rendpt);

      dbgprintf("Integrand_%d = ∫ dx_%d = %f\n", dir, dir, intg_value);
    }
  }

  dbgprintf("LEAVING dir %d\n\n", dir);

  gsl_integration_glfixed_table_free(gauss_legendre_tbl);

  return intg_value;
}


double surf_integrand_value(unsigned		 dim,
			    double * const	 eval_pt,
			    void		*params)
{
  // Current integrand params.
  integrand_params * const prms = (integrand_params *) params;

  levelset * const		phi	  = prms->lvlst;
  hyprect * const		h	  = prms->hrect;
  const inquad_params * const	inqparams = prms->inqparams;

  // It is crucial to use the prms->treated_dims for this recursion
  // level, which we saved during the recursion integrand set-up, and
  // NOT the h->treated_dims, which has been modified by outer
  // integral recursion levels since we left this level. For this
  // reason, we are using helper functions here to make this
  // distinction clear.
  ulong			 treated_dims  = integrand_params_treated_dims(prms);
  unsigned		 dir	       = prms->dir;
  integrand_field	*f	       = prms->field;
  unsigned		 reduced_dim   = integrand_params_reduced_dim(prms);

  // Output in phi->pts (modifies unfrzn coords of phi->pts).
  levelset_subs_eval_pt_coords_in_unfrzn_coords_of_pts(phi,
						       treated_dims,
						       eval_pt);

  // ----------------------------------------------------------------------

  double interval_left_end  = h->center[dir] - h->halfwidths[dir];
  double interval_right_end = h->center[dir] + h->halfwidths[dir];

  // At this recursion level there is in fact only one facet, with at
  // most one root in direction 'dir'.
  root_situation rt_situation = AT_MOST_ONE_ROOT_PER_FACET;

  double root;
  // Output in root. (Extra) Side-effect: Modifies unfrzn
  // coord 'dir' of phi->pts during root finding process. No need to
  // make a deep copy of phi->pts, as coord 'dir' is the only one
  // modified and other coord's (to be used by upcoming recursion
  // levels) are not touched.
  int nroots =
    levelset_find_roots_in_interval_along_dir_on_facets(phi,
							interval_left_end,
							interval_right_end,
							dir,
							rt_situation,
							inqparams,
							&root);

  dbgprintf("\nENTERING (surf) dir %d, where reduced_dim = %d\n",
	    dir, reduced_dim);

  if (nroots == 1) {
    dbgprintf("root in dx_%d = %f\n", dir, root);

    double * const pt = phi->pts;
    levelset_subs_value_in_coord_of_pts(phi, dir, root);

    // Output in phi->grad_value.
    levelset_eval_grad_on_facet(phi, pt, treated_dims);

    // Notice that, in the following evaluation 'call', we pass (by
    // reference) as the last argument the updated pt from this
    // integrand to the inner integrand.
    double	f_at_pt		= INTEGRAND_FLD_VAL(f, dim, pt);
    double	Dphi_at_pt	= norm2(phi->grad_value, dim);
    double	D_dir_phi_at_pt = phi->grad_value[dir];

    dbgprintf("D_%d_phi(pt) = %f\n", dir, D_dir_phi_at_pt);

    return f_at_pt * Dphi_at_pt / fabs(D_dir_phi_at_pt);
  }
  else {
    dbgprintf("surf_integrand_value = 0.0\n\n");
    return 0.0;
  }
}
