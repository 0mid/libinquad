// inquad_cubature.h ---

// Copyright (C) 2016 Omid Khanmohamadi

// Author: Omid Khanmohamadi

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef INQUAD_CUBATURE_H_INCLUDED
#define INQUAD_CUBATURE_H_INCLUDED

#include "inquad_geometry.h"	// for hyprect
#include "inquad_field.h"	// for integrand_field
#include "return_code.h"	// for errcode
#include "cond_compile.h"	// for TESTING

errcode hyprect_apply_cubature(const hyprect * const	 h,
			       integrand_field		*integrand,
			       double * const		 integral_val);

#if TESTING

int cubature_integrand(unsigned		 xdim,
		       const double *	 x,
		       void		*cubature_integrand_prms,
		       unsigned		 fdim,
		       double		*fval);

#endif

#endif // INQUAD_CUBATURE_H_INCLUDED
