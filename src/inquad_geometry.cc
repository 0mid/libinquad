// inquad_geometry.cc ---

// Copyright (C) 2016 Omid Khanmohamadi

// Author: Omid Khanmohamadi

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "inquad_geometry.h"
#include "return_code.h"

#include <stdlib.h>		// for malloc, free, size_t
#include <string.h>		// for memcpy
#include <math.h>		// for sqrt

static
errcode hyprect_alloc(unsigned dim, hyprect * const h)
{
  // C: sizeof is an operator; 'sizeof double' and 'sizeof double' are
  // interchangeable. malloc's return value (void *) is safely
  // converted to 'type *' of the variable it's being assigned to. In
  // fact, the cast (double *) is not necessary and in fact not
  // recommended.
  //
  // C++: only sizeof(double) is allowed. 'sizeof double' leads to
  // "error: expected primary-expression before 'double'". The cast is
  // mandatory, leaving it out leads to "error: invalid conversion
  // from 'void*' to 'double*' [-fpermissive]".
  //
  // Allocate memory for both h->center and h->halfwidths arrays in
  // one contiguous block of memory, the beginning of which is pointed
  // to by h->center. Then, get a pointer to the beginning of
  // h->halfwidths array by adding dim to h->center (compiler takes
  // care of pointer arithmetic).
  //
  // C++-specific way (non-Modern, non-Resource Acquisition Is
  // Initialization):
  //
  // h->center = new double[2*dim];  // delete[] h->center later.
  // h->halfwidths = h->center + dim; // nothing to deallocate later.
  h->center = (double *) malloc(2*dim * sizeof(double)); // & free(h->center)
  if (NULL == h->center)
    return ERR_MALLOC;

  h->halfwidths = h->center + dim; // nothing to deallocate later here

  return SUCCESS;
}

// No computation happens inside hyprect_make (as opposed to
// hyprect_make_from_range). User-provided data (center and
// halfwidths) is simply copied to the (heap) memory allocated by
// hyprect_alloc.
errcode hyprect_make(unsigned dim,
		     const double *center,
		     const double *halfwidths,
		     hyprect * const h)
{
  errcode ecode;
  if (SUCCESS != (ecode = hyprect_alloc(dim, h)))
    return ecode;

  h->dim	  = dim;
  h->reduced_dim  = dim;
  h->treated_dims = zero_UL;

  // #include <string.h>
  // void *memcpy(void *dest, const void *src, size_t n);
  //
  // The memcpy() function copies n bytes from memory area src to
  // memory area dest. The memory areas must not overlap. Use
  // memmove if the memory areas do overlap.
  //
  // The following loop does the same, but perhaps less efficiently.
  // (cf. https://stackoverflow.com/q/7776085.)
  //
  // for (unsigned i = 0; i < dim; ++i){
  //   h->center[i] = center[i];
  //   h->halfwidths[i] = halfwidths[i];
  // }
  size_t dim_double_bytes = dim * sizeof(double);
  memcpy(h->center,     center,     dim_double_bytes);
  memcpy(h->halfwidths, halfwidths, dim_double_bytes);

  return SUCCESS;
}


errcode hyprect_make_from_range(unsigned dim,
				const double *xmins,
				const double *xmaxs,
				hyprect * const h)
{
  errcode ecode;
  if (SUCCESS != (ecode = hyprect_alloc(dim, h)))
    return ecode;

  h->dim	  = dim;
  h->reduced_dim  = dim;
  h->treated_dims = zero_UL;

  for (unsigned i = 0; i < dim; ++i) {
    h->center[i]     = 0.5 * (xmaxs[i] + xmins[i]);
    h->halfwidths[i] = 0.5 * (xmaxs[i] - xmins[i]);
  }

  return SUCCESS;
}


void hyprect_free(hyprect * const h)
{
  // Nothing to deallocate for h->halfwidths, as it's just a pointer
  // to the middle of h->center.
  //
  // C++-specific way (if allocated using new[]):
  //
  // delete[] h->center;
  free(h->center);
}


// Output in h_dest, which this function points to the memory it
// allocates for copying the contents of h_src.
//
// It is crucial that we make a 'deep copy' here to use for one of the
// two halves of a hyprect after it's been split. A 'shallow copy' (as
// done, e.g., by doing h_dest = h_src) does not copy the data
// referenced by the pointer members of hyprect; it only copies the
// pointers, resulting in both halves sharing the same data. This in
// turn leads to completely wrong behavior if one half undergoes a
// different pruning than the other.
STATIC_UNLESS_TESTING errcode
hyprect_copy(hyprect * const h_src, hyprect * const h_dest)
{
  unsigned dim = h_src->dim;

  errcode ecode;
  if (SUCCESS != (ecode = hyprect_alloc(dim, h_dest)))
    return ecode;

  h_dest->dim	       = dim;
  h_dest->reduced_dim  = h_src->reduced_dim;
  h_dest->treated_dims = h_src->treated_dims;

  memcpy(h_dest->center, h_src->center, 2*dim * sizeof(double));

  return SUCCESS;
}


errcode hyprect_halve(hyprect * const h_left,
		      hyprect * const h_right,
		      unsigned dim2halve)
// This is not a 'pure' function; it mutates both hyprect's pointed to
// by h_left and h_right.
{
  h_left->halfwidths[dim2halve] *= 0.5;

  // Make a (deep) copy AFTER changing the dim2halve entry of
  // halfwidths, so that both h_left and h_right have a copy of the
  // updated halfwidths, as well as copies of center (which is updated
  // below).
  errcode ecode;
  if (SUCCESS != (ecode = hyprect_copy(h_left, h_right)))
    return ecode;

  double halfwidth_dim2halve = h_left->halfwidths[dim2halve];

  // Shift the center of the old hyprect by new halfwidths[dim2halve]
  // (half of the old halfwidths[dim2halve]) to the left (along dim
  // dim2halve). Shift the center of the new hyprect the same amount in
  // the opposite direction.
  h_left->center[dim2halve]  -= halfwidth_dim2halve;
  h_right->center[dim2halve] += halfwidth_dim2halve;

  return SUCCESS;
}

// This function is only called when the hyprect is fully inside the
// zero-levelset (i.e., the hyprect does not contain any interface.)
double hyprect_volume(const hyprect * const h)
{
  unsigned	dim	     = h->dim;
  unsigned	treated_dims = h->treated_dims;

  double volume = 1.0;

  for (unsigned i = 0; i < dim; ++i) {
    if (!IS_BIT_SET(treated_dims, i))
      volume *= 2.0 * h->halfwidths[i];
  }

  return volume;
}


// ----------------------------------------------------------------------
// [START] Used ONLY by inquad_cubature
//
errcode hyprect_range_alloc(const hyprect * const h,
			    hyprect_range * hrange)
{
  unsigned reduced_dim = h->reduced_dim;

  hrange->xmins = (double *) malloc(2*reduced_dim * sizeof(double));
  if (NULL == hrange->xmins) {
    return ERR_MALLOC;
  }

  hrange->xmaxs = hrange->xmins + reduced_dim;

  return SUCCESS;
}


void hyprect_range_free(hyprect_range * hrange)
{
  free(hrange->xmins);
}


void hyprect_range_calc(const hyprect * const h,
			hyprect_range * const hrange)
{
  unsigned	dim	     = h->dim;
  ulong		treated_dims = h->treated_dims;

  for (unsigned i = 0; i < dim; ++i) {
    if (!IS_BIT_SET(treated_dims, i)) { // cond T exactly reduced_dim # of times
      hrange->xmins[i] = h->center[i] - h->halfwidths[i];
      hrange->xmaxs[i] = h->center[i] + h->halfwidths[i];
    }
  }
}
//
// [END] Used ONLY by inquad_cubature
// ----------------------------------------------------------------------


// Output in xv.
//
// This function is called exactly once by hyprect_facet_set_next_vertex().
void static
hyprect_facet_set_1st_vertex(const hyprect * const h,
			     const double * const xc,
			     double * const xv)
{
  unsigned		dim	     = h->dim;
  ulong			treated_dims = h->treated_dims;
  const double * const	halfwidths   = h->halfwidths;

  size_t nbytes_xc = dim * sizeof(double);
  memcpy(xv, xc, nbytes_xc);

  // Set the 1st term of the (Gray-code) sequence to correspond to the
  // vertex (++...+), i.e., the vertex with xc[i]+halfwidths[i], for
  // all coordinates i which have NOT been treated (are free).
  for (unsigned i = 0; i < dim; ++i) {
    if (!IS_BIT_SET(treated_dims, i)) {
      xv[i] += halfwidths[i];
    }
  }
}

// Output in xv.
//
// WARNING: Since it uses 'static' (i.e., it keeps a state), this
// 'function' is NOT thread-safe; in particular, it may NOT be invoked
// in parallel. This, however, is not a practical limitation, since
// its job is to set xv to the vertices of a given facet (identified
// by xc) one at a time (following a Gray-code ordering sequence).
int
hyprect_facet_set_next_vertex(const hyprect * const h,
			      const double * const xc, // THE center of facet
			      double * const xv)       // A vertex of facet
{
  unsigned		reduced_dim  = h->reduced_dim;
  ulong			treated_dims = h->treated_dims;
  const double * const	halfwidths   = h->halfwidths;

  const int	i_init	   = -1;
  const ulong   signs_init = zero_UL;
  static int	i	   = i_init;
  static ulong	signs	   = signs_init;

  if (i == i_init) {
    hyprect_facet_set_1st_vertex(h, xc, xv);

    return ++i;
  }

  unsigned d = idx_least_sig_0_bit(i);

  if (d >= reduced_dim) {
    // If d ≥ reduced_dim (which is the number of DOFs of a reduced
    // hyprect---aka a facet), we have traversed all vertices. Return
    // ITER_STOP to the caller so that it stops calling us! Before
    // that, however, reset 'i' and 'signs' to their initial values,
    // so that we are ready for generating the terms of another
    // sequence upon a subsequent call.
    i	  = i_init;
    signs = signs_init;

    return ITER_STOP;
  }

  // Find position p of the dth unset (0) bit of treated_dims.
  unsigned dth = d + 1;
  unsigned p = idx_0_bit(treated_dims, dth);

 // Flip xv[p].
  TOGGLE_BIT(signs, p);
  xv[p] = IS_BIT_SET(signs, p)?
    xc[p] - halfwidths[p]:
    xc[p] + halfwidths[p];

  return ++i;
}


void hyprect_mark_dim_as_treated(hyprect * const	h,
				 unsigned		d)
{
  SET_BIT(h->treated_dims, d);
  h->reduced_dim -= 1;
}


double hyprect_reduced_radius(const hyprect * const h)
{
  unsigned		dim	     = h->dim;
  ulong			treated_dims = h->treated_dims;
  const double * const  halfwidths   = h->halfwidths;

  double sum_sqrs_non_treated_halfwidths = 0.0;
  for (unsigned i = 0; i < dim; ++i) {
    if (!IS_BIT_SET(treated_dims, i))
      sum_sqrs_non_treated_halfwidths += halfwidths[i]*halfwidths[i];
  }

  return sqrt(sum_sqrs_non_treated_halfwidths);
}


// The following function is used only during unit testing. So, both
// its definition (implementation; here, in .cc) and declaration
// (prototype; in .h) are 'conditionally compiled.'
#if TESTING

void hyprect_mark_dim_as_untreated(hyprect * const	h,
				   unsigned		d)
{
  CLEAR_BIT(h->treated_dims, d);
  h->reduced_dim += 1;
}

#endif
