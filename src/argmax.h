#ifndef ARGMAX_H_INCLUDED
#define ARGMAX_H_INCLUDED

unsigned argmax(const double *A, unsigned xdim);

unsigned argmax_abs(const double * const A, unsigned dim);

#endif // ARGMAX_H_INCLUDED
