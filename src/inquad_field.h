// inquad_field.h ---

// Copyright (C) 2016 Omid Khanmohamadi

// Author: Omid Khanmohamadi

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef INQUAD_FIELD_H_INCLUDED
#define INQUAD_FIELD_H_INCLUDED


typedef double (* integrand_field_func) (unsigned	 dim,
					 double * const	 x,
					 void		*params);

typedef struct integrand_field_s {
  integrand_field_func	 function;
  void			*params;
} integrand_field;


typedef double (* scalar_field_func) (unsigned			 dim,
				      const double * const	 x,
				      void			*params);

typedef struct scalar_field_s {
  scalar_field_func function;
  void		   *params;
} scalar_field;


typedef void (* vector_field_func) (unsigned			 dim,
				    const double * const	 x,
				    void			*params,
				    double * const		 value);


typedef struct vector_field_s {
  vector_field_func	 function;
  void			*params;
} vector_field;

#endif // INQUAD_FIELD_H_INCLUDED
