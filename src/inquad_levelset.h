// inquad_levelset.h ---

// Copyright (C) 2016 Omid Khanmohamadi

// Author: Omid Khanmohamadi

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef INQUAD_LEVELSET_INCLUDED
#define INQUAD_LEVELSET_INCLUDED

#include "bit_util.h"		// for ullong
#include "inquad_field.h"	// for {scalar,grad,curv}_field
#include "return_code.h"	// for errcode
#include "cond_compile.h"	// for TESTING
#include "inquad_geometry.h"	// for hyprect

typedef struct levelset_s {
  scalar_field  *field;	// struct ptr w/ function pointing to x→φ(x)
  vector_field	*grad_field; // struct ptr w/ function pointing to x→∇φ/|∇φ|(x)
  double	*grad_pt;    // len: dim; evaluation pt x for ∇φ/|∇φ|(x)
  double	*grad_value; // len: dim; D_i φ/|∇φ|(x) in pos. i
  double	*pts; // allocated len: dim*2npts; used len: all or half
  ullong	 sgns_neutral; // bit i is 1 if sgn cond facet i of reduced h is 0
  ullong	 sgns_posneg; // bit i is 1 if sgn cond facet i of reduced h is -1
  unsigned	 dim; // for ease of access; identical to dim of orig hyprect h
  unsigned	 npts;
} levelset;

#define LEVELSET_FN_VAL(phi, x)						\
  (*((phi)->field->function))( (phi)->dim, x, (phi)->field->params )

errcode
levelset_init_on_orig_hyprect(levelset * const		 phi,
			      const hyprect * const	 h,
			      scalar_field_func		 function,
			      void			*function_params,
			      vector_field_func		 grad_function,
			      void			*grad_function_params,
			      int			 sgn);

errcode levelset_copy_no_sgns(levelset * const		phi_new,
			      const levelset * const	phi);

errcode
levelset_copy_no_sgns_w_twice_storage_for_pts(levelset * const		phi_new,
					      const levelset * const	phi);

void levelset_free_pts(levelset * const phi);

void
levelset_subs_eval_pt_coords_in_unfrzn_coords_of_pts(levelset * const
						     phi,

						     ulong
						     treated_dims,

						     const double * const
						     eval_pt);

void levelset_subs_unfrzn_center_coords_in_pts(levelset * const		phi,
					       const hyprect * const	h);

void levelset_subs_value_in_coord_of_pts(levelset * const	phi,
					 unsigned		dir,
					 double			value);

bool
levelset_sgn_cond_satisfied_on_facet(const levelset * const	phi,
				     unsigned			j,
				     double			phi_xc);

void
levelset_copy_sgns(levelset * const		phi_new,
		   const levelset * const	phi);

errcode
levelset_copy(levelset * const		phi_new,
	      const levelset * const	phi);

bool
levelset_sgn_cond_satisfied_on_all_facets(const levelset * const phi);

#if TESTING

errcode
levelset_init_all_but_grad_on_orig_hyprect(levelset * const		 phi,
					   const hyprect * const	 h,
					   scalar_field_func		 function,
					   void				*params,
					   int				 sgn);

errcode
levelset_init_pts_on_orig_hyprect(levelset * const phi,
				  const hyprect * const h);

errcode levelset_alloc_pts(levelset * const phi,
			   unsigned dim,
			   unsigned npts);

// Output in phi_new->pts. This function copies the evaluation pts
// from the inner integral (previous recursion level) to the current
// recursion level. It is needed so that we can substitute the
// unfrozen coordinates of the center of the hyprect of the current
// recursion level in the evaluation points at the current recursion
// level without modifying the evaluation points of the previous
// recursion level.
void levelset_copy_pts(levelset * const		phi_new,
		       const levelset * const	phi);

void
levelset_copy_all_but_pts_and_sgns(levelset * const		phi_new,
				   const levelset * const	phi);

errcode
levelset_copy_no_sgns_w_multiplier_times_storage_for_pts(levelset * const
							 phi_new,

							 const levelset * const
							 phi,

							 unsigned
							 multiplier);

#endif

#endif	// INQUAD_LEVELSET_INCLUDED
