// basic_algebra.cc ---

// Copyright (C) 2016 Omid Khanmohamadi

// Author: Omid Khanmohamadi

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.


double square_d(double x)
{
  return x*x;
}


// This function does not differentiate between positive and negative
// zeros. It also considers zero and positive doubles to have the same
// sign. This is suitable, e.g., when we are looking for sign
// /changes/ (i.e., zero crossings).
bool have_same_sign_zero_pos(double x, double y)
{
  return ((x < 0) == (y < 0));
}


// This function does not differentiate between positive and negative
// zeros. It also treats zero as 'signless', having the same sign as
// either a positive or negative number.
bool have_same_sign_zero_signless(double x, double y)
{
  if (x == 0 || y == 0) return true;

  return ((x < 0) == (y < 0));
}


int signum(double x)
{
  return (x > 0)? 1 : ((x < 0)? -1 : 0);
}
