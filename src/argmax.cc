// argmax.cc ---

// Copyright (C) 2017 Omid Khanmohamadi

// Author: Omid Khanmohamadi

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <math.h>		// for fabs

unsigned argmax(const double * const A, unsigned dim)
{
  double      max_A = A[0];
  unsigned argmax_A = 0;

  for (unsigned i = 1; i < dim; ++i){
    if (A[i] > max_A){
      max_A = A[i];
      argmax_A = i;
    }
  }

  return argmax_A;
}


unsigned argmax_abs(const double * const A, unsigned dim)
{
  double	max_abs_A    = fabs(A[0]);
  unsigned	argmax_abs_A = 0;

  for (unsigned i = 1; i < dim; ++i){

    double abs_A_i = fabs(A[i]);
    if (abs_A_i > max_abs_A){
      max_abs_A	   = abs_A_i;
      argmax_abs_A = i;
    }
  }

  return argmax_abs_A;
}
