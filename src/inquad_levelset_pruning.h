// inquad_levelset_pruning.h ---

// Copyright (C) 2016 Omid Khanmohamadi

// Author: Omid Khanmohamadi

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef INQUAD_PRUNING_INCLUDED
#define INQUAD_PRUNING_INCLUDED

#include "inquad_levelset.h"	// for levelset
#include "inquad_geometry.h"	// for hyprect
#include "bit_util.h" // for ullong (and bit ops in implementation file)
#include "return_code.h"	// for errcode
#include "cond_compile.h"	// for TESTING

// This struct is used only during the pruning stage. (As a result,
// after some contemplation, I decided not to have the levelset struct
// point to it, as it has no use, or meaning, after that stage.) It is
// also unique to each recursion depth, and not shared among different
// depths. Hence, it will be stack allocated in each recursion depth.
typedef struct levelset_pruning_data_s {
  ullong	pts2keep;
  unsigned	npts2keep;
} levelset_pruning_data;

typedef enum interface_situation_e {
  MAY_HAVE_INTERFACE = 0,
  NO_INTERFACE_BUT_SGN_COND_PASSED,
  NO_INTERFACE_AND_SGN_COND_FAILED
} interface_situation;

typedef enum mark_retcode_e {
  MARK_SUCCESS = 0,
  MARK_NA_DOMAIN_EMPTY // marking not applicable; domain of int is empty
} mark_retcode;

// Output in phi_prn_data->{pts2keep,npts2keep}, which must be
// allocated (e.g., on the stack of the caller) and passed in.
//
// phi->pts is NOT modified by this function.
mark_retcode
levelset_mark_pts_on_facets_w_possible_interface(const levelset * const
						 phi,

						 const hyprect * const
						 h,

						 levelset_pruning_data * const
						 phi_prn_data);

void
levelset_pack_marked_pts_forget_rest(levelset * const
				     phi,

				     const levelset_pruning_data * const
				     phi_prn_data);

#if TESTING

interface_situation
levelset_sgnd_distfun_interface_situation_on_facet(const levelset * const phi,
						   unsigned		  j,
						   const hyprect * const  h,
						   double facet_radius);

#endif

#endif	// INQUAD_PRUNING_INCLUDED
