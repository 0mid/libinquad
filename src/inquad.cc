// inquad.cc ---

// Copyright (C) 2016 Omid Khanmohamadi

// Author: Omid Khanmohamadi

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "inquad.h"
#include "inquad_levelset_pruning.h"
#include "return_code.h"	// for errcode, SUCCESS
#include "inquad_cubature.h"
#include "inquad_levelset_height_function.h"
#include "argmax.h"
#include "inquad_integrand.h"
#include "bit_util.h"		// for idx_least_sig_0_bit

#include <stdbool.h> // for bool in C (>= C99); fund. type in C++; need no header
#include <stdlib.h>  // for malloc, free, NULL
#include <math.h>    // for fabs

static const unsigned max_nsubdivs = 16;

errcode
inquad(integrand_field			*f,
       const levelset * const		 phi,	// from inner ∫; a copy is modified
       hyprect * const			 h,
       bool				 is_surf,
       const inquad_params * const	 inqparams,
       double				*integral_value) // final ∫ value

{

  unsigned	dim	    = h->dim;
  unsigned	reduced_dim = h->reduced_dim;

  levelset	phi_pruned;
  scalar_field	phi_pruned_field;
  vector_field	phi_pruned_grad_field;
  phi_pruned.field	= &phi_pruned_field;
  phi_pruned.grad_field = &phi_pruned_grad_field;

  errcode ecode;

  if (SUCCESS
      !=
      (ecode = levelset_copy(&phi_pruned, phi))) {

    return ecode;
  }

  // Base case of recursion.
  if (reduced_dim == 1) {

    // We do NOT form a f_final here, just its params
    // (f_final_params), as we are in the base case of the recursion
    // and we will be calling integrand_value DIRECTLY. We CANNOT call
    // integrand_value INDIRECTLY---by setting a f_final to point to
    // it and calling inquad(&f_final, ...)---as that would result in
    // having no base case and an infinite recursion.
    integrand_params f_final_params;
    if (SUCCESS
	!=
	(ecode = integrand_params_alloc_partition_pts(&f_final_params,
						      phi_pruned.npts + 2))) {
      levelset_free_pts(&phi_pruned);
      return ecode;
    }

    // This evaluation point is shared by all levels. As a result, it
    // is only allocated at this (outermost) level and freed when we
    // return from this level (i.e., when we are done evaluating the
    // integral).
    double *eval_pt;
    if (SUCCESS
	!=
	(ecode = integrand_eval_pt_alloc(dim, &eval_pt))) {

      levelset_free_pts(&phi_pruned);
      integrand_params_free_partition_pts(&f_final_params);
      return ecode;
    }

    // The only remaining integration direction, 'dir_final',
    // corresponds to the least significant 0 (i.e., unset) bit in
    // h->treated_dims.
    unsigned dir_final = idx_least_sig_0_bit(h->treated_dims);

    dbgprintf("SETTING UP (FINAL) Integrand_%d = ∫ dx_%d\n",
	      dir_final, dir_final);

    integrand_params_set_all_but_partition_pts(&f_final_params,
					       f,
					       &phi_pruned,
					       h,
					       dir_final,
					       inqparams);

    *integral_value = integrand_value(dim, eval_pt, &f_final_params);

    dbgprintf("Integral = %f\n", *integral_value);

    levelset_free_pts(&phi_pruned);
    integrand_params_free_partition_pts(&f_final_params);
    integrand_eval_pt_free(eval_pt);

    return SUCCESS;
  }

  // Set facet center points for evaluating the levelset during
  // pruning.
  levelset_subs_unfrzn_center_coords_in_pts(&phi_pruned, h);

  // ----------------------------------------------------------------------
  // PRUNING, START
  {
    // As this structure is NOT used/needed after pruning, we delimit
    // the scope of its definition in '{}', making phi_pruned_prn_data go
    // out of scope (reducing 'namespace pollution') outside this
    // PRUNING block.
    levelset_pruning_data phi_pruned_prn_data;
    if (MARK_NA_DOMAIN_EMPTY
	==
	levelset_mark_pts_on_facets_w_possible_interface
	(&phi_pruned, h, &phi_pruned_prn_data)) {

      *integral_value = 0.0;

      dbgprintf("DETECTED: hyprect ⋂ levelset region = ∅\n");
      dbgprintf("Integral = %f\n", *integral_value);

      levelset_free_pts(&phi_pruned);
      return SUCCESS;
    }

    if (phi_pruned_prn_data.npts2keep == 0 && !is_surf) {
      // Output in integral_value.

      integrand_params * f_params = (integrand_params *)f->params;
      f_params->inqparams = inqparams;

      dbgprintf("DETECTED: hyprect facets (in reduced_dim = %d) "
		"⊂ levelset region\n", reduced_dim);

      hyprect_apply_cubature(h, f, integral_value);

      dbgprintf("Integral = %f\n", *integral_value);

      levelset_free_pts(&phi_pruned);
      return SUCCESS;
    }

    levelset_pack_marked_pts_forget_rest(&phi_pruned, &phi_pruned_prn_data);
  }
  // PRUNING, FINISH
  // ----------------------------------------------------------------------

  // TODO: Allocate only at innermost level, unless if run in parallel.
  if (SUCCESS
      !=
      (ecode = levelset_alloc_grad_workspace(&phi_pruned))) {

    levelset_free_pts(&phi_pruned);
    return ecode;
  }

  const double * const cen_pt_0 = phi_pruned.pts;

  // Output in phi_pruned.grad_value.
  levelset_eval_grad_on_facet(&phi_pruned, cen_pt_0, h->treated_dims);

  dbgprintf("∇φ(center of facet) = ");
  dbgprintf_array(dim, i, "%f ", phi_pruned.grad_value[i]);
  dbgprintf("\n");

  bool all_partials_near_zero = true;
  for (unsigned i = 0; i < dim; ++i) {
    if (fabs(phi_pruned.grad_value[i]) > 1e-10) {
      all_partials_near_zero = false;
      break;
    }
  }

  unsigned dir = argmax_abs(phi_pruned.grad_value, dim);

  ullong partial_sgns_neutral;
  ullong partial_sgns_posneg;
  if (!all_partials_near_zero &&
      levelset_direction_gives_height_function(&phi_pruned, dir, h,
					       is_surf,
					       &partial_sgns_neutral,
					       &partial_sgns_posneg)) {

    dbgprintf("SETTING UP Integrand_%d = ∫ dx_%d\n", dir, dir);

    // We first assign the sgn conds of phi_pruned (which are the same
    // as those of phi) to phi_new.
    levelset phi_new;
    levelset_copy_sgns(&phi_new, &phi_pruned);

    // Then, each bit-pair j is used to determined the sgn conds for
    // new lo and hi facets arising from facet j. These two new sgn
    // conds are stored in two bit-pairs, namely bit-pair j and
    // bit-pair j+hi_facet_offset=j+npts, for the new lo and hi
    // facets, respectively.
    phi_new.npts = phi_pruned.npts;
    levelset_set_new_facets_sgn_conds(&phi_new,
				      is_surf,
				      partial_sgns_neutral,
				      partial_sgns_posneg);

    // TODO: Directly send phi_new_{{,grad}_field} as arguments of
    // levelset_copy{,_with_twice_storage_for_pts} to ensure that they
    // are allocated before being dereferenced inside said functions.
    // Otherwise, there be dragons (random segfaults in optimized
    // code)!
    scalar_field	phi_new_field;
    vector_field	phi_new_grad_field;
    phi_new.field      = &phi_new_field;
    phi_new.grad_field = &phi_new_grad_field;

    // We have already set the new sgn conds in phi_new. So, we must
    // NOT copy the sgn conds from phi_pruned; otherwise, we would
    // overwrite those in phi_new.
    if (SUCCESS
	!=
	(ecode = levelset_copy_no_sgns_w_twice_storage_for_pts(&phi_new,
							       &phi_pruned))) {
      levelset_free_pts(&phi_pruned);
      levelset_free_grad_workspace(&phi_pruned);
      return ecode;
    }

    levelset_duplicate_pts_and_fill_in_new_frzn_coord(&phi_new, dir, h);

    integrand_field	f_new;
    integrand_params	f_new_params;
    f_new.params   = &f_new_params;

    if (!is_surf) {
      f_new.function = integrand_value;

      if (SUCCESS
	  !=
	  (ecode = integrand_params_alloc_partition_pts(&f_new_params,
							phi_pruned.npts + 2))) {
	levelset_free_pts(&phi_pruned);
	levelset_free_grad_workspace(&phi_pruned);
	levelset_free_pts(&phi_new);
	return ecode;
      }

    }
    else {
      f_new.function = surf_integrand_value;
    }

    integrand_params_set_all_but_partition_pts(&f_new_params,
					       f,
					       &phi_pruned,
					       h,
					       dir,
					       inqparams);

    // It is crucial for this call to be after (and NOT before) the
    // previous, as the previous call stores the CURRENT
    // h->treated_dims for the integrand we are forming and this call
    // updates h->treated_dims for the NEXT level.
    hyprect_mark_dim_as_treated(h, dir);

    ecode = inquad(&f_new, &phi_new, h, false, inqparams, integral_value);

    levelset_free_pts(&phi_pruned);
    levelset_free_grad_workspace(&phi_pruned);
    levelset_free_pts(&phi_new);

    if (!is_surf) {
      integrand_params_free_partition_pts(&f_new_params);
    }

    return ecode;
  } // if (levelset_direction_gives_height_function(...))

  else {  // [ if (!levelset_direction_gives_height_function(...)) ]

    // This was allocated to check for a height function direction, a
    // check that failed and that is why we are in 'else'. Hence,
    // before anything, we free it.
    levelset_free_grad_workspace(&phi_pruned);

    unsigned idx_max_halfwidth = argmax(h->halfwidths, dim);

    dbgprintf("\n8<--- HALVING dir %d --->8\n", idx_max_halfwidth);

    // h_left is a new name for the new hyprect we get after
    // successfully halving h. It will contain the left half of the
    // original h. Giving the new h a new name is for clarity only; we
    // could have just as well used h, since the right half is going
    // to be copied to a new storage.
    hyprect * const h_left = h;

    // h_right will contain a DEEP COPY of the right half of h if
    // hyprect_halve succeeds.
    hyprect h_right;
    if (SUCCESS != (ecode = hyprect_halve(h_left, &h_right,
					  idx_max_halfwidth))) {
      return ecode;
    }

    // EVEN IF we only intend to evaluate the integral over the two
    // half hyprects IN SERIAL, since phi gets modified by inquad, we
    // need to make a DEEP COPY of phi for use by the second half
    // before we start evaluating the first half.
    levelset * const phi_pruned_left = &phi_pruned;

    levelset		phi_pruned_right;
    scalar_field	phi_pruned_right_field;
    vector_field	phi_pruned_right_grad_field;
    phi_pruned_right.field      = &phi_pruned_right_field;
    phi_pruned_right.grad_field = &phi_pruned_right_grad_field;

    if (SUCCESS
	!=
	(ecode = levelset_copy(&phi_pruned_right, phi_pruned_left))) {
      return ecode;
    }

    dbgprintf("\nENTERING LEFT/LOWER half hyprect\n");

    double integral_value_h_left;
    if (SUCCESS
	!=
	(ecode = inquad(f,
			phi_pruned_left, h_left,
			is_surf, inqparams, &integral_value_h_left))) {
      return ecode;
    }

    dbgprintf("\nENTERING RIGHT/UPPER half hyprect\n");

    double integral_value_h_right;
    if (SUCCESS
	!=
	(ecode = inquad(f,
			&phi_pruned_right, &h_right,
			is_surf, inqparams, &integral_value_h_right))) {
      return ecode;
    }

    *integral_value =
      integral_value_h_left +
      integral_value_h_right;

    dbgprintf("\n+++ GLUING back the two half hyprects +++\n");
    dbgprintf("Sum of Integrals in these two halves = %f\n", *integral_value);

    // The left-half hyprect, h_left, is h, which was made by the user
    // and has to be freed by the user too. On the other hand,
    // phi_pruned is a DEEP COPY we made of the user's phi, so we have
    // to free it.
    levelset_free_pts(phi_pruned_left);

    hyprect_free(&h_right);
    levelset_free_pts(&phi_pruned_right);

    return SUCCESS;
  } // else [ if (!levelset_direction_gives_height_function(...)) ]

}
