// inquad_levelset_height_function.cc ---

// Copyright (C) 2016 Omid Khanmohamadi

// Author: Omid Khanmohamadi

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "inquad_levelset_height_function.h"
#include "norm2.h"		// for norm2
#include "basic_algebra.h"	// for square_d

#include <math.h>		// for fabs, sqrt
#include <stdlib.h>		// for malloc, free, size_t
#include <string.h>		// for memcpy

// This macro is to be used by the functions in this translation unit
// ONLY. It evaluates the raw ∇φ(x) vector for a given point a calling
// the corresponding user-supplied functions), which need to be
// processed before they are ready to use in the current, facet (aka,
// dimension-reduced hyprect). For this reason, they are NOT put in
// the header file.
#define LEVELSET_EVAL_GRAD(phi, x)				\
  (*((phi)->grad_field->function))( (phi)->dim,			\
				    x,				\
				    (phi)->grad_field->params,	\
				    (phi)->grad_value )


// If we were not to parallelize our implementation of inquad later,
// this temporary storage could be allocated once for the initial
// hyprect and then used by all hyprects (resulting from possibly
// halving the initial hyprect), knowing that only one hyprect at a
// time would use this temporary storage. To parallelize the
// implementation over different hyprects, this function has to be
// called when we halve a hyprect to allocate new storage for one half
// (the other half keeps using the storage already available). N.B.
// Our implementation of integration using recursion is inherently
// sequential. As a result, even in a parallel run (over different
// sub-hyprects) all different recursion depths for the same hyprect
// can safely use the same temporary storage.
errcode levelset_alloc_grad_workspace(levelset * phi)
{
  unsigned dim = phi->dim;

  phi->grad_pt = (double *) malloc((dim + dim) * sizeof(double));

  if (NULL == phi->grad_pt) {
    return ERR_MALLOC;
  }

  phi->grad_value = phi->grad_pt + dim;

  return SUCCESS;
}


// Output in phi->grad_value.
//
// Before this function is called, storage must be allocated for
// phi->grad_value, perhaps by calling levelset_alloc_grad_workspace.
void
levelset_eval_grad_on_facet(levelset * const		phi,
			    const double * const	x,	// pt on facet
			    ulong			treated_dims)
{
  // Output in phi->grad_value.
  LEVELSET_EVAL_GRAD(phi, x);

  unsigned		dim	   = phi->dim;
  double * const	grad_phi_x = phi->grad_value;

  for (unsigned i = 0; i < dim; ++i) {

    // The center point x corresponds to a facet (identified by one
    // of the 2^(dim-reduced_dim) possible combinations of
    // x[i]=center[i]±halfwidths[i] for i set in treated_dims). On
    // this facet, the coordinates x[i] do not change and, as a
    // result, D_i φ(x) will be zero.
    if (IS_BIT_SET(treated_dims, i))
      grad_phi_x[i] = 0.0;
  }
}

// Obviously, with its implementation simply indexing the result of a
// call to gradient, this function must not be used for calculating
// several partials (i.e., different k's) of the same function (phi)
// at the same point (x), as the same gradient would be unnecessarily
// recalculated multiple times in that case. This function is here
// (and is used in levelset_direction_gives_height_function) in case
// in the future we decide to define a new function for calculating
// the partial using the definition of the derivative as a linear
// form, Dφ(x)(e_k) =: D_dir φ(x).
STATIC_UNLESS_TESTING double
levelset_partial_on_facet_from_grad(levelset * const		phi,
				    const double * const	x,
				    unsigned			k,
				    ulong			treated_dims)
{
  // Output in phi->grad_value.
  levelset_eval_grad_on_facet(phi, x, treated_dims);

  return phi->grad_value[k];
}


bool
levelset_direction_gives_height_function(levelset * const	phi,
					 unsigned		k, // dir
					 const hyprect * const  h,
					 bool is_surf,
					 ullong * const partial_sgns_neutral,
					 ullong * const partial_sgns_posneg)

{
  unsigned		dim  = phi->dim;
  unsigned		npts = phi->npts;
  const double *	xc_j = phi->pts;
  double * const	xv_j = phi->grad_pt;

  // Initializing these variables is good form, but not necessary, as,
  // given a bit pair, store_sgn_in_bit_pair CLEARS the bit that it
  // DOES NOT change.
  *partial_sgns_neutral = zero_ULL;
  *partial_sgns_posneg  = zero_ULL;

  for (unsigned j = 0; j < npts; ++j) {
    // The Implicit Function Theorem requires that |D_k φ(xc_j)| be
    // bounded away from zero (i.e., D_k φ(x_j) not change sign) for
    // the equation φ(x)=0 (zero level set) to implicitly express, in
    // a neighborhood of xc_j, the coordinate 'k' of x as a function
    // of the rest of the coordinates.

    // Evaluate D_k φ at xc_j (which identifies the facet of h we are on).
    double D_k_phi_xc_j =
      levelset_partial_on_facet_from_grad(phi, xc_j, k, h->treated_dims);

    // Traverse the vertices of facet xc_j. Each call below to
    // hyprect_facet_set_next_vertex sets xv_j to a new vertex.
    while (ITER_STOP != hyprect_facet_set_next_vertex(h, xc_j, xv_j)) {

      // Evaluate D_k φ at vertex xv_j of the facet xc_j.
      double D_k_phi_xv_j =
	levelset_partial_on_facet_from_grad(phi, xv_j, k, h->treated_dims);

      // If the partial at a VERTEX is "too close" to zero, do not use
      // it for sign change detection. Move on to the next vertex.
      if (fabs(D_k_phi_xv_j) < 1e-10) {
	continue;
      }

      // If the partial at the CENTER is "too close" to zero, replace
      // it with the partial at the current vertex, which is not "too
      // close" to zero, since it has passed the if condition above.
      // Since once the partial at the center is replaced with a
      // not-"too-close"-to-zero value it is going to pass this if
      // condition, it follows that this replacement is done AT MOST
      // ONCE.
      if (fabs(D_k_phi_xc_j) < 1e-10) {
	D_k_phi_xc_j = D_k_phi_xv_j;
	continue;
      }

      // Since the signs of all vertices are being compared to that of
      // the center, it is crucial that we treat zero as signed
      // (either positive, as we do here, or negative; it doesn't
      // matter) so that if the center turns out to be zero, going
      // from a positive vertex to a negative one is detected here. If
      // zero is treated as signless (neutral), then neither (0, -1)
      // nor (0, 1) will pass the 'if condition' below.
      if (!have_same_sign_zero_pos(D_k_phi_xc_j,
				   D_k_phi_xv_j)) {
	return false;
      }
    }

    store_sgn_in_bit_pair(signum(D_k_phi_xc_j),
			  j,
			  partial_sgns_neutral,
			  partial_sgns_posneg);

    xc_j += dim;	 // go to next cen pt/facet (dim entries away)
  }

  return true;
}


void levelset_free_grad_workspace(levelset * const phi)
{
  free(phi->grad_pt);
}


// Output in phi->pts.
//
// N.B.: The sgn cond's for new facets have already been set by
// levelset_direction_gives_height_function.
STATIC_UNLESS_TESTING void
levelset_duplicate_pts(levelset * const phi)
{
  unsigned		dim  = phi->dim;
  unsigned		npts = phi->npts;
  double * const	pt_0 = phi->pts;

  unsigned pt_n_offset = dim*npts;

  // Duplicate all npts in one shot. This may allow for vectorization
  // and use of Streaming SIMD Extensions (SSE), since exactly the
  // same instruction (copy) is to be performed on multiple data
  // objects.
  memcpy(pt_0 + pt_n_offset, pt_0, pt_n_offset * sizeof(double));
}


// Output in phi->pts.
STATIC_UNLESS_TESTING void
levelset_fill_new_frzn_coord_in_pts(levelset * const		phi,
				    unsigned			frzn_coord,
				    const hyprect * const	h)
{
  unsigned	 dim  = phi->dim;
  unsigned	 npts = phi->npts;
  double	*pt_j = phi->pts;

  double frzn_lo_val = h->center[frzn_coord] - h->halfwidths[frzn_coord];
  double frzn_hi_val = h->center[frzn_coord] + h->halfwidths[frzn_coord];

  for (unsigned j = 0; j < npts; ++j) {
    unsigned pt_n_offset = dim*npts;

    // Freeze coordinate frzn_coord for the first half of points
    // (i.e., the original points, {pt_j, 0≤j<npts}), at frzn_lo_val.
    // Freeze coordinate frzn_coord for the second half of points
    // (i.e., {pt_j, npts≤j<2npts} made by the memcpy in
    // levelset_duplicate_pts), at frzn_hi_val. It is not important
    // for our algorithm which points use frzn_lo_val and which use
    // frzn_hi_val, as long as half use one and half the other.
    pt_j[frzn_coord]		   = frzn_lo_val;
    pt_j[frzn_coord + pt_n_offset] = frzn_hi_val; // pt_{j+npts}[frzn_coord]

    pt_j += dim;
  }

  phi->npts *= 2;
}


void levelset_duplicate_pts_and_fill_in_new_frzn_coord(levelset * const phi,
						       unsigned frzn_coord,
						       const hyprect * const h)

{
  levelset_duplicate_pts(phi);
  levelset_fill_new_frzn_coord_in_pts(phi, frzn_coord, h);
}


STATIC_UNLESS_TESTING int
levelset_sgn_cond_new_lo_facet(const levelset * const	phi,
			       unsigned			j,
			       bool			is_surf,
			       ullong			partial_sgns_neutral,
			       ullong			partial_sgns_posneg)

{
  int	sgn_cond_facet_j    = bit_pair_to_sgn(phi->sgns_neutral,
					      phi->sgns_posneg,
					      j);

  int	sgn_partial_facet_j = bit_pair_to_sgn(partial_sgns_neutral,
					      partial_sgns_posneg,
					      j);

  return ((sgn_partial_facet_j == -sgn_cond_facet_j) || is_surf)?
    sgn_cond_facet_j : 0;
}


STATIC_UNLESS_TESTING int
levelset_sgn_cond_new_hi_facet(const levelset * const	phi,
			       unsigned			j,
			       bool			is_surf,
			       ullong			partial_sgns_neutral,
			       ullong			partial_sgns_posneg)

{
  int	sgn_cond_facet_j    = bit_pair_to_sgn(phi->sgns_neutral,
					      phi->sgns_posneg,
					      j);

  int	sgn_partial_facet_j = bit_pair_to_sgn(partial_sgns_neutral,
					      partial_sgns_posneg,
					      j);

  return ((sgn_partial_facet_j == sgn_cond_facet_j) || is_surf)?
    sgn_cond_facet_j : 0;
}


void
levelset_set_new_facets_sgn_conds(levelset * const phi,
				  bool   is_surf,
				  ullong partial_sgns_neutral,
				  ullong partial_sgns_posneg)

{
  unsigned	npts		= phi->npts;
  unsigned	hi_facet_offset = npts;

  for (unsigned j = 0; j < npts; ++j) {

    int sgn_cond_new_lo_facet =
      levelset_sgn_cond_new_lo_facet(phi,
				     j,
				     is_surf,
				     partial_sgns_neutral,
				     partial_sgns_posneg);

    int sgn_cond_new_hi_facet =
      levelset_sgn_cond_new_hi_facet(phi,
				     j, // this is j, not j + hi_facet_offset
				     is_surf,
				     partial_sgns_neutral,
				     partial_sgns_posneg);

    // The new low facet information is stored at index j, overwriting
    // old facet information there.
    store_sgn_in_bit_pair(sgn_cond_new_lo_facet,
			  j,
			  &phi->sgns_neutral,
			  &phi->sgns_posneg);

    // The new high facet information is stored at index
    // j+hi_facet_offset, which does not contain any old facet
    // information.
    store_sgn_in_bit_pair(sgn_cond_new_hi_facet,
			  j + hi_facet_offset,
			  &phi->sgns_neutral,
			  &phi->sgns_posneg);
  }
}
