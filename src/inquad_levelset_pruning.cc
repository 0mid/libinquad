// inquad_levelset_pruning.cc ---

// Copyright (C) 2016 Omid Khanmohamadi

// Author: Omid Khanmohamadi

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "inquad_levelset_pruning.h"

#include <stdlib.h>  // for malloc, free
#include <string.h>  // for memcpy
#include <stdbool.h> // for bool in C (>= C99); fund. type in C++; need no header
#include <math.h>    // for fabs

// This function (i.e., the algorithm employed) is applicable only to
// a signed (aka oriented) distance function and not to a level set
// function that does not possess this property.
//
// The value of the signed distance function phi is assumed to be
// negative inside and positive outside the zero level set.
STATIC_UNLESS_TESTING interface_situation
levelset_sgnd_distfun_interface_situation_on_facet(const levelset * const phi,
						   unsigned		  j,
						   const hyprect * const  h,
						   double facet_radius)
{

  unsigned		dim	     = h->dim;
  const double * const	xc	     = phi->pts + j*dim;

  // This is the square root of the dimension (reduced_dim) of the
  // facet identified by xc.
  double	sgnd_dist_xc_to_bndry = LEVELSET_FN_VAL(phi, xc);
  double	     dist_xc_to_bndry = fabs(sgnd_dist_xc_to_bndry);

  // Everywhere, a center point xc (which is given as an input here)
  // identifies a facet U_xc. For this reason sometimes we use xc
  // and U_xc interchangeably. The halfwidths[i] of relevance to a
  // facet xc are those corresponding to the dimensions i of the
  // hyprect h that haven not been treated (reduced) yet.

  // Let ∂Ω denote the zero level set of φ, {x ∈ X | φ(x)=0}. If the
  // distance |φ(xc)| of the center xc to ∂Ω is > facet_radius, then
  // ∂Ω does NOT have an interface (i.e., φ does NOT change sign) in
  // this facet. The contrapositive of the above 'conditional
  // statement' (which is equivalent to it logically) is: If ∂Ω DOES
  // have an interface (i.e., φ does change sign) in facet xc, then
  // |φ(xc)| will be ≤ facet_radius. This gives a necessary (but not
  // sufficient) condition for having an interface (a sign change),
  // which we check below to see if the facet xc is to be kept
  // (i.e., NOT to be pruned). This is a conservative test (we
  // cannot do much better anyway, drawing a global-to-a-facet
  // conclusion given only local (center) information): It may lead
  // to keeping a facet that actually does not have an interface;
  // but it will never lead to pruning a facet that does have an
  // interface.
  if (dist_xc_to_bndry <= facet_radius) {
    return MAY_HAVE_INTERFACE;
  }

  // Otherwise, the facet U_xc has no interface. This could be due to
  // either one of two reasons:
  //
  // 1. The facet U_xc is fully 'inside' (outside) the zero level set
  // ∂Ω, and the user wants to integrate inside (outside) the level
  // set too (i.e., we are in agreement with a sign condition). In
  // this case, we have U_xc ⋂ Ω = U_xc. This facet will be pruned out
  // (forgotten) later.
  if (levelset_sgn_cond_satisfied_on_facet(phi, j,
					   sgnd_dist_xc_to_bndry)) {
    return NO_INTERFACE_BUT_SGN_COND_PASSED;
  }

  // 2. The facet U_xc is fully 'inside' (outside) the zero level set
  // ∂Ω, but the user wants to integrate outside (inside) the level
  // set (i.e., we are in DISAGREEMENT with a sign condition). In this
  // case, we have U_xc ⋂ Ω = ∅. As the domain of integration is
  // empty, the whole integral is zero.
  else {
    return NO_INTERFACE_AND_SGN_COND_FAILED;
  }
}


// Output in phi_prn_data->{pts2keep, npts2keep}.
mark_retcode
levelset_mark_pts_on_facets_w_possible_interface(const levelset * const
						 phi,

						 const hyprect * const
						 h,

						 levelset_pruning_data * const
						 phi_prn_data)
{

  // A ullong is guaranteed to be at least 64. This allows 64 = 2^6
  // combinations (of center[i] ± halfwidths[i], 0≤i<6), which happens
  // in the worst case scenario for a level set in R^6 (dim == 6). In
  // other words, this choice allows for 6 dimensions. However, this
  // is not a practical limitation. Even for simple integrands on
  // simple regions (e.g., a region which is a subset of the initial
  // hyprect), the curse of dimensionality kicks in at around
  // dimension 5 or 6, and we are forced to switch to other methods
  // (e.g., non-deterministic methods such as Monte Carlo).
  phi_prn_data->pts2keep  = zero_ULL; // initialize to 00...0 (keep no pts)
  phi_prn_data->npts2keep = 0;

  unsigned npts = phi->npts;

  // The following loop is over 'parallel' facets of the same hyprect
  // (i.e., facets that correspond to the same treated_dims). As a
  // result, they all have the same radius. That is why we take the
  // pain of using an extra facet_radius argument for
  // levelset_sgnd_distfun_interface_situation_on_facet, so that we
  // can calculate that radius here once and use it there with all the
  // 'parallel' facets received from here.
  double facet_radius = hyprect_reduced_radius(h);

  for (unsigned j = 0; j < npts; ++j) {
    // TODO: Look into vectorizing this evaluation.
    interface_situation facet_j_interface_situation =
      levelset_sgnd_distfun_interface_situation_on_facet(phi, j, h, facet_radius);


    switch (facet_j_interface_situation) {
    case MAY_HAVE_INTERFACE:
      SET_BIT(phi_prn_data->pts2keep, j);
      phi_prn_data->npts2keep += 1;
      break;

    case NO_INTERFACE_BUT_SGN_COND_PASSED:
      // This facet (equivalently center point) will be pruned out
      // later as it is not marked to keep.
      break;

    case NO_INTERFACE_AND_SGN_COND_FAILED:
      return MARK_NA_DOMAIN_EMPTY;
      break;			// for consistency
    }
  }

  return MARK_SUCCESS;
}


// Output in phi->pts (updated in place) and phi->npts.
void
levelset_pack_marked_pts_forget_rest(levelset * const
				     phi,

				     const levelset_pruning_data * const
				     phi_prn_data)
{

  unsigned	 dim  = phi->dim;
  unsigned	 npts = phi->npts;
  double	*pts  = phi->pts;

  // Keep and compare offsets instead of modifying and comparing ptrs,
  // in order not to have to deal with the details of ptr comparison
  // (defined for == and !=, but undefined otherwise; see C99
  // Standard, 6.5.9 on the equality operator and 6.5.8 on relational
  // operators).
  unsigned	pt_src_offset  = 0;
  unsigned	pt_dest_offset = 0;

  for (unsigned j = 0; j < npts; ++j) {

    // If current src pt is to be kept,
    if (IS_BIT_SET(phi_prn_data->pts2keep, j)) {

      // if src & dest ptrs are at same pt, then the data is already
      // at the right location; no move (via copy) is necessary. Just
      // increment dest ptr by dim so that it is at the next pt, so
      // that this pt is not overwritten upon a later copy.
      if (pt_src_offset == pt_dest_offset) {
	pt_dest_offset += dim;
      }
      // otherwise, we need to move back the pt at pt_src_offset to
      // pt_dest_offset. We do this by copying it over (i.e.,
      // overwriting) the pt at pt_dest_offset, which has been scanned
      // (in a previous iteration, since pt_src_offset >=
      // pt_dest_offset) and determined NOT to be a pt to keep.
      else {
	memcpy(pts + pt_dest_offset,
	       pts + pt_src_offset,
	       dim * sizeof(double));

      }
    }

    // Since it is unconditionally incremented (by dim), pt_src_offset
    // is always greater than or equal to pt_dest_offset, which is
    // conditionally incremented above.
    pt_src_offset += dim;
  }

  phi->npts = phi_prn_data->npts2keep;
}
