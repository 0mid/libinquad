CUBATURE_DIR = $(OPT)/cubature-1.0.2
GSL_DIR = $(OPT)/gsl-2.1

# This is a convenience library, used only when building the package.
# We link against this library to build our main and test executable
# files.
#
# https://stackoverflow.com/a/2305737
noinst_LTLIBRARIES = libinquad.la


# The sources of each target go into _SOURCES, with non-alphanumeric
# characters in the name of the libraries mapped to '_' (e.g., .la
# becomes _la).
#
# Automake automatically computes the list of objects to build and
# link from these files. Header files are not compiled. We list them
# only so they get distributed (Automake does not distribute files it
# does not know about), and, of course, to make sure their target is
# updated if they change. Unless otherwise specified, compiler and
# linker are inferred from the extensions.
#
# Usually, only header files that accompany installed libraries need
# to be installed. Headers used by programs or convenience libraries
# are not installed. The noinst_HEADERS variable can be used for such
# headers. However when the header actually belongs to a single
# convenience library or program, we recommend listing it in the
# program's or library's _SOURCES variable instead of in
# noinst_HEADERS. This is clearer for the Makefile.am reader.
# noinst_HEADERS would be the right variable to use in a directory
# containing only headers and no associated library or program.
# [https://www.gnu.org/software/automake/manual/html_node/Headers.html]

libinquad_la_SOURCES =			\
 array_funcro.h				\
 array_util.h				\
 return_code.h				\
 argmax.cc				\
 argmax.h				\
 norm2.h				\
 norm2.cc				\
 array_alloc.h				\
 array_alloc.cc				\
 inquad.h				\
 inquad_geometry.h			\
 inquad_geometry.cc			\
 sort_util.h				\
 sort_util.cc				\
 inquad_field.h				\
 inquad_levelset.h			\
 inquad_levelset.cc			\
 inquad_levelset_pruning.h		\
 inquad_levelset_pruning.cc		\
 inquad_levelset_height_function.h	\
 inquad_levelset_height_function.cc	\
 inquad_cubature.h			\
 inquad_cubature.cc			\
 bit_util.h				\
 bit_util.cc				\
 basic_algebra.h			\
 basic_algebra.cc			\
 cond_compile.h				\
 inquad_integrand.h			\
 inquad_integrand.cc			\
 inquad_integrand_params.h		\
 inquad_levelset_root.h			\
 inquad_levelset_root.cc		\
 inquad_params.h			\
 inquad.h				\
 inquad.cc

libinquad_la_CPPFLAGS = \
 -I$(CUBATURE_DIR)	\
 -I$(GSL_DIR)/include


# Non-default flags should NOT be added to Autoconf files; doing so
# violates the principal of least surprise, as anyone familiar with
# Autoconf expects the default flags to be '-g -O2' for CXXFLAGS and
# you should not change that. If a user wants anything but the
# default, they can configure/make the package with their non-default
# options, e.g., with
#
# ./configure CXXFLAGS='-g -ggdb -gdwarf-3 -O0'
#
# to get a version suitable for debugging with DWARF-3 compatible GDB's, or
#
# make check CXXFLAGS='-O3'
#
# to get a version with extra optimizations.
# [https://stackoverflow.com/a/3147058, https://stackoverflow.com/a/4680578].
libinquad_la_CXXFLAGS = -Wall -Wextra -std=c++11

libinquad_la_LIBADD = \
 $(CUBATURE_DIR)/lib/libhcubature.a
