// inquad_params.h ---

// Copyright (C) 2017 Omid Khanmohamadi

// Author: Omid Khanmohamadi

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef INQUAD_PARAMS_H_INCLUDED
#define INQUAD_PARAMS_H_INCLUDED

// https://en.wikipedia.org/wiki/Comparison_of_file_systems#Limits
#define MAX_FILENAME_LEN 255

typedef struct inquad_params_s {
  double   root_abserr;
  double   root_relerr;
  double   root_interval_shrink_factor;
  unsigned root_nsubintervals;
  unsigned root_maxniters;

  unsigned	 quad_npts;
  const char	*quad_outfname;
} inquad_params;

const inquad_params inquad_default_params = {0.0, 1e-6, 0.96, 50, 100,
					     8, ""};

#endif // INQUAD_PARAMS_H_INCLUDED
