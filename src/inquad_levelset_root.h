// inquad_levelset_root.h ---

// Copyright (C) 2017 Omid Khanmohamadi

// Author: Omid Khanmohamadi

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef INQUAD_LEVELSET_ROOT_H_INCLUDED
#define INQUAD_LEVELSET_ROOT_H_INCLUDED

#include "inquad_levelset.h"	// for levelset
#include "inquad_geometry.h"	// for hyprect
#include "cond_compile.h"	// for TESTING
#include "inquad_params.h"	// for inquad_params

typedef enum root_situation_e {
  AT_MOST_ONE_ROOT_PER_FACET = 1,
  MAY_HAVE_SEVERAL_ROOTS_PER_FACET
} root_situation;

int
levelset_find_roots_in_interval_along_dir_on_facets(levelset * const
						    phi,

						    double
						    interval_left_end,

						    double
						    interval_right_end,

						    unsigned
						    dir,

						    root_situation
						    rt_situation,

						    const inquad_params * const
						    inqparams,

						    double * const
						    roots);

#if TESTING

#include "gsl/gsl_math.h"	// for gsl_function
#include "gsl/gsl_roots.h"	// for gsl_root_{...}

bool
gsl_function_has_same_sign_at_points(gsl_function	*F,
				     double		 x1,
				     double		 x2);


bool
gsl_function_sgn_chng_detected_in_interval(gsl_function *phi_1d,
					   double *root_interval_lend,
					   double *root_interval_rend,
					   double interval_step_size);

errcode
gsl_function_find_root_in_interval_w_sgn_chng(gsl_function *phi_1d,

					      gsl_root_fsolver
					      **brent_solver,

					      double
					      root_interval_lend,

					      double
					      root_interval_rend,

					      const inquad_params * const
					      inqparams,

					      double
					      *root);

#endif

#endif // INQUAD_LEVELSET_ROOT_H_INCLUDED
