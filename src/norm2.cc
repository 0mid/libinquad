// norm2.cc ---

// Copyright (C) 2016 Omid Khanmohamadi

// Author: Omid Khanmohamadi

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <math.h>		// for sqrt

double norm2(const double * const v, unsigned dim)
// This function does not protect against undue overflow during the
// intermediate steps of calculation. One way to do so, at the expense
// of extra operations, is to find the norm of v./max(|v|) and then
// multiply by max(|v|).
{
  double accum = 0;
  for (unsigned i = 0; i < dim; ++i){
    accum += v[i]*v[i];
  }

  return sqrt(accum);
}
