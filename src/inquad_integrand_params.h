// inquad_integrand_params.h ---

// Copyright (C) 2017 Omid Khanmohamadi

// Author: Omid Khanmohamadi

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef INQUAD_INTEGRAND_PARAMS_H_INCLUDED
#define INQUAD_INTEGRAND_PARAMS_H_INCLUDED

#include "inquad_field.h"	// for integrand_field
#include "inquad_levelset.h"	// for levelset
#include "inquad_geometry.h"	// for hyprect
#include "inquad_params.h"	// for inquad_params

// Note that this data structure is RECURSIVELY defined in an implicit
// manner. At each recursion level during the INNER-to-outer integral
// setup (∫<–∫), the 'params' member of the new (outer) integrand
// (e.g., f_new.params) is set to the relevant information at that
// recursion level, which information includes the current integrand
// (e.g., f) Then, at each recursion level during the OUTER-to-inner
// integral evaluation (∫–>∫), we can access the members of the inner
// integrand 'params' (i.e., those of the upcoming recursion level) by
// following (i.e., dereferencing with proper intermediate casts) the
// integrand 'params' of that recursion level to get to its 'field'
// and following the latter to get to the inner integrand 'params'.
// That is,
//
// Current integrand params.
// integrand_params * const prms       = (integrand_params *) params;
//
// Inner integrand params.
// integrand_params * const prms_inner = (integrand_params *) prms->field->params;
typedef struct integrand_params_s {
  integrand_field	*field;
  levelset		*lvlst;
  hyprect		*hrect;
  double		*partition_pts;
  const inquad_params	*inqparams;
  ulong			 treated_dims;
  unsigned		 dir;
  unsigned		 dim;
} integrand_params;

#endif // INQUAD_INTEGRAND_PARAMS_H_INCLUDED
