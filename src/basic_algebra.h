// basic_algebra.h ---

// Copyright (C) 2016 Omid Khanmohamadi

// Author: Omid Khanmohamadi

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef BASIC_ALGEBRA_H_INCLUDED
#define BASIC_ALGEBRA_H_INCLUDED

#include <stdbool.h> // for bool in C (>= C99); fund. type in C++; need no header

double square_d(double x);

bool have_same_sign_zero_pos(double x, double y);

bool have_same_sign_zero_signless(double x, double y);

int signum(double x);

#endif // BASIC_ALGEBRA_H_INCLUDED
