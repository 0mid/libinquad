typedef double (*gsl_function_func) (double y, void *params);

// Only used by gsl_function_func and hence put in the implementation file
// rather than the header (unless testing).
typedef struct gsl_function_params_s {
  integrand_field *field;
  double *eval_pt; // cannot be const double *; need to modify element(s)
  unsigned dir;
  unsigned dim;
  unsigned reduced_dim;	      // checked against dim only if WRITEQPTS
  const char *quad_outfname;
} gsl_function_params;
