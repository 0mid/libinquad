// alloc_array.h ---

// Copyright (C) 2017 Omid Khanmohamadi

// Author: Omid Khanmohamadi

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef ALLOC_ARRAY_H_INCLUDED
#define ALLOC_ARRAY_H_INCLUDED

#include "return_code.h"	// for errcode

errcode alloc_darray(unsigned len, double **darray);

void free_darray(double *darray);

#endif // ALLOC_ARRAY_H_INCLUDED
