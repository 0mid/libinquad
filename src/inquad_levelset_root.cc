// inquad_levelset_root.cc ---

// Copyright (C) 2017 Omid Khanmohamadi

// Author: Omid Khanmohamadi

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "inquad_levelset_root.h"
#include "inquad_levelset.h"
#include "sort_util.h"		// for darray_qsort

#include "gsl/gsl_errno.h"	// for GSL_CONTINUE
#include "gsl/gsl_roots.h"	// for gsl_root_{...}
#include "gsl/gsl_math.h"	// for gsl_function, GSL_FN_EVAL

#include <stdbool.h> // for bool in C (>= C99); fund. type in C++; need no header

// Only used by levelset_1d and hence put in the implementation file
// rather than the header.
typedef struct levelset_1d_params_s {
  levelset	*lvlst;
  double	*eval_pt;
  unsigned	 dir;
} levelset_1d_params;

// This defines the function x ↦ phi({x[0], ..., y, x[dim]}) =: phi_1d(y).
STATIC_UNLESS_TESTING double
levelset_1d(double y, void *params)
{
  levelset_1d_params *prms = (levelset_1d_params *)params;

  prms->eval_pt[prms->dir] = y;

  return LEVELSET_FN_VAL(prms->lvlst, prms->eval_pt);
}


STATIC_UNLESS_TESTING bool
gsl_function_has_same_sign_at_points(gsl_function	*phi_1d,
				     double		 x1,
				     double		 x2)

{
  double phi_1d_x1 = GSL_FN_EVAL(phi_1d, x1);
  double phi_1d_x2 = GSL_FN_EVAL(phi_1d, x2);

  if ((phi_1d_x1 < 0.0 && phi_1d_x2 < 0.0)
      ||
      (phi_1d_x1 > 0.0 && phi_1d_x2 > 0.0)) {

    return true;
  }

  return false;
}


STATIC_UNLESS_TESTING bool
gsl_function_sgn_chng_detected_in_interval(gsl_function *phi_1d,
					   double *root_interval_lend,
					   double *root_interval_rend,
					   double interval_step_size)

{
  while (*root_interval_lend < *root_interval_rend
	 &&
	 gsl_function_has_same_sign_at_points(phi_1d,
					      *root_interval_lend,
					      *root_interval_rend)) {
    *root_interval_lend += interval_step_size;
    *root_interval_rend -= interval_step_size;
  }

  if (*root_interval_lend > *root_interval_rend) {
    return false;
  }

  return true;
}


STATIC_UNLESS_TESTING errcode
gsl_function_find_root_in_interval_w_sgn_chng(gsl_function *phi_1d,

					      gsl_root_fsolver
					      **brent_solver,

					      double
					      root_interval_lend,

					      double
					      root_interval_rend,

					      const inquad_params * const
					      inqparams,

					      double
					      *root)
{
  const unsigned	maxniters = inqparams->root_maxniters;
  const double		abserr	  = inqparams->root_abserr;
  const double		relerr	  = inqparams->root_relerr;

  // Copy these input parameters, as we will be modifying their values
  // below. Also, this allows to use the same names as the GSL
  // functions' prefixes below.
  double x_lower = root_interval_lend;
  double x_upper = root_interval_rend;

  gsl_root_fsolver_set(*brent_solver,
		       phi_1d,
		       x_lower,
		       x_upper);

  int status;
  unsigned niters = 0;
  do {
    gsl_root_fsolver_iterate(*brent_solver);
    *root = gsl_root_fsolver_root(*brent_solver);

    x_lower = gsl_root_fsolver_x_lower(*brent_solver);
    x_upper = gsl_root_fsolver_x_upper(*brent_solver);

    status = gsl_root_test_interval(x_lower,
				    x_upper,
				    abserr, relerr);

  } while (status == GSL_CONTINUE && ++niters < maxniters);

  if (niters < maxniters) {
    return SUCCESS;
  }

  return ERR_MAX_NITER;
}


// The root finding, which is along 'dir' coordinate of eval_pt, is
// necessary to find the interface of the levelset in the facet pairs
// of the hyprect at the current recursion depth.
int
levelset_find_roots_in_interval_along_dir_on_facets(levelset * const
						    phi,

						    double
						    interval_lend,

						    double
						    interval_rend,

						    unsigned
						    dir,

						    root_situation
						    rt_situation,

						    const inquad_params * const
						    inqparams,

						    double * const
						    roots)

{
  const double	interval_shrink_factor = inqparams->root_interval_shrink_factor;
  const unsigned nsubintervals = inqparams->root_nsubintervals;
  const double relerr = inqparams->root_relerr;

  double interval_length = interval_rend - interval_lend;

  double interval_step_size  =
    0.5 * (1 - interval_shrink_factor) * interval_length;

  double subinterval_length = interval_length/nsubintervals;

  unsigned	 dim	  = phi->dim;
  double	*phi_pt_j = phi->pts;
  unsigned	 npts	  = phi->npts;

  levelset_1d_params phi_1d_prms;
  phi_1d_prms.lvlst   = phi;
  phi_1d_prms.dir     = dir;

  gsl_function phi_1d;
  phi_1d.function = levelset_1d;
  phi_1d.params   = &phi_1d_prms;

  static const gsl_root_fsolver_type *brent_solver_t =
    gsl_root_fsolver_brent;

  // This cannot be static or const.
  gsl_root_fsolver *brent_solver =
    gsl_root_fsolver_alloc(brent_solver_t);

  // Loop over different facets to find roots in each.
  //
  // Loop invariant: we have found nroots so far.
  unsigned nroots = 0;
  for (unsigned j = 0; j < npts; ++j) {

    phi_1d_prms.eval_pt = phi_pt_j;

    double	root_interval_lend = interval_lend;
    double	root_interval_rend = interval_rend;
    if (rt_situation == AT_MOST_ONE_ROOT_PER_FACET) {

      if (gsl_function_sgn_chng_detected_in_interval(&phi_1d,
						       &root_interval_lend,
						       &root_interval_rend,
						       interval_step_size)) {
	double root_facet_j;
	if (SUCCESS
	    ==
	    gsl_function_find_root_in_interval_w_sgn_chng(&phi_1d,
							  &brent_solver,
							  root_interval_lend,
							  root_interval_rend,
							  inqparams,
							  &root_facet_j)) {
	  roots[nroots++] = root_facet_j;
	}
      }
    }
    else if (rt_situation == MAY_HAVE_SEVERAL_ROOTS_PER_FACET) {
      // Assumption: There is at most ONE root per subinterval per
      // facet. In other words, we have at most 'nsubintervals' roots
      // per facet.
      for (unsigned i = 0; i < nsubintervals; ++i) {

	// If root_interval_lend happens to fall on (or "near") the
	// root just found in the previous subinterval
	// [root_interval_lend, root_interval_rend] (i.e., if the
	// previous root, roots[nroots-1], is at or "near" the rend of
	// the previous subinterval), we must skip the new
	// subinterval. Otherwise, we would find and write the same
	// root twice, causing an "invalid write" in the raw array
	// partition_pts.
	if ( nroots > 0 &&
	     fabs(root_interval_lend-roots[nroots-1])
	     <
	     relerr*fabs(roots[nroots-1]) ) {
	  continue;
	}

	root_interval_rend = root_interval_lend + subinterval_length;

	// These copies are needed, since
	// gsl_function_sgn_chng_detected_in_interval modifies its 2nd
	// and 3rd args (which are the lend and rend pts of the
	// subinterval being swept). We need to be able to use the
	// lend and rend values in case no sgn chng is detected in the
	// current subinterval and we must move on to the next.
	double rt_intrvl_lend = root_interval_lend;
	double rt_intrvl_rend = root_interval_rend;
	if (gsl_function_sgn_chng_detected_in_interval(&phi_1d,
						       &rt_intrvl_lend,
						       &rt_intrvl_rend,
						       interval_step_size)) {
	  double root_i_facet_j;
	  if (SUCCESS
	      ==
	      gsl_function_find_root_in_interval_w_sgn_chng(&phi_1d,
							    &brent_solver,
							    rt_intrvl_lend,
							    rt_intrvl_rend,
							    inqparams,
							    &root_i_facet_j)) {
	    roots[nroots++] = root_i_facet_j;
	  }
	}

	root_interval_lend += subinterval_length;
      }	// for i over nsubintervals
    }

    phi_pt_j += dim;
  } // for j over npts

  gsl_root_fsolver_free(brent_solver);

  return nroots;
}
