// inquad_cubature.cc ---

// Copyright (C) 2016 Omid Khanmohamadi

// Author: Omid Khanmohamadi

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "inquad_cubature.h"
#include "cubature.h"		// for hcubature
#include "return_code.h"	// for errcode
#include "cond_compile.h"	// for TESTING
#include "inquad_integrand_params.h" // for integrand_params
#include "inquad_params.h"	     // for inquad_params

#include <stdlib.h>		// for size_t
#include <string.h>		// for strcat

typedef struct cubature_integrand_params_s {
  integrand_field *integrand;
} cubature_integrand_params;


STATIC_UNLESS_TESTING
int cubature_integrand(unsigned		 xdim,
		       const double *	 x,
		       void		*cubature_integrand_prms,
		       unsigned		 fdim,
		       double		*fval)
{
  cubature_integrand_params *p =
    (cubature_integrand_params *) cubature_integrand_prms;

  const inquad_params * const inqparams =
    (inquad_params *)((integrand_params *)p->integrand->params)->inqparams;

  // This cast is necessary as p->integrand->function has a
  // non-constant 2nd parameter, but x comes in to this function as a
  // pointer to a constant double (const double *), as required by
  // cubature's interface. Implicit conversion from const to non-const
  // is not allowed, so without this explicit cast this code will
  // compile but not link.
  double * nonconst_x = (double *)x;

  dbgprintf("cubature, reduced_dim-D (%dD), eval_pt = ", xdim);
  dbgprintf_array(xdim, i, "%f ", x[i]);
  dbgprintf("\n");

  // The optimizer should remove this when the following
  char quad_outfname_pruning[MAX_FILENAME_LEN];

  // This is a hack to have the following statements parsed by the
  // preprocessor and turned into
  //
  // do if (WRT_QPT_FILE) {
  // strcpy(quad_outfname_pruning, inqparams->quad_outfname);
  // strcat(quad_outfname_pruning, "_pruning");
  // } while (0);
  //
  // where WRT_QPT_FILE will have been replaced by either 1 or 0
  // depending on whether 'WRITEQPTS' was defined (as a CPPFLAG) or
  // not. The optimizer will of course remove all that if WRT_QPT_FILE
  // is 0.
  //
  // The reason I am not simply surrounding this chunk in #if
  // WRITEQPTS ... #endif is to have the compiler check the
  // preprocessed code EVEN IF WRITEQPTS is NOT DEFINED to ensure that
  // this chunk of the code remains syntactically correct after we
  // have changed other parts. Otherwise, if this chunk is taken out
  // by the preprocessor except when WRITEQPTS is defined, then when
  // we are building with WRITEQPTS undefined and suddenly the need
  // arises for a 'WRITEQPTS build,' that build may not successfully
  // go through, because this chunk (or others) may not have been kept
  // up to date with the rest of the code and the discrepancies were
  // never noticed in other builds.
  wrtinvoke_statements(

  strcpy(quad_outfname_pruning MACRO_COMMA inqparams->quad_outfname)
  MACRO_SEMICOLON

  strcat(quad_outfname_pruning MACRO_COMMA "_pruning")
  MACRO_SEMICOLON
		       );

  // We have at least 9 bytes extra in inqparams->quad_outfname. So,
  // we can safely concatenate "_pruning" to the end of it.
  wrtprintf_array_if(xdim, i,
		     inqparams->quad_outfname[0] != '\0',
		     quad_outfname_pruning,
		     "%f ", x[i]);

  for (unsigned i = 0; i < fdim; ++i) {
    fval[i] = p->integrand->function(xdim,
				     nonconst_x,
				     p->integrand->params);
  }

  return 0;
}


errcode hyprect_apply_cubature(const hyprect * const	 h,
			       integrand_field		*integrand,
			       double * const		 integral_val)
{
  const unsigned xdim = h->reduced_dim;
  const unsigned fdim = 1;

  const size_t	maxeval	   = 0;	// unlimited # of evals of integrand
  const double	abserr_tol = 0;	// use relerr only
  const double	relerr_tol = 1e-6;
  double	integral_err;

  hyprect_range hrange;
  errcode ecode;
  if (SUCCESS != (ecode = hyprect_range_alloc(h, &hrange))) {
    return ecode;
  }

  hyprect_range_calc(h, &hrange);

  cubature_integrand_params cubature_integrand_prms;
  cubature_integrand_prms.integrand = integrand;

  hcubature(fdim, cubature_integrand, &cubature_integrand_prms,
	    xdim, hrange.xmins, hrange.xmaxs,
	    maxeval, abserr_tol, relerr_tol, ERROR_INDIVIDUAL,
	    integral_val,
	    &integral_err);

  hyprect_range_free(&hrange);

  return SUCCESS;
}
