// inquad_integrand.h ---

// Copyright (C) 2017 Omid Khanmohamadi

// Author: Omid Khanmohamadi

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef INQUAD_INTEGRAND_H_INCLUDED
#define INQUAD_INTEGRAND_H_INCLUDED

#include "inquad_integrand_params.h" // for integrand_params
#include "cond_compile.h"	     // for TESTING

#define INTEGRAND_FLD_VAL(FLD, dim, eval_pt)		\
  (*((FLD)->function))( dim, eval_pt, (FLD)->params )

errcode integrand_eval_pt_alloc(unsigned	  dim,
				double		**eval_pt);

void integrand_eval_pt_free(double * const eval_pt);

// The name integrand_set_params would be confusing, as the first
// argument is a pointer to integrand_params, not to a integrand_field
// (which would be the integrand).
void
integrand_params_set_all_but_partition_pts(integrand_params * const f_new_params,
					   integrand_field *f,
					   levelset * const phi,
					   hyprect * const h,
					   unsigned dir,
					   const inquad_params * const inqparams);

// Only run at the recursion level of outermost integral.
errcode
integrand_params_alloc_partition_pts(integrand_params * const f_new_params,
				   unsigned npts);

// Only run at the recursion level of outermost integral.
void
integrand_params_free_partition_pts(integrand_params * const f_new_params);


// This defines the function x ↦ ∫f(x[0], ..., y, x[dim])dy as the
// integrand for the next outer integral.
double integrand_value(unsigned		 dim,
		       double * const	 eval_pt,
		       void		*params);



#if TESTING

#include "gsl/gsl_integration.h"	// for gsl_function in following
#include "inquad_integrand_gsl_function.h"

double
function_1d(double y, void *params);

void
integrand_set_gsl_function_along_eval_pt_dir(gsl_function * const
					     F_1d,

					     gsl_function_params * const
					     F_1d_params,

					     double * const
					     eval_pt,

					     unsigned
					     dir,

					     unsigned
					     dim,

					     unsigned
					     reduced_dim,

					     const char * const
					     quad_outfname,

					     integrand_field * const
					     f,

					     const gsl_function_func
					     F_1d_function);


double
integrand_1d_value_in_subinterval(gsl_function * const
				  F_1d,

				  gsl_integration_glfixed_table
				  *gauss_legendre_tbl,

				  double
				  subint_lendpt,

				  double
				  subint_rendpt);

ulong
integrand_params_treated_dims(const integrand_params * const params);

unsigned
integrand_params_reduced_dim(const integrand_params * const params);

double surf_integrand_value(unsigned		 dim,
			    double * const	 eval_pt,
			    void		*params);

#endif

#endif // INQUAD_INTEGRAND_H_INCLUDED
