// inquad_geometry.h ---

// Copyright (C) 2016 Omid Khanmohamadi

// Author: Omid Khanmohamadi

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef INQUAD_GEOMETRY_H_INCLUDED
#define INQUAD_GEOMETRY_H_INCLUDED

#include "return_code.h"
#include "bit_util.h"
#include "cond_compile.h"

// Giving the struct a name (hyprect_s) in addition to typedef'ing it
// (as hyprect) ensures that we can, if need be, forward-declare it
// (declare it before defining it: 'struct hyprect_s' will work, but
// not 'struct hyprect').

// Each recursive call needs to keep track of its reduced_dim (by
// storing it on its stack). N.B. The recursion depth we are in is the
// difference of dim and reduced_dim.

// Bit d in treated_dims (LSB being bit 0) is set iff we have treated
// dimension d of the hyprect (i.e., ∫ dx_d has been defined as a
// function of the coordinates corresponding to the unset bits in
// treated_dims). The bits in treated_dims are not necessarily set in
// succession. A ulong is guaranteed to be (at least) 32 bits. So,
// ulong treated_dims allows for 32 dimensions. N.B. Pop count of
// treated_dims == (dim - reduced_dim).
typedef struct hyprect_s {
  double	*center;       // len: 2*dim
  double	*halfwidths;   // hyprect_alloc points this to 'center+dim'
  unsigned	 dim;	       // original dim of hyprect ⊂ R^{dim}
  unsigned	 reduced_dim;  // dim after each recursive dim reduction
  ulong          treated_dims;
} hyprect;

errcode hyprect_make(unsigned dim,
		     const double *center,
		     const double *halfwidths,
		     hyprect * const h);

void hyprect_free(hyprect * const h);

errcode hyprect_make_from_range(unsigned dim,
				const double *xmins,
				const double *xmaxs,
				hyprect * const h);

errcode hyprect_halve(hyprect * const h_left,
		      hyprect * const h_right,
		      unsigned dim2halve);

void hyprect_reduce(hyprect * const h, unsigned rmdim);

double hyprect_volume(const hyprect * const h);



// ----------------------------------------------------------------------
// [START] Used ONLY by inquad_cubature
//
typedef struct hyprect_range_s {
  double *xmins;
  double *xmaxs;
} hyprect_range;

errcode hyprect_range_alloc(const hyprect * const h,
			    hyprect_range * hrange);

// Calculate xmins and xmaxs range for the untreated dimensions of the
// current hyprect, to be used by hcubature.
// Output in hrange->{xmins, xmaxs}.
void hyprect_range_calc(const hyprect * const h,
			hyprect_range * const hrange);

void hyprect_range_free(hyprect_range * hrange);
//
// [END] Used ONLY by inquad_cubature
// ----------------------------------------------------------------------


int
hyprect_facet_set_next_vertex(const hyprect * const h,
			      const double * const xc, // THE center of facet
			      double * const xv); // A vertex of facet

void hyprect_mark_dim_as_treated(hyprect * const	h,
				unsigned		d);

double hyprect_reduced_radius(const hyprect * const h);

#if TESTING

errcode hyprect_copy(hyprect * const h_src, hyprect * const h_dest);

void hyprect_mark_dim_as_untreated(hyprect * const	h,
				   unsigned		d);

#endif

#endif	// INQUAD_GEOMETRY_H_INCLUDED
