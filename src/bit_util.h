// bit_util.h ---

// Copyright (C) 2016 Omid Khanmohamadi

// Author: Omid Khanmohamadi

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef BIT_UTIL_H_INCLUDED
#define BIT_UTIL_H_INCLUDED

#include <limits.h>		// for CHAR_BIT

// uintmax_t is the largest unsigned type provided by the
// implementation. We cast 1 as uintmax_t here so that these macros
// may be used on unsigned int, unsigned long int (>= 32 bits), or
// unsigned long long int (>= 64 bits).
#include <stdint.h>		// for uintmax_t

typedef unsigned long int ulong;
typedef unsigned long long int ullong;

#define zero_UL 0UL // Unsigned Long literal 0
#define one_UL 1UL  // Unsigned Long literal 1

#define zero_ULL 0ULL // Unsigned Long literal 0
#define one_ULL 1ULL  // Unsigned Long literal 1

#define SET_BIT(num, d)     ((num) |=        ((uintmax_t)1 << (d)))
#define CLEAR_BIT(num, d)   ((num) &=       ~((uintmax_t)1 << (d)))
#define TOGGLE_BIT(num, d)  ((num) ^=        ((uintmax_t)1 << (d)))
#define IS_BIT_SET(num, d)  (((num) >> (d)) & (uintmax_t)1)

#define NBITS_TYPE(type) (CHAR_BIT*sizeof(type))

unsigned nset_bits_ull(ullong num);
unsigned nset_bits_ul(ulong num);

int idx_1_bit(ullong num, unsigned d);

int idx_0_bit(ullong num, unsigned d);

unsigned idx_least_sig_0_bit(ullong num);

// Output in sgns_{neutral, posneg}.
void
store_sgn_in_bit_pair(int	 sgn, // 1 (pos), -1 (neg), 0 (+/- zero)
		      unsigned	 idx, // idx of bit to store sgn in
		      ullong	*sgns_neutral,
		      ullong	*sgns_posneg);

int bit_pair_to_sgn(ullong	sgns_neutral,
		    ullong	sgns_posneg,
		    unsigned	idx);

#endif	// BIT_UTIL_H_INCLUDED
